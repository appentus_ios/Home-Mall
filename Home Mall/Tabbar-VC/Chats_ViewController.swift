//  Chats_ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 01/05/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit

class Chats_ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let chat_VC = storyboard?.instantiateViewController(withIdentifier: "Chat_User_List_ViewController") as! Chat_User_List_ViewController
        chat_VC.modalPresentationStyle = .fullScreen
        present(chat_VC, animated: false, completion: nil)
    }
    
}
