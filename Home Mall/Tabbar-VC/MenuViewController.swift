//  MenuViewController.swift
//  AKSwiftSlideMenu
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.



import UIKit
import GooglePlaces


protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex_menu(_ index : Int32)
}



var is_image_ads_list = false
var is_image_ads_city = false


class MenuViewController: UIViewController {
    @IBOutlet weak var view_login_container:UIView!
    @IBOutlet weak var btn_cancel:UIButton!
    @IBOutlet weak var btn_login:UIView!
    
    @IBOutlet weak var view_by_ad_phone_no:UIView!
    @IBOutlet weak var view_by_location:UIView!
    
    @IBOutlet weak var view_1:UIView!
//    @IBOutlet weak var view_2:UIView!
    
    @IBOutlet weak var lbl_by_ad_phone_no:UILabel!
    @IBOutlet weak var lbl_by_location:UILabel!
    
    @IBOutlet weak var btn_by_ad_phone_number:UIButton!
    @IBOutlet weak var btn_by_location:UIButton!
    @IBOutlet weak var btn_search:UIButton!
    
    @IBOutlet weak var txt_phone_number:UITextField!
    @IBOutlet weak var view_phone_number:UIView!
    
    
    @IBOutlet weak var btn_search_1:UIButton!
    
    @IBOutlet weak var view_image_ads_only:UIView!
    @IBOutlet weak var btn_image_ads:UIButton!
    
    @IBOutlet weak var view_Please_login:UIView!
    
    var view_loading = UIView()
    
    /**
     *  Array to display menu options
     */

//    @IBOutlet var tblMenuOptions : UITableView!

    /**
     *  Transparent button to hide menu
     */

    @IBOutlet var btnCloseMenuOverlay : UIButton!

    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()

    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!

    /**
     *  Delegate of the MenuVC
     */
    
    var delegate : SlideMenuDelegate?
    
    @IBOutlet weak var view_picker_container:UIView!
    @IBOutlet weak var picker_view:UIPickerView!
    @IBOutlet weak var lbl_Category:UILabel!
    
    var view_loader = UIView()
    var  arr_cate = [Model_Category_List]()
    @IBOutlet weak var view_last_search:UIView!
    @IBOutlet weak var tbl_last_searched:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        func_rounded_corner(view: view_login_container)
//        func_rounded_corner(view: btn_cancel)
//        func_rounded_corner(view: btn_login)
        
        func_load_UI()
        
        txt_phone_number.attributedPlaceholder = NSAttributedString(string:txt_phone_number.placeholder!,
                                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        view_picker_container.isHidden = true
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_close_button), name: NSNotification.Name (rawValue: "selected_location"), object: nil)
    }
    
    func func_rounded_corner(view:UIView)  {
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        arr_cate.removeAll()
        updateArrayMenuOptions()
        
        view_Please_login.isHidden = true
        
        if str_city_list == "list" {
             btn_image_ads.isSelected = is_image_ads_list
        } else {
            btn_image_ads.isSelected = is_image_ads_city
        }
        
        func_get_all_save_city()
        
    }

    func updateArrayMenuOptions() {
        arrayMenuOptions.append(["title":"Home", "icon":"HomeIcon"])
        arrayMenuOptions.append(["title":"Play", "icon":"PlayIcon"])

//        tblMenuOptions.reloadData()
    }
    
    @IBAction func btn_search_by_mobile(_ button:UIButton!) {
        if txt_phone_number.text!.isEmpty {
            func_ShowHud_Error(with: "Enter mobile number".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                self.func_HideHud()
            })
            return
        }
        
        onCloseMenuClick(button)
        
        is_loaded_first_time = false
        
        Model_Search_VC.shared.user_mobile = txt_phone_number.text!
        Model_City.shared.user_mobile = txt_phone_number.text!
        
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "user_mobile_search"), object: nil)
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!) {
        btnMenu.tag = 0
        
        self.tabBarController?.tabBar.isHidden = false
        is_menu_open = false
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "hide_tabbar"), object: nil)
        
        if (self.delegate != nil) {
            
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex_menu(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x:UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            //                self.removeFromParent()
        })
    }
    
    
    @IBAction func btn_delete_search(_ button:UIButton!) {
        let alert = UIAlertController (title: "", message: "Are you sure?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "yes".localized, style: .default) { (yes) in
            self.func_delete_last_search()
        }
        
        let no = UIAlertAction(title: "no".localized, style: .default) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.view.tintColor = UIColor .black
        present(alert, animated: true, completion: nil)
    }
    
    func func_delete_last_search() {
        view_loading.isHidden = false
        Model_Last_Searched.shared.func_delete_save_city_post { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.tbl_last_searched.isHidden = true
                self.view_last_search.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: "Deleted successfully".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.view_last_search.isHidden = true
                    })
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
}



import UIKit

extension UIViewController:SlideMenuDelegate {

    func slideMenuItemSelectedAtIndex_menu(_ index: Int32) {
//        let topViewController : UIViewController = self.navigationController!.topViewController!
//        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            print("Home\n", terminator: "")

//            self.openViewControllerBasedOnIdentifier_menu("Home")

            break
        case 1:
            print("Play\n", terminator: "")

//            self.openViewControllerBasedOnIdentifier_menu("PlayVC")

            break
        default:
            print("default\n", terminator: "")
        }
    }

    func openViewControllerBasedOnIdentifier_menu(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }

//    func addSlideMenuButton(){
//
//        let btnShowMenu = UIButton (type: UIButtonType.system)
//        btnShowMenu.setImage(self.defaultMenuImage(), for:.normal)
//        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        btnShowMenu.addTarget(self, action: #selector(onSlideMenuButtonPressed(_:)), for:.touchUpInside)
//        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
//        self.navigationItem.leftBarButtonItem = customBarItem;
//    }

    func defaultMenuImage_menu() -> UIImage {
        var defaultMenuImage_menu = UIImage()

        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)

        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()

        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()

        defaultMenuImage_menu = UIGraphicsGetImageFromCurrentImageContext()!

        UIGraphicsEndImageContext()

        return defaultMenuImage_menu;
    }

    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex_menu(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })

            return
        }

        sender.isEnabled = false
        sender.tag = 10
        
        is_menu_open = true
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "hide_tabbar"), object: nil)
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.bringSubview(toFront: self.view)
        self.tabBarController?.tabBar.isHidden = true
        //        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        menuVC.view.frame=CGRect(x:UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);

        UIView.animate(withDuration: 0.15, animations: { () -> Void in
            menuVC.view.frame=CGRect(x:0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
}





extension MenuViewController {
    
    func func_load_UI() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        view_phone_number.layer.cornerRadius = 10
        view_phone_number.clipsToBounds = true
        
        view_1.layer.cornerRadius = 10
        view_1.clipsToBounds = true
        
//        view_2.layer.cornerRadius = view_2.frame.size.height/2
//        view_2.clipsToBounds = true
        
        btn_by_ad_phone_number.layer.cornerRadius = 5
        btn_by_ad_phone_number.clipsToBounds = true
        
        btn_by_location.layer.cornerRadius = 5
        btn_by_location.clipsToBounds = true
        
        btn_search.layer.cornerRadius = 10
        btn_search.layer.shadowOpacity = 3.0
        btn_search.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_search.layer.shadowRadius = 3.0
        btn_search.layer.shadowColor = color_app.cgColor
        
        btn_search_1.layer.cornerRadius = 10
        btn_search_1.layer.shadowOpacity = 3.0
        btn_search_1.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_search_1.layer.shadowRadius = 3.0
        btn_search_1.layer.shadowColor = color_app.cgColor
        
        view_image_ads_only.layer.cornerRadius = 5
        view_image_ads_only.clipsToBounds = true
        
        lbl_by_ad_phone_no.backgroundColor = UIColor .white
        lbl_by_ad_phone_no.textColor = UIColor .black
        lbl_by_location.backgroundColor = UIColor .clear
        lbl_by_location.textColor = UIColor .white
        
        lbl_by_ad_phone_no.layer.cornerRadius = 10
        lbl_by_ad_phone_no.clipsToBounds = true
        lbl_by_location.layer.cornerRadius = 10
        lbl_by_location.clipsToBounds = true
        
        view_by_ad_phone_no.isHidden = false
        view_by_location.isHidden = true
        
//        cell.Label.layer.addBorder(UIRectEdge.Top, color: UIColor.greenColor(), thickness: 0.5)
        
//        lbl_by_location.layer.addBorder(edge:.top , color: UIColor .green, thickness: 2)
    }



    @IBAction func btn_ad_by_phone(_ sender:UIButton) {
        lbl_by_ad_phone_no.backgroundColor = UIColor .white
        lbl_by_ad_phone_no.textColor = UIColor .black
        lbl_by_location.backgroundColor = UIColor .clear
        lbl_by_location.textColor = UIColor .white
        
        view_by_ad_phone_no.isHidden = false
        view_by_location.isHidden = true
    }
    
    @IBAction func btn_by_location(_ sender:UIButton) {
        lbl_by_location.backgroundColor = UIColor .white
        lbl_by_location.textColor = UIColor .black
        lbl_by_ad_phone_no.backgroundColor = UIColor .clear
        lbl_by_ad_phone_no.textColor = UIColor .white
        
        view_by_ad_phone_no.isHidden = true
        view_by_location.isHidden = false
    }
    
    
}



extension CALayer {
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: thickness)
            
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.bounds.height - thickness,  width: UIScreen.main.bounds.width, height: thickness)
            
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0,  width: thickness, height: self.bounds.height)
            
        case UIRectEdge.right:
            border.frame = CGRect(x: self.bounds.width - thickness, y: 0,  width: thickness, height: self.bounds.height)
        default:
            print("nothing")
        }
        
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
    
}



extension MenuViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    @objc func func_close_button() {
        
        self.tabBarController?.tabBar.isHidden = false
        is_menu_open = false
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "hide_tabbar"), object: nil)
        
        delegate?.slideMenuItemSelectedAtIndex_menu(-1)
        
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
//            self.view.layoutIfNeeded()
//            self.view.backgroundColor = UIColor.clear
//        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
//            self.removeFromParentViewController()
//                self.removeFromParent()
//        })
    }
    
    @IBAction func btn_category(_ sender:UIButton) {
        func_get_category()
    }
    
    @IBAction func btn_cancel_picker(_ sender:UIButton) {
        view_picker_container.isHidden = true
    }
    
    @IBAction func btn_done_picker(_ sender:UIButton) {
        Model_City.shared.subcate_code = ""
        view_picker_container.isHidden = true
        
        if str_city_list == "list" {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "search_by_cate_list"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "search_by_cate_city"), object: nil)
        }
        
        onCloseMenuClick(sender)
    }
    
    @IBAction func btn_search_image_ads_only(_ sender:UIButton) {
        if str_city_list == "list" {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "image_ads_list"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "image_ads_city"), object: nil)
        }
        onCloseMenuClick(sender)
    }
    
    @IBAction func btn_image_ads(_ sender:UIButton) {
        if str_city_list == "list" {
            if btn_image_ads.isSelected {
                btn_image_ads.isSelected = false
                is_image_ads_list = false
            } else {
                btn_image_ads.isSelected = true
                is_image_ads_list = true
            }
        } else {
            if btn_image_ads.isSelected {
                btn_image_ads.isSelected = false
                is_image_ads_city = false
            } else {
                btn_image_ads.isSelected = true
                is_image_ads_city = true
            }
        }
        
    }
    
    @IBAction func btn_place_By_search(_ sender:UIButton) {
        onCloseMenuClick(sender)
        
        if str_city_list == "list" {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "open_google_list"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "open_google_city"), object: nil)

        }
        
    }
    
    @IBAction func btn_chat_with_homemal_admin(_ sender:UIButton) {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            Model_Splash.shared.user_id = "\(dict_LoginData["user_id"] ?? "")"
            
            Model_Chat_List.shared.user_role = "2"
            Model_Chat_Message_Fri.shared.fri_id = "50"
            Model_Chat_Message_Fri.shared.fri_name = "admin"
            
            let chat_message = storyboard?.instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController") as! Chat_Message_Fri_ViewController
            chat_message.modalPresentationStyle = .fullScreen
            present(chat_message, animated: true, completion: nil)
        } else {
            view_Please_login.isHidden = false
        }
    }
    
    @IBAction func btn_chat_users(_ sender:UIButton) {
        onCloseMenuClick(sender)
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "show_chat_VC"), object: nil)
    }
    
    @IBAction func btn_login(_ button:UIButton!) {
        view_Please_login.isHidden = true
        let my_profile = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        my_profile.modalPresentationStyle = .fullScreen
        present(my_profile, animated: true, completion: nil)
    }
    
    @IBAction func btn_cancel(_ button:UIButton!) {
        view_Please_login.isHidden = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_cate.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let model = arr_cate[row]
        return model.category_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let model = arr_cate[row]
        lbl_Category.text = model.category_name
        
        Model_City.shared.category_code = model.category_code
        Model_Search_VC.shared.category_code = model.category_code
    }
    
    func func_get_category() {
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_cate.removeAll()
                
                if status == "success" {
                    self.arr_cate = Model_Category_List.shared.arr_category_list
                }
                
                self.view_picker_container.isHidden = false
                self.picker_view.reloadAllComponents()
            }
        }
    }
    
}



extension MenuViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == tbl_searched {
//            return Model_Last_Searched.shared.arr_get_city.count
//        } else {
            return Model_Last_Searched.shared.arr_get_all_save_city.count
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if tableView == tbl_searched {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath)
//
//            let lbl_city_name = cell.viewWithTag(1) as! UILabel
//
//            let model = Model_Last_Searched.shared.arr_get_city[indexPath.row]
//            lbl_city_name.text = "\(model .city_name) ( \(model.city_nicname) )"
//
//            return cell
//        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            let lbl_city_name = cell.viewWithTag(1) as! UILabel
            
            let model = Model_Last_Searched.shared.arr_get_all_save_city[indexPath.row]
            lbl_city_name.text = model .city_name
            
            return cell
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        
        let btn_ = UIButton()
        onCloseMenuClick(btn_)
        
        let model = Model_Last_Searched.shared.arr_get_all_save_city[indexPath.row]
        
        Model_Search_VC.shared.location_lat = model .city_lat
        Model_Search_VC.shared.location_lang = model .city_long
        
        if str_city_list == "list" {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_google_list"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_google_city"), object: nil)
        }
        
    }
    
    func func_get_all_save_city() {
        view_loading.isHidden = false
        Model_Last_Searched.shared.func_get_all_save_city { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    if Model_Last_Searched.shared.arr_get_all_save_city.count > 0 {
                        self.view_last_search.isHidden = false
                    } else {
                        self.view_last_search.isHidden = true
                    }
                } else {
                    self.view_last_search.isHidden = true
                }
                self.tbl_last_searched.reloadData()
            }
        }
        
        
        
//        func func_delete_save_city_post() {
//
//        }
        
        
        
//        @IBAction func onCloseMenuClick(_ button:UIButton!) {
//
//        }
//
//    @IBAction func btn_delete_city(_ sender:UIButton!) {
//    }
    
    func func_after_select_location() {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location"), object: nil)
        
        if str_city_list == "list" {
            //            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_list"), object: nil)
        } else {
            //            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_city"), object: nil)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

}
