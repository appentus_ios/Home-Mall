//
//  File.swift
//  Home Mall
//
//  Created by iOS-Appentus on 23/05/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation






class Model_Tabbar {
    static let shared = Model_Tabbar()
    
    var is_checked = false
    var message = ""
    var shop_count = ""
    
    func func_is_user_active(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"is_user_active"
//        print(str_FullURL)
        
        let params = [
            "user_id":Model_Splash.shared.user_id
        ]
        
//        print(params)
        
        API_Home_Mall .postAPI_check_login_data(url: str_FullURL, parameters: params) {
            (dict_JSON) in
//            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                if "1" != "\(dict_JSON["shop_count"]!)" {
                    self.message = "Shop is delete from Admin"
                    NotificationCenter.default.post(name: NSNotification.Name (rawValue: "shop_count"), object: nil)
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        if "\(dict_JSON["message"]!)" != "user exists" {
                            if "\(dict_JSON["message"]!)" != "Error In API" {
                                self.message = "This user is delete from Admin"
                                NotificationCenter.default.post(name: NSNotification.Name (rawValue:"user_exists"), object: nil)
                                UserDefaults.standard.removeObject(forKey: "login_Data")
                            }
                        }
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
}





