//
//  tabBar.swift
//  TacTec
//
//  Created by Love on 18/07/18.
//  Copyright © 2018 Appentus. All rights reserved.
//

import UIKit

var is_menu_open = false
var is_list = true

class TabBar_Controller: UITabBarController ,UITabBarControllerDelegate {
    
    var menuButton_middle = UIButton()
    var menuButton_profile = UIButton()
    var menu_left = UIView()
    var menu_right = UIView()
    var infoWindow = MyLocation()
    var infoWindow_profile = Profile_tabbar()
    
    var timer = Timer()
    
    var tabbar_Apearance = UITabBarItem.appearance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if #available(iOS 10.0, *) {
            tabbar_Apearance.badgeColor = UIColor (red: 0.0/255.0, green: 251.0/255.0, blue: 213.0/255.0, alpha: 1.0)
        } else {
            // Fallback on earlier versions
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_hide_tabbar), name: NSNotification.Name (rawValue: "hide_tabbar"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(menuButtonAction(sender:)), name: NSNotification.Name (rawValue: "login_chat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_count_notifications), name:Notification.Name(rawValue: "count_notifications"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(show_chat_VC), name:Notification.Name(rawValue: "show_chat_VC"), object: nil)
        
//        timer = Timer .scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(func_is_user_active), userInfo: nil, repeats: true)
        
        func_tabbar()
        setupMiddleButton()
        func_Profile()
        func_add_line()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_count_notifications()
        is_my_location_enable = true
    }
    
    @objc func func_hide_tabbar() {
        if is_menu_open {
            menuButton_middle.isHidden = true
            menuButton_profile.isHidden = true
            menu_left.isHidden = true
            menu_right.isHidden = true

            infoWindow.isHidden = true
            infoWindow_profile.isHidden = true
        } else {
            menuButton_middle.isHidden = false
            menuButton_profile.isHidden = false
            menu_left.isHidden = false
            menu_right.isHidden = false

            infoWindow.isHidden = false
            infoWindow_profile.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    // MARK: - Setups
    func setupMiddleButton() {
        menuButton_middle = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        
        var menuButtonFrame = menuButton_middle.frame
        
        menuButtonFrame.origin.y = view.bounds.height - menuButtonFrame.height - 15
        
        menuButtonFrame.origin.x = view.bounds.width/2 - menuButtonFrame.size.width/2
        
        menuButton_middle.frame = menuButtonFrame
        menuButton_middle.backgroundColor =  UIColor.clear
        
        infoWindow = Bundle.main.loadNibNamed("MyLocation", owner: self, options: nil)?.first as! MyLocation
        infoWindow.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        infoWindow.backgroundColor = UIColor.clear
        var frame_window = infoWindow.frame
        
        if self.view.bounds.height > 736 {
            frame_window.origin.y = menuButton_middle.frame.origin.y+10-32
        } else {
            frame_window.origin.y = menuButton_middle.frame.origin.y+10
        }
        
        frame_window.origin.x = menuButton_middle.frame.origin.x+10
        infoWindow.frame = frame_window
        
        view.addSubview(infoWindow)
        view.addSubview(menuButton_middle)
        
        menuButton_middle.addTarget(self, action: #selector(menuButtonAction(sender:)), for: .touchUpInside)
        
        view.layoutIfNeeded()
    }
    
    
    func func_Profile() {
        menuButton_profile = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        
        var menuButtonFrame = menuButton_profile.frame
        menuButtonFrame.origin.y = view.bounds.height - menuButtonFrame.height
        
//        print(Locale.current.regionCode)
//        print(Locale.current.languageCode)
//        print(NSLocale.current.languageCode)
        
//        if Locale.current.languageCode == "en" {
            menuButtonFrame.origin.x = view.bounds.width - menuButtonFrame.size.width
//        } else {
//            menuButtonFrame.origin.x = 10
//        }
        
        menuButton_profile.frame = menuButtonFrame
        menuButton_profile.clipsToBounds = true
        
        infoWindow_profile = Bundle.main.loadNibNamed("Profile_tabbar", owner: self, options: nil)?.first as! Profile_tabbar
        infoWindow_profile.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        var frame_window = infoWindow_profile.frame
        if self.view.bounds.height > 736 {
            frame_window.origin.y = menuButton_profile.frame.origin.y+20-32
        } else {
            frame_window.origin.y = menuButton_profile.frame.origin.y+20
        }
        
        frame_window.origin.x = menuButton_profile.frame.origin.x+10
        infoWindow_profile.frame = frame_window
        
        view.addSubview(infoWindow_profile)
        view.addSubview(menuButton_profile)
        
//        menuButton.setImage(UIImage(named:"user"), for: .normal)
        menuButton_profile.addTarget(self, action: #selector(Profile_ViewController.onSlideMenuButtonPressed_1(_:)), for: .touchUpInside)
        
        view.layoutIfNeeded()
    }
    
    func func_add_line() {
        menu_left = UIView(frame: CGRect(x:view.bounds.width/4-20, y: view.bounds.height - 45, width: 1, height:40))
        
        if self.view.bounds.height > 736 {
           menu_left = UIView(frame: CGRect(x:view.bounds.width/4-20, y: view.bounds.height - 45-32, width: 1, height:40))
        } else {
            menu_left = UIView(frame: CGRect(x:view.bounds.width/4-20, y: view.bounds.height - 45, width: 1, height:40))
        }
        
        menu_left.backgroundColor = UIColor .lightGray
        view.addSubview(menu_left)
        
        menu_right = UIView(frame: CGRect(x:view.bounds.width-60, y: view.bounds.height - 45, width: 1, height:40))
        
        if self.view.bounds.height > 736 {
            menu_right = UIView(frame: CGRect(x:view.bounds.width-70, y: view.bounds.height - 45-32, width: 1, height:40))
        } else {
            menu_right = UIView(frame: CGRect(x:view.bounds.width-70, y: view.bounds.height - 45, width: 1, height:40))
        }
        
        menu_right.backgroundColor = UIColor .lightGray
        view.addSubview(menu_right)
    }
    
    
    
    @objc func func_tabbar() {
        let controller1 = self.storyboard?.instantiateViewController(withIdentifier: "Chats_ViewController") as! Chats_ViewController
        let chat = "chat".localized
        
        controller1.tabBarItem = UITabBarItem.init(title: chat, image: UIImage(named: "speech-bubble"), selectedImage: UIImage(named: "speech-bubble"))
        controller1.tabBarItem.tag = 1
        let nav1 = UINavigationController(rootViewController: controller1)
        
        let list = "قائمة" //.localized
        let controller2 = self.storyboard?.instantiateViewController(withIdentifier: "List_ViewController") as! List_ViewController
        controller2.tabBarItem = UITabBarItem.init(title: list, image: UIImage(named: "list"), selectedImage: UIImage(named: "list"))
        let nav2 = UINavigationController(rootViewController: controller2)
        
        let controller3 = self.storyboard?.instantiateViewController(withIdentifier: "City_ViewController") as! City_ViewController
        controller3.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: ""), selectedImage: UIImage(named: ""))
        let nav3 = UINavigationController(rootViewController: controller3)
        
        let controller4 = self.storyboard?.instantiateViewController(withIdentifier: "My_Loction_ViewController") as! My_Loction_ViewController
//        let loc_MyLocation = NSLocalizedString("MyLocation", comment: "")
//        let loc_MyLocation = LocalizationSystem.sharedInstance.localizedStringForKey(key: "MyLocation", comment: "")
        let city = "City".localized
        controller4.tabBarItem = UITabBarItem.init(title: city, image: UIImage(named: "saudi_map"), selectedImage: UIImage(named: "saudi_map"))
        let nav4 = UINavigationController(rootViewController: controller4)
        
        let controller5 = self.storyboard?.instantiateViewController(withIdentifier: "City_ViewController") as! City_ViewController
        controller5.tabBarItem = UITabBarItem.init(title: "", image: UIImage(named: ""), selectedImage: UIImage(named: ""))
        let nav5 = UINavigationController(rootViewController: controller5)
        
        viewControllers = [nav1,nav2,nav3,nav4,nav5]
        
//        self.tabBar.items![0].badgeValue = "10"
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
        DispatchQueue.main.async {
            self.selectedIndex = 2
        }
        
    }
    
    @objc private func menuButtonAction(sender:UIButton) {
//        if is_my_location_enable {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "my_location"), object: nil)
//        }
        selectedIndex = 2
    }
    
    @objc private func show_chat_VC() {
        selectedIndex = 0
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let list = "قائمة" //.localized
        let map = "خريطة" //.localized
        let city = "City".localized
        
        if item.title == list {
            tabBar.items![1].title = map
            tabBar.items![1].image = UIImage (named: "placeholder")
            tabBar.items![1].selectedImage = UIImage (named: "placeholder")
            is_list = false
            
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                NotificationCenter.default.post(name: NSNotification.Name (rawValue: "map_or_list"), object: nil)
            }
        } else if item.title == map {
            tabBar.items![1].title = list
            tabBar.items![1].image = UIImage (named: "list")
            tabBar.items![1].selectedImage = UIImage (named: "list")
            is_list = true
            
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "map_or_list"), object: nil)
        } else if item.title == city {
            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "City"), object: nil)
        }
    }
}



extension TabBar_Controller {
    @objc func func_count_notifications() {
        if let count_noti = UserDefaults.standard.object(forKey: "count_notifications") as? String {
            let tabItems = self.tabBar.items
            let tabItem = tabItems![0]
            
            if !(count_noti.isEmpty) {
                tabItem.badgeValue = "\(count_noti)"
                UIApplication.shared.applicationIconBadgeNumber = Int(count_noti)!
            } else {
                tabItem.badgeValue = nil
                tabItem.badgeValue?.removeAll()
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            
        }
    }
    
    
    
    @objc func func_is_user_active() {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            Model_Splash.shared.user_id = "\(dict_LoginData["user_id"] ?? "")"
            
            Model_Tabbar.shared.func_is_user_active { (status) in
                if status == "failed" {
                    self.timer.invalidate()
                } else if status == "success" {
                    self.timer = Timer .scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.func_is_user_active), userInfo: nil, repeats: true)
                }
            }
            
        }
        
    }
    
}









