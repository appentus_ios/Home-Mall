//  AppDelegate.swift
//  Home Mall
//  Created by iOS-Appentus on 09/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit

import GoogleMaps
import GooglePlaces
import GooglePlacesAPI

import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import UserNotificationsUI
import FirebaseMessaging

//let k_GOOLGE_API_KEY = "AIzaSyCl0fMesVtHvhfEwLbRqesm83AwdDtE-xE"
//let k_GOOLGE_API_KEY = "AIzaSyA80-ommi_fpf76xQLUduhno1XQG1uVix0" // this is running
let k_GOOLGE_API_KEY = "AIzaSyCl8EQNpuwCmYy_JAhP_sTfas_sxCSr3_s"

var is_first_time_launch = true
var userInfo_first_time = [AnyHashable: Any]()

var str_selected_lang = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        GMSServices.provideAPIKey(k_GOOLGE_API_KEY)
        GMSPlacesClient.provideAPIKey(k_GOOLGE_API_KEY)
        GooglePlaces.provide(apiKey: k_GOOLGE_API_KEY)
        
//        GooglePlacesAPI.provide(apiKey: k_GOOLGE_API_KEY)
        
        askForNotificationPermission(application)
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_is_first_time_launch_1), name: NSNotification.Name (rawValue: "is_first_time_launch"), object: nil)
        
        let language = "ar"
        
//        Bundle.setLanguage(language)
//        LocalizationSystem.sharedInstance.setLanguage(languageCode: language)
        str_selected_lang = language
        
        Language.language = Language.english
        
        return true
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}



var k_FireBaseFCMToken = ""

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        k_FireBaseFCMToken = fcmToken
    }
    
}



extension AppDelegate:UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
    }
    
    func askForNotificationPermission(_ application: UIApplication) {
        let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
        if !isRegisteredForRemoteNotifications {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                    if error == nil {
                        if !granted {
                            self.openNotificationInSettings()
                        } else {
                            UNUserNotificationCenter.current().delegate = self
                        }
                    }
                }
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
        } else {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().delegate = self
            } else {
                
            }
        }
        
        application.registerForRemoteNotifications()
    }
    
    
    func openNotificationInSettings() {
        let alertController = UIAlertController(title: "Notification Alert", message: "Please enable Notification from Settings to never miss a text.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                } else {
                    
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .badge, .sound])
        
        let application_BadgeNumber = UIApplication.shared
        application_BadgeNumber.applicationIconBadgeNumber = application_BadgeNumber.applicationIconBadgeNumber+1
        
        func_count_notifications()
        
        let dataDict = notification.request.content.userInfo
        let result : String = dataDict["result"] as! String
        guard let jsonResp = anyConvertToDictionary(text: result)! as?  [String:Any] else {
            return
        }
        print(jsonResp)
        
        let dict_title = dataDict["aps"] as! [String:Any]
        let dict_alert = dict_title["alert"] as! [String:Any]
        let str_title = dict_alert["title"] as! String
        
        print(str_title)
        let arr_details = jsonResp["details"] as! [[String:Any]]
        
        if str_title.contains("Chat") {
            Model_Chat_Message_Fri.shared.fri_id = arr_details[0]["user_id"] as! String
            Model_Chat_Message_Fri.shared.fri_name = arr_details[0]["user_name"] as! String
            
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NotificationCenter.default.post(name: Notification.Name("chat_incoming"), object: nil)
            }
        } else if str_title.contains("Unblocked You") || str_title.contains("Blocked You") {
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NotificationCenter.default.post(name: Notification.Name("Unblocked_block"), object: nil)
            }
        }
        
    }

    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application_BadgeNumber = UIApplication.shared
        application_BadgeNumber.applicationIconBadgeNumber = application_BadgeNumber.applicationIconBadgeNumber+1
        
        let userInfo = response.notification.request.content.userInfo
//        print(userInfo)
        if is_first_time_launch {
            userInfo_first_time = userInfo
        } else {
            func_did_recieve_APNS(userInfo: userInfo)
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    @objc func func_did_recieve_APNS(userInfo : [AnyHashable: Any]) {
        var jsonResp = [String:Any]()
        var arr_details = [[String:Any]]()
        
        var str_MSG = ""
        
        let dataDict = userInfo
        let result : String = dataDict["result"] as! String
        if let jsonResp_1 = anyConvertToDictionary(text: result) as? [String:Any] {
            jsonResp = jsonResp_1
            arr_details = jsonResp["details"] as! [[String:Any]]
            if let response = jsonResp as? [String:Any] {
                jsonResp = response
                str_MSG = jsonResp["msgs"] as! String
                print(str_MSG)
            }
        } else {
            str_MSG = ""
        }
        
        
        
        if str_MSG.contains("Chat") {
            Model_Chat_Message_Fri.shared.fri_id = arr_details[0]["user_id"] as! String
            Model_Chat_Message_Fri.shared.fri_name = arr_details[0]["user_name"] as! String
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController")
            
            let class_name = "\(viewController.classForCoder)"
            let class_name_now = "\(UIApplication.topViewController()!.classForCoder)"
            
            if class_name == class_name_now {
                
            } else {
                let appdelgateObj = UIApplication.shared.delegate as! AppDelegate
                if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"Chat_Message_Fri_ViewController") as? Chat_Message_Fri_ViewController {
                    if let window = appdelgateObj.window , let rootViewController = window.rootViewController {
                        var currentController = rootViewController
                        while let presentedController = currentController.presentedViewController {
                            currentController = presentedController
                        }
                        currentController.present(destinationVC, animated: true, completion: nil)
                    }
                }
            }
            
            
//            let appdelgateObj = UIApplication.shared.delegate as! AppDelegate
//            if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController") as? Chat_Message_Fri_ViewController {
//                if let window = appdelgateObj.window , let rootViewController = window.rootViewController {
//                    var currentController = rootViewController
//                    while let presentedController = currentController.presentedViewController {
//                        currentController = presentedController
//                    }
//
////                    if is_first_time_launch {
//                        if self.window!.rootViewController is Chat_Message_Fri_ViewController {
//                            print("same view controller")
//                        } else {
//                            currentController.present(destinationVC, animated: true, completion: nil)
//                        }
//
////                    } else {
////                        currentController.present(destinationVC, animated: true, completion: nil)
////                        is_first_time_launch = false
////                    }
//                }
//            }
            
//            if destinationVC.restorationIdentifier == self.window?.rootViewController?.restorationIdentifier {
//                print("same view controller")
//                if is_first_time_launch {
//                    if self.window!.rootViewController is Chat_Message_Fri_ViewController {
//                        print("same view controller")
//                    } else {
//                        currentController.present(destinationVC, animated: true, completion: nil)
//                    }
//                }
//            } else {
//                self.window!.rootViewController!.present(destinationVC, animated: true, completion: nil)
//                is_first_time_launch = false
//            }

            
//            let appdelgateObj = UIApplication.shared.delegate as! AppDelegate
//            if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController") as? Chat_Message_Fri_ViewController {
//
//                if let window = appdelgateObj.window , let rootViewController = window.rootViewController {
//                    var currentController = rootViewController
//
//                    while let presentedController = currentController.presentedViewController {
//                        currentController = presentedController
//                    }
//
//                    print(destinationVC.restorationIdentifier)
//                    print(self.window?.rootViewController?.restorationIdentifier)
//
//                    if destinationVC.restorationIdentifier == self.window?.rootViewController?.restorationIdentifier {
//                        print("same view controller")
//                        if is_first_time_launch {
//                            if self.window!.rootViewController is Chat_Message_Fri_ViewController {
//                                print("same view controller")
//                            } else {
//                                currentController.present(destinationVC, animated: true, completion: nil)
//                            }
//                        }
//                    } else {
//                        self.window!.rootViewController!.present(destinationVC, animated: true, completion: nil)
//                        is_first_time_launch = false
//                    }
//                }
//            }
            NotificationCenter.default.post(name: Notification.Name("chat_incoming"), object: nil)
        }
        
    }
    
    
    func func_count_notifications() {
        if let count_notifications = UserDefaults.standard.object(forKey: "count_notifications") as? String {
            if count_notifications.isEmpty {
                UserDefaults.standard.setValue("1", forKey: "count_notifications")
                NotificationCenter.default.post(name: Notification.Name("count_notifications"), object: nil)
            } else {
                UserDefaults.standard.setValue("\(Int(count_notifications)!+1)", forKey: "count_notifications")
                NotificationCenter.default.post(name: Notification.Name("count_notifications"), object: nil)
            }
        } else {
            UserDefaults.standard.setValue("1", forKey: "count_notifications")
            NotificationCenter.default.post(name: Notification.Name("count_notifications"), object: nil)
        }

    }
    
}


extension AppDelegate {
    
    @objc func func_is_first_time_launch_1() {
//        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.func_is_first_time_launch(userInfo: userInfo_first_time)
//        }
    }
    
    func func_is_first_time_launch(userInfo : [AnyHashable: Any]) {
        if !userInfo_first_time.isEmpty {
           func_did_recieve_APNS(userInfo: userInfo)
        }
        
    }
    
}



public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}



extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}


