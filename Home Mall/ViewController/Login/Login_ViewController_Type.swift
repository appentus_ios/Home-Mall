//  Login_ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 21/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import MessageUI



let share_text = "تطبيق Home Mall , يحتوى على العديد من متاجر الاسر المتنجة , رائع وسهل الاستخدام , انظم معنا وتصفح العديد من المتاجر الرائعة , يمكنك المشاركة والطلب والتقيم , حمله الان ."


class Login_ViewController_Type: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var snapchat:UIView!
    @IBOutlet weak var twitter:UIView!
    @IBOutlet weak var appstore:UIView!
    
    @IBOutlet weak var twitter_round:UIButton!
    @IBOutlet weak var btn_whats_app:UIButton!
    
    @IBOutlet weak var img_snapchat:UIImageView!
    @IBOutlet weak var img_twitter:UIImageView!
    @IBOutlet weak var img_appstore:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_shadow(view_MyEarnings: snapchat)
        func_shadow(view_MyEarnings: twitter)
        func_shadow(view_MyEarnings: appstore)
        
        func_shadow(view_MyEarnings: twitter_round)
        func_shadow(view_MyEarnings: btn_whats_app)
        
        img_twitter.layer.cornerRadius = 4
        img_snapchat.layer.cornerRadius = 4
        img_appstore.layer.cornerRadius = 8
        
        img_appstore.clipsToBounds = true
        img_twitter.clipsToBounds = true
        img_snapchat.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func func_call(_ sender:Any) {
        func_send_mail()
        
//        if let phoneCallURL = URL(string: "tel://+919116645020") {
//            let application:UIApplication = UIApplication.shared
//            if (application.canOpenURL(phoneCallURL)) {
//                if #available(iOS 10.0, *) {
//                    application.open(phoneCallURL, options: [:], completionHandler: nil)
//                } else {
//                    application.openURL(phoneCallURL)
//                }
//            }
//        }
    }
    
    @IBAction func btn_snapchat(_ sender: UIButton) {
        str_tell_your_friends = "https://www.instagram.com/Homemall.hm"//"www.instagram.com/homemallsa"
        navigation_title_tell_your_friends = "Homemall-Instagram"
        
        let alert_VC = UIAlertController (title: "", message: "Open Instagram".localized, preferredStyle: .actionSheet)
        
        let withApp = UIAlertAction (title: "In Browser".localized, style: .default) { (status) in
            let tell_your_friends = self.storyboard?.instantiateViewController(withIdentifier: "Tell_Your_Friends_ViewController") as! Tell_Your_Friends_ViewController
            tell_your_friends.modalPresentationStyle = .fullScreen
            self.present(tell_your_friends, animated: true, completion: nil)
        }
        
        let intoApp = UIAlertAction (title: "In Instagram".localized, style: .default) { (status) in
            if let url = URL(string: str_tell_your_friends),UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                let tell_your_friends = self.storyboard?.instantiateViewController(withIdentifier: "Tell_Your_Friends_ViewController") as! Tell_Your_Friends_ViewController
                tell_your_friends.modalPresentationStyle = .fullScreen
                self.present(tell_your_friends, animated: true, completion: nil)
            }
        }
        
        let cancel = UIAlertAction (title:"Cancel".localized, style: .cancel) { (status) in
            
        }
        
        alert_VC.addAction(withApp)
        alert_VC.addAction(intoApp)
        alert_VC.addAction(cancel)
        present(alert_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_twitter(_ sender: UIButton) {
        navigation_title_tell_your_friends = "Homemall-Twitter"
        str_tell_your_friends = "https://twitter.com/homemall3"
        
        let alert_VC = UIAlertController (title: "", message: "Open Twitter".localized, preferredStyle: .actionSheet)
        
        let withApp = UIAlertAction (title: "In Browser".localized, style: .default) { (status) in
            let tell_your_friends = self.storyboard?.instantiateViewController(withIdentifier: "Tell_Your_Friends_ViewController") as! Tell_Your_Friends_ViewController
            tell_your_friends.modalPresentationStyle = .fullScreen
            self.present(tell_your_friends, animated: true, completion: nil)
        }
        
        let intoApp = UIAlertAction (title: "In Twitter".localized, style: .default) { (status) in
            if let url = URL(string: str_tell_your_friends),UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                let tell_your_friends = self.storyboard?.instantiateViewController(withIdentifier: "Tell_Your_Friends_ViewController") as! Tell_Your_Friends_ViewController
                tell_your_friends.modalPresentationStyle = .fullScreen
                self.present(tell_your_friends, animated: true, completion: nil)
            }
        }
        
        let cancel = UIAlertAction (title:"Cancel".localized, style: .cancel) { (status) in
            
        }
        
        alert_VC.addAction(withApp)
        alert_VC.addAction(intoApp)
        alert_VC.addAction(cancel)
        present(alert_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_appstore(_ sender: UIButton) {
        if let url = URL(string: homemall_appstore_link),UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func whatsappShareLink(_ sender: UIButton) {
        let msg = "\(share_text) \(homemall_appstore_link)"
        let urlWhats = "https://api.whatsapp.com/send?text=\(msg)"
        
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.openURL(whatsappURL as URL)
                } else {
                    print("please install watsapp")
                }
            }
        }
        
    }
    
    @IBAction func btn_twitter_ShareLink(_ sender: UIButton) {
        let msg = share_text
        
        let tweetText = msg
        let tweetUrl = "\(homemall_appstore_link)"
        
        let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)&url=\(tweetUrl)"
        
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if let url = URL(string: escapedShareString),UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        
    }
    
    func func_shadow(view_MyEarnings:AnyObject) {
        if #available(iOS 13.0, *) {
        view_MyEarnings.layer.shadowOpacity = 3.0
        view_MyEarnings.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        view_MyEarnings.layer.shadowRadius = 3.0
        view_MyEarnings.layer.shadowColor = UIColor .lightGray.cgColor
        }
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func func_send_mail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["Homemall.HM1@gmail.com"])
//            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
