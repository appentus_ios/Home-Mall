//  Pay_Method_ViewController.swift
//  Home Mall

//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit


class Pay_Method_ViewController: UIViewController {
    @IBOutlet weak var view_package:UIView!
    
    @IBOutlet weak var txt_mobile_no:UITextField!
    @IBOutlet weak var txt_user_name:UITextField!
    @IBOutlet weak var txt_amt_transfer:UITextField!
    @IBOutlet weak var txt_payment_time:UITextField!
    
    @IBOutlet weak var txt_your_notes:UITextView!
    
    @IBOutlet weak var btn_send:UIButton!
    
    var view_loading = UIView()
    
    var str_packages = ""
    
    let color_1 = UIColor (red: 249.0/255.0, green: 245.0/255.0, blue: 234.0/255.0, alpha: 1.0)
    let color_2 = UIColor (red: 255.0/255.0, green: 98.0/255.0, blue: 205.0/255.0, alpha: 1.0)
    
    @IBOutlet weak var view_date_picker:UIView!
    
    @IBOutlet weak var view_Please_login:UIView!
    @IBOutlet weak var view_Please_login_1:UIView!
    
    @IBOutlet weak var tbl_pay_method:UITableView!
    @IBOutlet weak var coll_packages:UICollectionView!
    
    var arr_select_package = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_your_notes.text = "...Your notes if you have".localized
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        view_loading.isHidden = true
        
        func_set_UI()
        
        func_get_bank_details()
        view_date_picker.isHidden = true
        
        view_Please_login_1.layer.cornerRadius = 4
        view_Please_login_1.clipsToBounds = true
        
        for _ in Model_Ads_Fees.shared.arr_get_packages {
            arr_select_package.append(false)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view_Please_login.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btn_login(_ button:UIButton!) {
        view_Please_login.isHidden = true
        let my_profile = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        my_profile.modalPresentationStyle = .fullScreen
        present(my_profile, animated: true, completion: nil)
    }
    
    @IBAction func btn_cancel(_ button:UIButton!) {
        view_Please_login.isHidden = true
    }

    @IBAction func btn_done_date_picker(_ sender:AnyObject) {
        view_date_picker.isHidden = true
    }
    
    @IBAction func date_picker(_ sender: UIDatePicker) {
        let date = sender.date
        let format = DateFormatter()
        format.dateFormat = "dd-MMM-yyyy hh:mm a"
        format.locale = Locale (identifier: date_localization)
        
        txt_payment_time.text = format.string(from: date)
    }
    
    @IBAction func btn_payment_time(_ sender:AnyObject) {
        view_date_picker.isHidden = false
    }
    
    @IBAction func btn_send(_ sender:AnyObject) {
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
            
            return
        }
        
        if !func_validation() {
            return
        }
        
        Model_Pay_Methods.shared.user_name = txt_user_name.text!
        Model_Pay_Methods.shared.user_mobile = txt_mobile_no.text!
        Model_Pay_Methods.shared.payment_amount = txt_amt_transfer.text!
        Model_Pay_Methods.shared.payment_time = txt_payment_time.text!
        Model_Pay_Methods.shared.payment_note = txt_your_notes.text!
        
        view_loading.isHidden = false
        Model_Pay_Methods.shared.func_user_register { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                
                self.func_ShowHud_Success(with: Model_Pay_Methods.shared.str_msg)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.func_HideHud()
                }
            }
        }
    }
    
    @IBAction func btn_packages(_ sender:UIButton) {
        view_date_picker.isHidden = true
        if sender.tag == 1 {
            str_packages = "1"
            
            let model = Model_Ads_Fees.shared.arr_get_packages[0]
            Model_Pay_Methods.shared.package_id = model.package_id
            
            txt_amt_transfer.text = model.package_price
            
//            btn_packages_1.backgroundColor = color_2
//            btn_packages_2.backgroundColor = color_1
//            btn_packages_1.setTitleColor(UIColor .white, for: .normal)
//            btn_packages_2.setTitleColor(UIColor .black, for: .normal)
        } else {
            str_packages = "2"
            
            let model = Model_Ads_Fees.shared.arr_get_packages[1]
            Model_Pay_Methods.shared.package_id = model .package_id
            
            txt_amt_transfer.text = model.package_price
            
//            btn_packages_1.backgroundColor = color_1
//            btn_packages_2.backgroundColor = color_2
//            btn_packages_2.setTitleColor(UIColor .white, for: .normal)
//            btn_packages_1.setTitleColor(UIColor .black, for: .normal)
        }
    }
    
    @IBAction func btn_back(_ sender:AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func func_get_bank_details() {
        view_loading.isHidden = false
        
        Model_Pay_Methods.shared.func_get_bank_details { (status) in
            DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                self.view_loading.isHidden = true
//                self.func_set_data()
                self.tbl_pay_method.reloadData()
            })
        }
    }
    
    func func_validation() -> Bool {
        if txt_mobile_no.text!.isEmpty {
            func_ShowHud_Error(with: "Enter mobile number".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_user_name.text!.isEmpty {
            func_ShowHud_Error(with: "Enter user name")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if str_packages.isEmpty {
            func_ShowHud_Error(with: "Select a package")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_amt_transfer.text!.isEmpty {
            func_ShowHud_Error(with: "Enter amount of transfer")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_mobile_no.text!.isEmpty {
            func_ShowHud_Error(with: "Enter mobile number")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_payment_time.text!.isEmpty {
            func_ShowHud_Error(with: "Enter payment time")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_your_notes.text == "...Your notes if you have".localized {
            func_ShowHud_Error(with: "Enter your notes")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else {
            return true
        }
        
    }
    
    
    func func_set_data() {
//        let model = Model_Pay_Methods.shared.arr_bank_details[0]
//
//        lbl_bank_name_1_value.text = model.banck_ac_name
//        lbl_bank_no.text = model .bank_ac_no
//        lbl_iban_number_1_value.text = model .bank_iban_no
//
//        let model_1 = Model_Pay_Methods.shared.arr_bank_details[1]
//
//        lbl_bank_name_2_value.text = model_1.banck_ac_name
//        lbl_bank_no_2.text = model_1 .bank_ac_no
//        lbl_iban_number_2_value.text = model .bank_iban_no
    }
    
}



extension Pay_Method_ViewController : UITextViewDelegate,UITextFieldDelegate {
    
    func func_set_UI() {
        view_package.layer.cornerRadius = 10
        view_package.layer.borderWidth = 3
        view_package.layer.borderColor = color_app .cgColor
        view_package.clipsToBounds = true
        
        txt_mobile_no.layer.cornerRadius = 10
        txt_mobile_no.layer.borderColor = UIColor .lightGray.cgColor
        txt_mobile_no.layer.borderWidth = 1
        txt_mobile_no.clipsToBounds = true
        
        txt_user_name.layer.cornerRadius = 10
        txt_user_name.layer.borderColor = UIColor .lightGray.cgColor
        txt_user_name.layer.borderWidth = 1
        txt_user_name.clipsToBounds = true
        
        txt_amt_transfer.layer.cornerRadius = 10
        txt_amt_transfer.layer.borderColor = UIColor .lightGray.cgColor
        txt_amt_transfer.layer.borderWidth = 1
        txt_amt_transfer.clipsToBounds = true
        
        txt_payment_time.layer.cornerRadius = 10
        txt_payment_time.layer.borderColor = color_app.cgColor
        txt_payment_time.layer.borderWidth = 3
        txt_payment_time.clipsToBounds = true
        
        txt_your_notes.layer.cornerRadius = 10
        txt_your_notes.layer.borderColor = UIColor .black.cgColor
        txt_your_notes.layer.borderWidth = 1
        txt_your_notes.clipsToBounds = true
        
        btn_send.layer.cornerRadius = 10
        btn_send.layer.shadowOpacity = 3.0
        btn_send.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_send.layer.shadowRadius = 3.0
        btn_send.layer.shadowColor = color_app.cgColor
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        view_date_picker.isHidden = true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        view_date_picker.isHidden = true
        if textView.text == "...Your notes if you have".localized {
            textView.text = ""
            textView.textColor = UIColor .black
        }
        
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "...Your notes if you have".localized
            textView.textColor = UIColor .darkGray
        }
    }

    
}




extension Pay_Method_ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 360
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Pay_Methods.shared.arr_bank_details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Pay_method_TableViewCell
        
        let model = Model_Pay_Methods.shared.arr_bank_details[indexPath.row]
        
        cell.lbl_bank_name_value.text=model.bank_name
        cell.lbl_iban_number_value.text=model .bank_iban_no
        cell.lbl_bank_no.setTitle(model.bank_ac_no, for: .normal)
        
        if indexPath.row == 0 {
            cell.lbl_bank_name.text = "مصرف الراجحي"
        } else if indexPath.row == 1 {
            cell.lbl_bank_name.text = "البنك الاهلي"
        } else {
            cell.lbl_bank_name.text = "مصرف الراجحي \(indexPath.row+1)"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


//  MARK:- UICollectionView methods
extension Pay_Method_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: 171, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Model_Ads_Fees.shared.arr_get_packages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Pay_Method_CollectionViewCell
        
        let model = Model_Ads_Fees.shared.arr_get_packages[indexPath.row]
        
        coll_Car.btn_pck.setTitle(model.package_name, for: .normal)
        coll_Car.btn_pck.tag = indexPath.row
        coll_Car.btn_pck.addTarget(self, action: #selector(btn_package(_:)), for: .touchUpInside)
        
        if arr_select_package[indexPath.row] {
            coll_Car.btn_pck.setTitleColor(color_2, for: .normal)
//            coll_Car.btn_pck.backgroundColor = color_2
            
            coll_Car.btn_pck.layer.borderColor = color_2.cgColor
            coll_Car.btn_pck.layer.borderWidth = 1
        } else {
            coll_Car.btn_pck.setTitleColor(UIColor .black, for: .normal)
//            coll_Car.btn_pck.backgroundColor = color_1
            
            coll_Car.btn_pck.layer.borderColor = color_1.cgColor
            coll_Car.btn_pck.layer.borderWidth = 1
        }
        
        return coll_Car
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
    }
    
    @IBAction func btn_package(_ sender:UIButton) {
        let model =  Model_Ads_Fees.shared.arr_get_packages[sender.tag]
        txt_amt_transfer.text = model.package_price
        str_packages = "\(sender.tag+1)"
        for i in 0..<Model_Ads_Fees.shared.arr_get_packages.count {
            if i == sender.tag {
                arr_select_package[i] = true
            } else {
                arr_select_package[i] = false
            }
        }
        
        coll_packages.reloadData()
    }
    
}






