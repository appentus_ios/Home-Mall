//
//  Model_Feedback.swift
//  Home Mall
//
//  Created by iOS-Appentus on 31/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation


class Model_Pay_Methods {
    static let shared = Model_Pay_Methods()
    
    var user_name = ""
    var payment_account_number = ""
    var package_id = ""
    var account_of_transfer = ""
    var payment_time = ""
    var payment_note = ""
    var user_mobile = ""
    var payment_amount = ""
    
    var bank_id = ""
    var bank_ac_no = ""
    var banck_ac_name = ""
    var bank_iban_no = ""
    var bank_name = ""
    
    var str_msg = ""
    
    var arr_bank_details = [Model_Pay_Methods]()
    
    func func_get_bank_details(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_bank_details"
        
        API_Home_Mall.func_API_Call_GET(apiName: str_FullURL) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.arr_bank_details.removeAll()
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_bank_details.append(self.func_get_bank_details(dict: dict_json))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_get_bank_details(dict:[String:Any]) -> Model_Pay_Methods {
        let model = Model_Pay_Methods()
        
        model.bank_id = "\(dict["bank_id"] ?? "")"
        model.bank_ac_no = "\(dict["bank_ac_no"] ?? "")"
        model.bank_name = "\(dict["bank_name"] ?? "")"
        model.banck_ac_name = "\(dict["banck_ac_name"] ?? "")"
        model.bank_iban_no = "\(dict["bank_iban_no"] ?? "")"
        
        return model
    }
    
    
    func func_user_register(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"submit_payment"
        
        let params = [
            "user_id":Model_Splash.shared.user_id,
            "user_name":user_name,
            "payment_account_number":payment_account_number,
            "package_id":package_id,
            "account_of_transfer":account_of_transfer,
            "payment_time":payment_time,
            "payment_note":payment_note,
            "user_mobile":user_mobile,
            "payment_amount":payment_amount
        ]
        
        
        print(params)
        
        API_Home_Mall .postAPI(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_msg = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_msg = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    
    
}
