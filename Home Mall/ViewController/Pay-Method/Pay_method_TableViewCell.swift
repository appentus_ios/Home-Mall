





//
//  Pay_method_TableViewCell.swift
//  Home Mall
//
//  Created by iOS-Appentus on 28/02/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Pay_method_TableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_bank_name:UILabel!
    @IBOutlet weak var lbl_bank_no:UIButton!
    
    @IBOutlet weak var lbl_bank_name_value:UILabel!
    @IBOutlet weak var lbl_iban_number_value:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_bank_no.layer.cornerRadius = 10
        lbl_bank_no.layer.shadowOpacity = 1.0
        lbl_bank_no.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        lbl_bank_no.layer.shadowRadius = 3.0
        lbl_bank_no.layer.shadowColor = color_app.cgColor
        
        lbl_bank_name.layer.cornerRadius = 10
//        lbl_bank_name.layer.borderColor = UIColor .black.cgColor
//        lbl_bank_name.layer.borderWidth = 1
        lbl_bank_name.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
