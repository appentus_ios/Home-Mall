
//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 09/01/19.
//  Cop=yright © 2019 appentus. All rights reserved.


import UIKit

var is_from_chat = false

class Chat_User_List_ViewController: UIViewController {

    @IBOutlet weak var view_login_container:UIView!
    @IBOutlet weak var btn_cancel:UIButton!
    @IBOutlet weak var btn_login:UIButton!
    
    @IBOutlet weak var view_Please_login:UIView!
    @IBOutlet weak var tbl_chat:UITableView!
    @IBOutlet weak var lbl_no_chat:UILabel!
    @IBOutlet weak var navbar:UINavigationItem!
    
    var view_loader = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navbar.title = "chat".localized
        
        func_rounded_corner(view: view_login_container)
        func_rounded_corner(view: btn_cancel)
        func_rounded_corner(view: btn_login)
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(true, animated:false)
        NotificationCenter .default.addObserver(self, selector: #selector(func_chat_hide), name: NSNotification.Name.init(rawValue: "chat_hide"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_chat_hide), name:Notification.Name(rawValue: "chat_incoming"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_hide_login_view), name:Notification.Name(rawValue: "hide_login_view"), object: nil)
    }
    
    func func_rounded_corner(view:UIView) {
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
    }
    
    @objc func func_chat_hide() {
        UserDefaults.standard.setValue("", forKey: "count_notifications")
        NotificationCenter.default.post(name: Notification.Name("count_notifications"), object: nil)
        
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            Model_Splash.shared.user_id = "\(dict_LoginData["user_id"] ?? "")"
            
            view_Please_login.isHidden = true
            func_is_user_active()
            
//            func_get_chat_user_list()
        } else {
            view_Please_login.isHidden = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaults.standard.setValue("", forKey: "count_notifications")
        NotificationCenter.default.post(name: Notification.Name("count_notifications"), object: nil)
        
        func_chat_hide()
    }
    
    func func_get_chat_user_list() {
        view_loader.isHidden = false
        Model_Chat_List.shared.func_get_chat_user_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if Model_Chat_List.shared.arr_chat_list.count > 0 {
                    self.tbl_chat.isHidden = false
                    self.lbl_no_chat.isHidden = true
                } else {
                    self.tbl_chat.isHidden = true
                    self.lbl_no_chat.isHidden = false
                }
                
                self.tbl_chat.reloadData()
            }
        }
    }
    
    func func_is_user_active() {
        view_loader.isHidden = false
        Model_Chat_List.shared.func_is_user_active { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if Model_Chat_List.shared.is_user_active {
                    self.func_get_chat_user_list()
                } else {
                    self.func_ShowHud_Error(with:Model_Chat_List.shared.msg)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                        self.func_HideHud()
                        let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                        tabbar.modalPresentationStyle = .fullScreen
                        self.present(tabbar, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ button:Any!) {
        presentingViewController?.dismiss(animated: false, completion: nil)
        NotificationCenter .default.post(name: NSNotification.Name .init(rawValue: "login_chat"), object: nil)
    }
    
    @IBAction func btn_login(_ button:UIButton!) {
        is_myshop = false
        is_chat_login = true
        view_Please_login.isHidden = true
        is_from_chat = true
        
        let my_profile = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        my_profile.modalPresentationStyle = .fullScreen
        present(my_profile, animated: true, completion: nil)
    }
    
    @IBAction func btn_cancel(_ button:UIButton!) {
        presentingViewController?.dismiss(animated: false, completion: nil)
        func_hide_login_view()
    }
    
    @objc func func_hide_login_view() {
        view_Please_login.isHidden = true
        NotificationCenter .default.post(name: NSNotification.Name .init(rawValue: "login_chat"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        is_from_chat = false
    }
    
}



extension Chat_User_List_ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Chat_List.shared.arr_chat_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Chat_User_List_TableViewCell
        
        let model = Model_Chat_List.shared.arr_chat_list[indexPath .row]
        
        cell.lbl_user_name.text = model .name
        cell.lbl_date.text = model .date
        cell.lbl_last_msg.text = model.msg
        
        if model.user_role == "2" {
//            user_role == 2 for adming
            cell.img_user.image = UIImage (named: "ic_home.png")
        } else {
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.height/2
            cell.img_user.clipsToBounds = true
            
            cell.img_user.sd_setShowActivityIndicatorView(true)
            cell.img_user.sd_setIndicatorStyle(.gray)
            cell.img_user.sd_setImage(with:URL (string: model.profile), placeholderImage:(UIImage(named:"user-gray.png")))
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = Model_Chat_List.shared.arr_chat_list[indexPath .row]
        
        Model_Chat_List.shared.user_role = model .user_role
        Model_Chat_Message_Fri.shared.fri_id = model .fri_id
        if model.name.isEmpty {
            Model_Chat_Message_Fri.shared.fri_name = model.user_mobile
        } else {
            Model_Chat_Message_Fri.shared.fri_name = model.name
        }
        
        Model_Chat_Message_Fri.shared.shop_id = model.shop_id
        
        let chat_message = storyboard?.instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController") as! Chat_Message_Fri_ViewController
        chat_message.modalPresentationStyle = .fullScreen
        present(chat_message, animated: true, completion: nil)
    }
    
}





