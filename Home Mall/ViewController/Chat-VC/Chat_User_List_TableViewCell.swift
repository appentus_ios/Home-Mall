//
//  Chat_TableViewCell.swift
//  Home Mall
//
//  Created by iOS-Appentus on 09/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit


class Chat_User_List_TableViewCell: UITableViewCell {
    @IBOutlet weak var img_user:UIImageView!
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var view_live_or_offline:UIView!
    
    @IBOutlet weak var lbl_user_name:UILabel!
    @IBOutlet weak var lbl_date:UILabel!
    @IBOutlet weak var lbl_last_msg:UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        view_live_or_offline.layer.cornerRadius = view_live_or_offline.frame.size.height/2
        view_live_or_offline.clipsToBounds = true
        
        view_container.layer.shadowOpacity = 3.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 3.0
        view_container.layer.shadowColor = UIColor .darkGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
}


