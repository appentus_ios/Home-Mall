//
//  Model_Chat_List.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 27/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.


import Foundation
import Alamofire


class Model_Chat_List {
    static let shared = Model_Chat_List()
    
    var is_user_active = false
    
    var fri_id = ""
    
    var date = ""
    var time = ""
    var msg = ""
    var user_role = ""
    var name = ""
    var user_mobile = ""
    var profile = ""
    var shop_id = ""
    
    var str_msg_response = ""
    
    var arr_chat_list = [Model_Chat_List]()
    
    func func_get_chat_user_list(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+"get_chat_user_list"
        
        let str_Params = [
                        "user_id":Model_Splash.shared.user_id,
                    ]
        print(str_Params)
        
        API_Home_Mall.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_chat_list.removeAll()
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_chat_list.append(self.func_set_chat_data(dict: dict_json))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    private func func_set_chat_data(dict:[String:Any]) -> Model_Chat_List {
        print(dict)
        
        let model = Model_Chat_List()
        
        let str_fri_id = dict["fri_id"] as! String
        
        if str_fri_id == Model_Splash.shared.user_id {
            model.fri_id = dict["user_id"] as! String
        } else {
            model.fri_id = dict["fri_id"] as! String
        }
        
        model.msg = dict["msg"] as! String
        model.time = dict["time"] as! String
        model.date = dict["date"] as! String
        model.shop_id = "\(dict["shop_id"] ?? "")"
        
        if let dict_user_detail = dict["user_detail"] as? [String:Any] {
            if let name = dict_user_detail["user_name"] as? String {
                model.name = name
            }
            
            if let user_mobile = dict_user_detail["user_mobile"] as? String {
                model.user_mobile = user_mobile
            }
            
            if let customer_profile = dict_user_detail["user_profile"] as? String {
                model.profile = customer_profile
            }
            
            model.user_role = "\(dict_user_detail["user_role"] ?? "")"
        }
        
        return model
    }
    
    
    func func_is_user_active(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"is_user_active"
        //        print(str_FullURL)
        
        let params = [
            "user_id":Model_Splash.shared.user_id
        ]
        
        //        print(params)
        API_Home_Mall .postAPI_check_login_data(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.is_user_active = true
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if "\(dict_JSON["message"]!)" != "Error In API" || "\(dict_JSON["message"]!)" == "user not exists" {
                    self.msg = "This user is delete from Admin"
                    self.is_user_active = false
                    UserDefaults.standard.removeObject(forKey: "login_Data")
                }
                
                if let _ = dict_JSON["status"] as? String {
                    completionHandler(dict_JSON["status"] as! String)
                } else {
                    completionHandler("failed")
                }
                
            }
            
        }
    }
    
    
    
}




