//
//  Model_SignIn.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire

class Model_Chat_Message_Fri {
    static let shared = Model_Chat_Message_Fri()
    
    var fri_name = ""
    var fri_img = ""
    
    var user_id = ""
    var fri_id = ""
    
    var date = ""
    var time = ""
    var msg = ""
    
    var str_msg_response = ""
    
    var arr_chat_list = [Model_Chat_Message_Fri]()
    
    var fri_blocked_me = ""
    var me_blocked_fri = ""
    
    func func_chat_msg(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+"get_user_chat"
        
        let str_Params = [
            "user_id":Model_Splash.shared.user_id,
            "fri_id":fri_id
        ]
        print(str_Params)
        
        API_Home_Mall.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)

            if dict_JSON["status"] as? String == "success" {
                self.arr_chat_list.removeAll()
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_chat_list.append(self.func_set_chat_data(dict: dict_json))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {

                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }

    }
    
    private func func_set_chat_data(dict:[String:Any]) -> Model_Chat_Message_Fri {
        let model = Model_Chat_Message_Fri()
        
        model.date = dict["date"] as! String
        model.msg = dict["msg"] as! String
        model.time = dict["time"] as! String
        model.user_id = dict["user_id"] as! String
        model.fri_id = dict["fri_id"] as! String
        
        return model
    }
    
    var msg_send = ""
    var shop_id = ""
    
    func func_insert_chat(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+"insert_chat"
        
        let date = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.locale = Locale (identifier: date_localization)
        
        let formatter_time = DateFormatter()
        formatter_time.dateFormat = "hh:mm a"
        formatter_time.locale = Locale (identifier: date_localization)
        
        let str_date = formatter.string(from: date)
        let str_time = formatter_time.string(from: date)
        
        let str_Params = [
            "user_id":Model_Splash.shared.user_id,
            "fri_id":"\(fri_id)",
            "msg":"\(msg_send)",
            "type":"1",
            "date":str_date,
            "time":str_time,
            "shop_id":shop_id
        ]
        print(str_Params)
        
        API_Home_Mall.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_msg_response = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    func func_get_block_user_status(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+"get_block_user_status"
        
        let str_Params = [
            "user_block_id":Model_Splash.shared.user_id,
            "fri_blocked_id":fri_id
        ]
        print(str_Params)
        
        API_Home_Mall.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if let dict_user_block = dict_JSON as? [String:Any] {
                print(dict_user_block)
                
                self.fri_blocked_me = "\(dict_user_block["fri_blocked_me"] ?? "")"
                self.me_blocked_fri = "\(dict_user_block["me_blocked_fri"] ?? "")"
                
                completionHandler("success")
            } else {
                completionHandler("fail")
            }
        }
        
    }
    
    func func_do_block(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+"do_block"
        
        let str_Params = [
            "user_block_id":Model_Splash.shared.user_id,
            "fri_blocked_id":fri_id
        ]
        print(str_Params)
        
        API_Home_Mall.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if let dict_user_block = dict_JSON as? [String:Any] {
                print(dict_user_block)
                
                self.fri_blocked_me = "\(dict_user_block["fri_blocked_me"] ?? "")"
                self.me_blocked_fri = "\(dict_user_block["me_blocked_fri"] ?? "")"
                
                completionHandler("success")
            } else {
                completionHandler("fail")
            }
        }
        
    }

    
    func func_unblock(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+"unblock"
        
        let str_Params = [
            "user_block_id":Model_Splash.shared.user_id,
            "fri_blocked_id":fri_id
        ]
        print(str_Params)
        
        API_Home_Mall.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if let dict_user_block = dict_JSON as? [String:Any] {
                print(dict_user_block)
                
                self.fri_blocked_me = "\(dict_user_block["fri_blocked_me"] ?? "")"
                self.me_blocked_fri = "\(dict_user_block["me_blocked_fri"] ?? "")"
                completionHandler("success")
            } else {
                completionHandler("fail")
            }
        }
        
    }

    
    
}



