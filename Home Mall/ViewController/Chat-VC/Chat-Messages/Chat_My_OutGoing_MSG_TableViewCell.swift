//
//  Chat_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 26/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Chat_My_OutGoing_MSG_TableViewCell: UITableViewCell {
    @IBOutlet weak var view_bubble:UIView!
    
    @IBOutlet weak var width_layout: NSLayoutConstraint!
    @IBOutlet weak var width_layout_view_bubble: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_msg:UILabel!
    @IBOutlet weak var lbl_msg_time:UILabel!
    
    @IBOutlet weak var lbl_msg_time_uper:UILabel!

    @IBOutlet weak var btn_GoDetails:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_msg_time_uper.layer.cornerRadius = lbl_msg_time_uper.frame.size.height/2
        lbl_msg_time_uper.clipsToBounds = true
        
        view_bubble.layer.cornerRadius = 2
        view_bubble.clipsToBounds = true
        
        lbl_msg.layer.cornerRadius = 2
        lbl_msg.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
