


import UIKit
import Alamofire

import NextGrowingTextView


let color_AppDefault = UIColor (red: 224.0/255.0, green: 59.0/255.0, blue: 11.0/255.0, alpha: 1.0)
var dict_userdetails = [String:Any]()
var dict_ride_details_1 = [String:Any]()



var str_fri_ID = ""
var is_logged_in = false



class Chat_Message_Fri_ViewController: UIViewController {
//    @IBOutlet weak var txt_msg:UITextView!
    @IBOutlet weak var txt_msg: NextGrowingTextView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var tbl_chat:UITableView!
    @IBOutlet weak var btn_send:UIButton!
    
    @IBOutlet weak var lbl_user_name:UILabel!
    @IBOutlet weak var lbl_last_update:UILabel!
    
    @IBOutlet weak var btn_block:UIButton!
    @IBOutlet weak var width_block:NSLayoutConstraint!
    
    @IBOutlet weak var view_down:UIView!
    @IBOutlet weak var btn_more:UIButton!
    
    var view_loading = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let localized = "... Write here".localized
        self.txt_msg.layer.cornerRadius = 4
        self.txt_msg.backgroundColor = UIColor.clear
        self.txt_msg.textView.textAlignment = .right
        
        
        
        
        self.txt_msg.textView.font = UIFont (name:  "Lato-Regular", size:  22.0)
        self.txt_msg.textView.text = localized
        
//        self.txt_msg.placeholderAttributedText = NSAttributedString(
//            string: localized,
//            attributes: [
//                .font: UIFont (name:  "Lato-Regular", size:  22.0)
//            ]
//        )
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name:Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name:Notification.Name.UIKeyboardWillHide, object: nil)
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
//        self.txt_msg.text = localized
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        view_loading.isHidden = true
        
        Model_Chat_Message_Fri.shared.arr_chat_list.removeAll()
        
        func_get_block_user_status()
        
        btn_block.layer.cornerRadius = 4
        
        btn_block.layer.shadowOpacity = 3.0
        btn_block.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        btn_block.layer.shadowRadius = 3.0
        btn_block.layer.shadowColor = UIColor .lightGray.cgColor
        
        width_block.constant = 0
            
        NotificationCenter.default.addObserver(self, selector: #selector(func_chat_msg), name:Notification.Name(rawValue: "chat_incoming"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_get_block_user_status), name:Notification.Name(rawValue: "Unblocked_block"), object: nil)
        
        txt_msg.layer.cornerRadius = 6
        txt_msg.clipsToBounds = true
        
        lbl_user_name.text = Model_Chat_Message_Fri.shared.fri_name
        
        if Model_Chat_List.shared.user_role == "2" {
            btn_more.isUserInteractionEnabled = false
        } else {
            btn_more.isUserInteractionEnabled = true
        }
        
        let gesture = UITapGestureRecognizer (target: self, action: #selector(func_tap))
        tbl_chat.addGestureRecognizer(gesture)
    }
    
    @objc func func_tap() {
        self.view.endEditing(true)
        width_block.constant = 0
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear], animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        Model_Chat_Message_Fri.shared.arr_chat_list.removeAll()
    }
    
    @IBAction func btn_back(_ sender:Any) {
      dismiss(animated: true, completion: nil)
    }
    
    @objc func func_get_block_user_status() {
//        view_loading.isHidden = false
        Model_Chat_Message_Fri.shared.func_get_block_user_status { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.func_chat_msg()
                
                if Model_Chat_Message_Fri.shared.me_blocked_fri == "1" {
                    self.btn_block.isHidden = false
                    self.view_down.isHidden = true
                    self.btn_block.setTitle("Unblock".localized, for: .normal)
                } else if Model_Chat_Message_Fri.shared.fri_blocked_me == "1" {
                    self.btn_block.isHidden = true
                    self.view_down.isHidden = true
//                    self.btn_block.setTitle("You are blocked", for: .normal)
                } else {
                    self.btn_block.setTitle("Block".localized, for: .normal)
                    self.view_down.isHidden = false
                    self.btn_block.isHidden = false
                }
                
            }
        }
    }

    @objc func func_do_block() {
//        view_loading.isHidden = false
        Model_Chat_Message_Fri.shared.func_do_block { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.view_down.isHidden = true
                self.btn_block.setTitle("Unblock".localized, for: .normal)
            }
        }
    }
    
    
    @objc func func_unblock() {
//        view_loading.isHidden = false
        Model_Chat_Message_Fri.shared.func_unblock { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.view_down.isHidden = false
                self.btn_block.setTitle("Block".localized, for: .normal)
            }
        }
    }

    
    @objc func func_chat_msg() {
        UserDefaults.standard.setValue("", forKey: "count_notifications")
        NotificationCenter.default.post(name: Notification.Name("count_notifications"), object: nil)
        
//        view_loading.isHidden = false
        Model_Chat_Message_Fri.shared.func_chat_msg { (status) in
            DispatchQueue.main.async {
               self.view_loading.isHidden = true
                self.tbl_chat.reloadData()
                
                if Model_Chat_Message_Fri.shared.arr_chat_list.count > 1 {
                    let indexPath = IndexPath (row:Model_Chat_Message_Fri.shared.arr_chat_list.count-1, section: 0)
                    self.tbl_chat .scrollToRow(at:indexPath , at: UITableViewScrollPosition.bottom, animated: true)
                }
                
                if Model_Chat_Message_Fri.shared.arr_chat_list.count > 0 {
                    let model = Model_Chat_Message_Fri.shared.arr_chat_list[Model_Chat_Message_Fri.shared.arr_chat_list.count-1]
                    let localized = "last update in".localized
                    self.lbl_last_update.text = "\(localized) \(model.date), \(model.time)"
                }
            }
        }
    }
    
    @IBAction func btn_send_msg (_ sender:UIButton) {
        self.view.endEditing(true)
        
        width_block.constant = 0
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear], animations: {
            self.view.layoutIfNeeded()
        })
        
        let localized_1 = "text a message".localized
        
        let str_msg = txt_msg.textView.text.trimmingCharacters(in: CharacterSet .whitespacesAndNewlines)
        print(str_msg)
        
        if str_msg.isEmpty {
            func_ShowHud_Error(with: localized_1)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        }
        
        let localized = "... Write here".localized
        if txt_msg.textView.text == "" || txt_msg.textView.text == localized {
            func_ShowHud_Error(with: localized_1)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        }
        
//        view_loading.isHidden = false
        
//        let localized = "... Write here".localized
        
        Model_Chat_Message_Fri.shared.msg_send = txt_msg.textView.text!
        self.txt_msg.textView.text = localized
        self.txt_msg.textView.textColor = UIColor .darkGray
        
        Model_Chat_Message_Fri.shared.func_insert_chat { (status) in
            DispatchQueue.main.async {
               self.view_loading.isHidden = true
                if status == "success" {
                    self.func_chat_msg()
                } else {
                    self.func_ShowHud_Error(with: Model_Chat_Message_Fri.shared.str_msg_response)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        self.func_HideHud()
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func btn_block(_ sender:UIButton) {
        if view_down.isHidden {
            func_unblock()
        } else {
            func_do_block()
        }
    }
    
    @IBAction func btn_more(_ sender:UIButton) {
        sender.isSelected = !(sender.isSelected)
        if btn_block.isHidden {
            let alert = UIAlertController (title: "Blocked".localized, message: "You are blocked by this user".localized, preferredStyle: .alert)
            let no = UIAlertAction(title: "ok".localized, style: .default) { (yes) in
                
            }
            alert.addAction(no)
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        if width_block.constant == 0 {
            width_block.constant = 130
        } else {
            width_block.constant = 0
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear], animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
}



extension Chat_Message_Fri_ViewController : UITableViewDelegate,UITableViewDataSource {
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = Model_Chat_Message_Fri.shared.arr_chat_list[indexPath.row]
        var messageText = ""
            
        if indexPath.row == 0 {
            messageText = " بخصوص إعلانك رقم "+"#\(Model_Chat_Message_Fri.shared.shop_id) "+"\n\n\(model.msg)"
        } else {
            messageText = model.msg
        }
            
        if model.user_id != Model_Splash.shared.user_id {
            let width = tbl_chat.frame.size.width-80
            if func_size(width: Int(width), messageText: messageText).height < 60 {
                return 60+50
            }
            return func_size(width: Int(width), messageText: messageText).height+20+50
        } else {
            let width = tbl_chat.frame.size.width-30
            if func_size(width: Int(width), messageText: messageText).height < 60 {
                return 60+50
            }
            return func_size(width: Int(width), messageText: messageText).height+20+50
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Chat_Message_Fri.shared.arr_chat_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = Model_Chat_Message_Fri.shared.arr_chat_list[indexPath.row]
        if model.user_id == Model_Splash.shared.user_id {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-Incoming-msg", for: indexPath) as! Chat_Incoming_MSG_TableViewCell
            
            let str_msg = model.msg
//            cell.lbl_msg?.text = str_msg
            cell.lbl_msg_time.text = "\(model.date.date_localized) \(model.time)"//.date_time_localized
            cell.lbl_msg_time_uper.text = model.time//.time_localized
            
            let width = cell.frame.size.width-80
            cell.width_layout.constant = func_size(width: Int(width), messageText: str_msg).width
            
            if cell.width_layout.constant < 120 {
               cell.width_layout.constant = 125
            } else {
                cell.width_layout.constant = func_size(width: Int(width), messageText: str_msg).width
            }
            
            cell.width_layout_view_bubble.constant = cell.width_layout.constant+20
            
            if indexPath.row == 0 {
                cell.width_layout.constant += 50
                
                let msg_send = " بخصوص إعلانك رقم #\(Model_Chat_Message_Fri.shared.shop_id)" + "\n\n\(model.msg)"
                
                let arr_MSG = msg_send.components(separatedBy: "#")
                let main_string = msg_send
                let string_to_color = arr_MSG[1].components(separatedBy: "\n")[0]
                
                let range = (main_string as NSString).range(of: string_to_color)
                let attributedString = NSMutableAttributedString(string:main_string)
                attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: range)
                
                cell.lbl_msg?.attributedText = attributedString
            } else {
                cell.lbl_msg?.text = str_msg
            }
            cell.btn_GoDetails.addTarget(self, action: #selector(btn_ShopDetails(_:)), for: .touchUpInside)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_my_outGoing_msg", for: indexPath) as! Chat_My_OutGoing_MSG_TableViewCell
            
            let str_msg = model.msg
            cell.lbl_msg?.text = str_msg
            cell.lbl_msg_time.text = "\(model.date.date_localized) \(model.time)"//.date_time_localized
            cell.lbl_msg_time_uper.text = model.time//.time_localized
            
            let width = cell.frame.size.width-30
            cell.width_layout.constant = func_size(width: Int(width), messageText: str_msg).width
            if cell.width_layout.constant < 120 {
                cell.width_layout.constant = 125
            } else {
                cell.width_layout.constant = func_size(width: Int(width), messageText: str_msg).width
            }
            
            cell.width_layout_view_bubble.constant = cell.width_layout.constant+20
            
            if indexPath.row == 0 {
                let msg_send = " بخصوص إعلانك رقم #\(Model_Chat_Message_Fri.shared.shop_id)" + "\n\n\(model.msg)"
                
                let arr_MSG = msg_send.components(separatedBy: "#")
                let main_string = msg_send
                let string_to_color = arr_MSG[1].components(separatedBy: "\n")[0]
                
                let range = (main_string as NSString).range(of: string_to_color)
                let attributedString = NSMutableAttributedString(string:main_string)
                attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: range)
                
                cell.lbl_msg?.attributedText = attributedString
            } else {
                cell.lbl_msg?.text = str_msg
            }
            
            cell.btn_GoDetails.addTarget(self, action: #selector(btn_ShopDetails(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func btn_ShopDetails(_ sender:UIButton) {
        Model_Details.shared.index_selected = 0
        Model_Details.shared.shop_id = Model_Chat_Message_Fri.shared.shop_id
            
        let fav_VC = storyboard?.instantiateViewController(withIdentifier: "Details_ViewController") as! Details_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        present(fav_VC, animated: true, completion: nil)
    }
}



extension Chat_Message_Fri_ViewController :UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        let localized = "... Write here".localized
        if textView.text == localized {
            textView.textColor = UIColor .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let localized = "... Write here".localized
        if textView.text == "" {
            textView.textColor = UIColor .darkGray
            textView.text = localized
        }
    }
    
    
    
    @objc func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
//            if let _ = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.inputContainerViewBottom.constant =  0
                //textViewBottomConstraint.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    
    
    @objc func keyboardWillShow(_ sender: Notification) {
        self.txt_msg.textView.text = ""
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    
}



func func_size(width:Int,messageText:String) -> CGSize {
    let size = CGSize(width:width, height: 1000)
    let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
    let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont (name: "Lato-Regular", size: 18.0) ?? UIFont .systemFont(ofSize: 18.0)], context: nil)
    
    return CGSize(width:estimatedFrame.width, height: estimatedFrame.height+20)
}


