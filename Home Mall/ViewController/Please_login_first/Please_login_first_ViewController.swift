//
//  Please_login_first_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Please_login_first_ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btn_cancel (_ sender:UIButton) {
//        NotificationCenter .default.post(name: NSNotification.Name (rawValue: "for_login"), object: nil)
        let my_profilee = storyboard?.instantiateViewController(withIdentifier: "Please_login_first_ViewController") as! Please_login_first_ViewController
        
        my_profilee.view.isHidden = true
        
        my_profilee.remove()
//        remove(vc: my_profilee)
        
        if is_myshop_login {
//            onSlideMenuButtonPressed(sender)
        }
        
    }
    
    @IBAction func btn_Login (_ sender:Any) {
        let login_VC = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        login_VC.modalPresentationStyle = .fullScreen
        present(login_VC, animated: true, completion: nil)
    }

}


