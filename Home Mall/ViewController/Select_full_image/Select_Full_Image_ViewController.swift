//  Select_Full_Image_ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 14/05/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit

class Select_Full_Image_ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        dismiss(animated: true, completion: nil)
    }

}



//  MARK:- UICollectionView methods
extension Select_Full_Image_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize (width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Model_Details.shared.arr_shop_images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let model = Model_Details.shared.arr_shop_images[indexPath.row]
        
        let img_ad = cell.viewWithTag(1) as! UIImageView
        let shop_image_tag = cell.viewWithTag(2) as! UILabel
        
        img_ad.sd_setShowActivityIndicatorView(true)
        img_ad.sd_setIndicatorStyle(.gray)
        img_ad.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"")))
        
        shop_image_tag.text = model.shop_image_tag
        
        if model.shop_image_tag.isEmpty {
            shop_image_tag.isHidden = true
        } else {
            shop_image_tag.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}


