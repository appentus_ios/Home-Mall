//
//  Sub_Category_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Sub_Category_ViewController: UIViewController {
    @IBOutlet weak var tbl_sub_category:UITableView!
    var arr_select = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Model_Category_List.shared.arr_sub_category_list.removeAll()
        arr_select.removeAll()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "user_exists"), object: nil)
        
        func_get_sub_category()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func func_shop_Count() {
        if Model_My_Ads.shared.is_edit {
            func_ShowHud_Error(with:Model_Tabbar.shared.message)
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.func_HideHud()
                let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                tabbar.modalPresentationStyle = .fullScreen
                self.present(tabbar, animated: true, completion: nil)
//                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }

    func func_get_sub_category() {
        let view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_sub_category { (status) in
            DispatchQueue.main.async {
                view_loader.isHidden = true
                if status == "success" {
                    self.tbl_sub_category.isHidden = false
                    for i in 0..<Model_Category_List.shared.arr_sub_category_list.count {
                        self.arr_select.append(false)
                        
                        if Model_My_Ads.shared.is_edit {
                            let model_cate = Model_Category_List.shared.arr_sub_category_list[i]
                            if Model_My_Ads.shared.is_edit {
                                let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
                                
                                if model.subcate_code == model_cate.subcate_code {
                                    self.arr_select[i] = true
                                }
                            }
                        }
                    }
                } else {
                    self.tbl_sub_category.isHidden = true
                }
                self.tbl_sub_category.reloadData()
            }
        }
    }
    
}



extension Sub_Category_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Category_List.shared.arr_sub_category_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lbl_cate_name  = cell .viewWithTag(1) as! UILabel
        let view_pink  = cell .viewWithTag(2) as! UIView
        
        let model = Model_Category_List.shared.arr_sub_category_list[indexPath.row]
        lbl_cate_name.text = "\(model.subcate_name)"
        
        if arr_select[indexPath.row] {
            view_pink.backgroundColor = color_app
            lbl_cate_name.textColor = UIColor .white
        } else {
            view_pink.backgroundColor = UIColor .white
            lbl_cate_name.textColor = UIColor .black
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = Model_Category_List.shared.arr_sub_category_list[indexPath.row]
        Model_Ad_Shop.shared.subcate_code = model.subcate_code
        Model_Ad_Shop.shared.subcate_name = model.subcate_name
        
        for i in 0..<Model_Category_List.shared.arr_sub_category_list.count {
            if i == indexPath.row {
                arr_select[i] = true
            } else {
                arr_select[i] = false
            }
        }
        
        tbl_sub_category.reloadData()
        
        let sub_cate = storyboard?.instantiateViewController(withIdentifier: "Add_image_ViewController") as! Add_image_ViewController
        sub_cate.modalPresentationStyle = .fullScreen
        present(sub_cate, animated: true, completion: nil)
    }
    
}
