//
//  Tell_Your_Friends_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 21/02/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import WebKit


var navigation_title_tell_your_friends = ""
var str_tell_your_friends = ""



class Tell_Your_Friends_ViewController: UIViewController ,UIWebViewDelegate {
    
    @IBOutlet weak var navBar:UINavigationItem!
    @IBOutlet weak var webView:UIWebView!
    
    var view_loading = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBar.title = navigation_title_tell_your_friends
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        view_loading.isHidden = true
        
        let url = URL(string: str_tell_your_friends)
        webView.loadRequest((URLRequest(url: url!)))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        view_loading.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        view_loading.isHidden = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("error is:-",error)
        view_loading.isHidden = true
    }
    
}
