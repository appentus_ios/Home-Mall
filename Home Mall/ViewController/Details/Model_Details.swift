//
//  Model_Details.swift
//  Home Mall
//
//  Created by iOS-Appentus on 31/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation

class Model_Details {
    static let shared = Model_Details()
    
    var arr_details = [Model_Search_VC]()
    
    var last_count = ""
    var shop_id = ""
    
    var index_selected = -1
    
    var shop_image_id = ""
    var shop_image = ""
    var shop_image_tag = ""
    
    var arr_shop_images = [Model_Details]()
    
    var alert_id = ""
    var alert_name = ""
    
    var convict_user_id = ""
    
    var arr_get_alert_type = [Model_Details]()
    
    func func_view_count(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"view_count"
        let params = "last_count=\(last_count)&shop_id=\(shop_id)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {

                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
        
    var feedback_cmt = ""
    var receiver_shop_id = ""
    var feedback_rating = ""
    var str_message = ""
    
    func func_do_feedback(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"do_feedback"
        let str_params = "sender_user_id=\(Model_Splash.shared.user_id)&receiver_shop_id=\(shop_id)&feedback_cmt=&feedback_rating=\(feedback_rating)"
        print(str_params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
//                self.str_message = dict_JSON["message"] as! String
                self.str_message = "feedbacksubmit".localized
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }

    
    
    func func_get_shop_detail(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_detail"
        let params = "shop_id=\(shop_id)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_shop_images.removeAll()
            if dict_JSON["status"] as? String == "success" {
                
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_details.append(self.func_set_Category(dict: dict_json))
                }
                if let arr_images = dict_JSON["images"] as? [[String:Any]] {
                    for dict_json in arr_images {
                        self.arr_shop_images.append(self.func_ser_shop_images(dict: dict_json))
                    }
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_ser_shop_images(dict:[String:Any]) -> Model_Details {
        let model = Model_Details()
        
        model.shop_image_id = "\(dict["shop_image_id"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        model.shop_image_tag = "\(dict["shop_image_tag"] ?? "")"
        model.shop_id = "\(dict["shop_id"] ?? "")"

        return model
    }
    
    func func_get_alert_type(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_alert_type"
        
        API_Home_Mall.func_API_Call_GET(apiName: str_FullURL) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_alert_type.removeAll()
            if dict_JSON["status"] as? String == "success" {
                if let arr_images = dict_JSON["result"] as? [[String:Any]] {
                    for dict_json in arr_images {
                        self.arr_get_alert_type.append(self.func_get_alert_type(dict: dict_json))
                    }
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_get_alert_type(dict:[String:Any]) -> Model_Details {
        let model = Model_Details()
        
        model.alert_id = "\(dict["alert_id"] ?? "")"
        model.alert_name = "\(dict["alert_name"] ?? "")"
        
        return model
    }
    
    func func_do_report(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"do_report"
        let params = "user_id=\(Model_Splash.shared.user_id)&convict_user_id=\(convict_user_id)&shop_id=\(shop_id)&alert_id=\(alert_id)&message="
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }

    
    
    private func func_set_Category(dict:[String:Any]) -> Model_Search_VC {
        let model = Model_Search_VC()
        
        model.shop_id = "\(dict["shop_id"] ?? "")"
        model.user_id = "\(dict["user_id"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        
        let str_shop_lat = "\(dict["shop_lat"] ?? "")"
        model.shop_lat = str_shop_lat.trimmingCharacters(in: .whitespaces)
        
        let str_shop_lang = "\(dict["shop_lang"] ?? "")"
        model.shop_lang = str_shop_lang
        
        model.shop_location = "\(dict["shop_location"] ?? "")"
        model.shop_add_date = "\(dict["shop_add_date"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        model.subcate_code = "\(dict["subcate_code"] ?? "")"
        model.shop_near_by = "\(dict["shop_near_by"] ?? "")"
        model.shop_address = "\(dict["shop_address"] ?? "")"
        model.shop_status = "\(dict["shop_status"] ?? "")"
        
        model.user_name = "\(dict["user_name"] ?? "")"
        model.user_country_code = "\(dict["user_country_code"] ?? "")"
        model.user_mobile = "\(dict["user_mobile"] ?? "")"
        
        model.user_email = "\(dict["user_email"] ?? "")"
        model.user_join_date = "\(dict["user_join_date"] ?? "")"
        model.user_profile = "\(dict["user_profile"] ?? "")"
        model.user_shop_id = "\(dict["user_shop_id"] ?? "")"
        model.user_device_type = "\(dict["user_device_type"] ?? "")"
        model.user_device_token = "\(dict["user_device_token"] ?? "")"
        
        model.user_password = "\(dict["user_password"] ?? "")"
        model.user_status = "\(dict["user_status"] ?? "")"
        model.category_id = "\(dict["category_id"] ?? "")"
        model.category_name = "\(dict["category_name"] ?? "")"
        
        model.category_status = "\(dict["category_status"] ?? "")"
        model.subcate_id = "\(dict["subcate_id"] ?? "")"
        model.subcate_name = "\(dict["subcate_name"] ?? "")"
        model.subcate_status = "\(dict["subcate_status"] ?? "")"
        model.distance = "\(dict["distance"] ?? "")"
        model.shop_name = "\(dict["shop_name"] ?? "")"
        model.shop_about = "\(dict["shop_about"] ?? "")"
        model.view_count = "\(dict["view_count"] ?? "")"
        model.avg_rating = "\(dict["avg_rating"] ?? "")"
        
        model.fav_id = "\(dict["fav_id"] ?? "")"
        
        model.feedback_id = "\(dict["feedback_id"] ?? "")"
        model.feedback_comment = "\(dict["feedback_comment"] ?? "")"
        model.sender_user_id = "\(dict["sender_user_id"] ?? "")"
        model.receiver_shop_id = "\(dict["receiver_shop_id"] ?? "")"
        model.feedback_rating = "\(dict["feedback_rating"] ?? "")"
        
        return model
    }
}



