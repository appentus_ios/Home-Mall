//  Details_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 10/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import SDWebImage
import GoogleMaps
import ASStarRatingView
import SDWebImage
import Social
import CoreLocation



let homemall_appstore_link = "https://apps.apple.com/us/app/home-mall/id1464653434"



class Details_ViewController: UIViewController {
    @IBOutlet weak var btn_previous:UIButton!
    @IBOutlet weak var btn_next:UIButton!
    @IBOutlet weak var btn_favorite:UIButton!
    
    @IBOutlet weak var view_message:UIView!
    
    @IBOutlet weak var nav_bar:UINavigationItem!
    
    @IBOutlet weak var btn_continue:UIButton!
    
    @IBOutlet weak var map_view:GMSMapView!
    @IBOutlet weak var star:ASStarRatingView!
    
    @IBOutlet weak var lbl_Ad_name:UILabel!

    @IBOutlet weak var lbl_shop_address:UILabel!
    @IBOutlet weak var img_shopImage:UIImageView!
    
    @IBOutlet weak var lbl_category:UILabel!
    @IBOutlet weak var lbl_sub_category:UILabel!
    
    @IBOutlet weak var lbl_views:UILabel!
    @IBOutlet weak var lbl_last_date:UILabel!
    @IBOutlet weak var lbl_about_ad:UILabel!
    
    @IBOutlet weak var img_user:UIImageView!
    
    @IBOutlet weak var lbl_user_name:UILabel!
    @IBOutlet weak var lbl_user_mobile:UILabel!
    @IBOutlet weak var lbl_add_number:UILabel!
    
    @IBOutlet weak var view_rating:UIView!
    @IBOutlet weak var txt_comment_for_rating:UITextView!
    @IBOutlet weak var star_rating:ASStarRatingView!
    @IBOutlet weak var btn_submit:UIButton!
    @IBOutlet weak var view_rating_1:UIView!
    
    @IBOutlet weak var coll_shop_images:UICollectionView!
    @IBOutlet weak var view_shop_images:UIView!
    
    @IBOutlet weak var view_Please_login:UIView!
    @IBOutlet weak var view_Please_login_1:UIView!
    
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    var co_Ordinate_shop = CLLocationCoordinate2DMake(0.0, 0.0)
    
    var is_updated_location = false
    
    var view_loading = UIView()
    
    var ad_name = ""
    var ad_details = ""
    var image_link = ""
    
    @IBOutlet weak var view_alert:UIView!
    @IBOutlet weak var view_alert_1:UIView!
    @IBOutlet weak var tbl_alert_1:UITableView!
    @IBOutlet weak var btn_sent_alert:UIButton!
    
    @IBOutlet weak var lbl_msg_on_thankyou:UILabel!
    
    @IBOutlet weak var view_thank_you_alert:UIView!
    @IBOutlet weak var btn_ok:UIButton!
    
    @IBOutlet weak var btn_cancel:UIButton!
    @IBOutlet weak var btn_login:UIView!
    
    var arr_radio = [Bool]()
    var is_alert_selected = false
    
    var is_SetAllData = false
    
    @IBOutlet weak var height_About:NSLayoutConstraint!
    @IBOutlet weak var height_containerView:NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_rounded_corner(view: btn_cancel)
        func_rounded_corner(view: btn_login)
        
        view_alert_1.layer.cornerRadius = 10
        view_alert_1.clipsToBounds = true
        
        btn_sent_alert.layer.cornerRadius = btn_sent_alert.frame.size.height/2
        btn_sent_alert.clipsToBounds = true
        
        view_thank_you_alert.layer.cornerRadius = 10
        view_thank_you_alert.clipsToBounds = true
        
        btn_ok.layer.cornerRadius = btn_sent_alert.frame.size.height/2
        btn_ok.clipsToBounds = true
        
        view_alert.isHidden = true
        view_thank_you_alert.isHidden = true
        
        func_set_nav_bar_title()
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        view_loading.isHidden = true
        
        btn_continue.layer.cornerRadius = 10
        btn_continue.layer.shadowOpacity = 3.0
        btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_continue.layer.shadowRadius = 3.0
        btn_continue.layer.shadowColor = color_app.cgColor
        
        view_message.layer.cornerRadius = 10
        view_message.layer.shadowOpacity = 3.0
        view_message.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        view_message.layer.shadowRadius = 3.0
        view_message.layer.shadowColor = color_app.cgColor
        
        view_rating.isHidden = true
        
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height/2
        btn_submit.clipsToBounds = true
        
        txt_comment_for_rating.layer.cornerRadius = 2
        txt_comment_for_rating.layer.borderColor = color_app.cgColor
        txt_comment_for_rating.layer.borderWidth=1
        
        func_shadow(view_shadow:view_shop_images)
        view_rating_1.layer.cornerRadius = 10
        view_rating_1.clipsToBounds = true
        
        view_Please_login_1.layer.cornerRadius = 4
        view_Please_login_1.clipsToBounds = true
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func func_rounded_corner(view:UIView)  {
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view_Please_login.isHidden = true
    }
    
    func func_shadow(view_shadow:UIView) {
        view_shadow.layer.shadowOpacity = 5.0
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowRadius = 5.0
        view_shadow.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    func func_set_nav_bar_title() {
        let from = "From".localized
        nav_bar.title = "\(Model_Details.shared.index_selected+1) \(from) \(Model_Details.shared.arr_details.count)"
        
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            func_get_fav_list()
        } else {
            self.func_view_count()
        }
    }
    
    func func_set_data() {
        if Model_Details.shared.index_selected+1 == Model_Details.shared.arr_details.count {
            btn_next.isHidden = true
        } else {
            btn_next.isHidden = false
        }
        
        if Model_Details.shared.index_selected+1 == 1 {
            btn_previous.isHidden = true
        } else {
            btn_previous.isHidden = false
        }
        
        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        
        img_user.sd_setShowActivityIndicatorView(true)
        img_user.sd_setIndicatorStyle(.gray)
        img_user.sd_setImage(with:URL (string: model.user_profile), placeholderImage:(UIImage(named:"image-add-button.png")))
        
        lbl_category.text = model.category_name
        lbl_sub_category.text = model.subcate_name
        lbl_views.text = model.view_count
        lbl_about_ad.text = model.shop_about
        lbl_user_name.text = model.user_name
        Model_Chat_Message_Fri.shared.fri_name = model.user_name
        lbl_add_number.text = model.shop_id
        
        height_About.constant = func_size(width: Int(self.view.bounds.width-50), messageText:model.shop_about).height
        height_containerView.constant = height_containerView.constant + height_About.constant
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale (identifier:date_localization)
        let date_converted = formatter.date(from:model.shop_add_date)
        
        lbl_last_date.text = formatter.string(from:date_converted!)
        
        ad_name = model.shop_name
        ad_details = model.shop_about
        image_link = model.shop_image
        lbl_shop_address.text = model.shop_address
        lbl_Ad_name.text = model.shop_name
        
        lbl_user_mobile.text = model.user_mobile
        
        if model.avg_rating.isEmpty {
            star.rating = 0
        } else {
            star.rating = Float(model.avg_rating)!
        }
        
        var shop_lat = 0.0
        var shop_lang = 0.0
        
        if !model.shop_lat.isEmpty {
            shop_lat = Double(model.shop_lat)!
            shop_lang = Double(model.shop_lang)!
        }
        
        map_view.clear()
        let position = CLLocationCoordinate2D(latitude:shop_lat, longitude:shop_lang)
        co_Ordinate_shop = position
        
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
        
        map_view.camera = camera
        map_view?.animate(to: camera)
        
        let marker_position = GMSMarker(position: position)
        
        marker_position.map =  self.map_view
        let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
        let img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
        img_marker.frame = view_hotel.frame
        view_hotel.addSubview(img_marker)
        marker_position.iconView = view_hotel
        
        Model_Details.shared.shop_id = model.shop_id
        
        Model_Details.shared.convict_user_id = model.user_id
        Model_Details.shared.shop_id = model.shop_id
        
        for model_fav in Model_Favorites.shared.arr_favo_list {
            if model.shop_id == model_fav.shop_id {
                btn_favorite.isSelected = true
                break
            } else {
                btn_favorite.isSelected = false
            }
        }
        self.func_get_shop_detail()
    }
    
    func func_SetDetails() {
        if Model_Details.shared.index_selected+1 == Model_Details.shared.arr_details.count {
            btn_next.isHidden = true
        } else {
            btn_next.isHidden = false
        }
        
        if Model_Details.shared.index_selected+1 == 1 {
            btn_previous.isHidden = true
        } else {
            btn_previous.isHidden = false
        }
        
        let model = Model_Details.shared.arr_details[0]
        
        img_user.sd_setShowActivityIndicatorView(true)
        img_user.sd_setIndicatorStyle(.gray)
        img_user.sd_setImage(with:URL (string: model.user_profile), placeholderImage:(UIImage(named:"image-add-button.png")))
        
        lbl_category.text = model.category_name
        lbl_sub_category.text = model.subcate_name
        lbl_views.text = model.view_count
        lbl_about_ad.text = model.shop_about
        lbl_user_name.text = model.user_name
        Model_Chat_Message_Fri.shared.fri_name = model.user_name
        lbl_add_number.text = model.shop_id
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale (identifier:date_localization)
        let date_converted = formatter.date(from:model.shop_add_date)
        
        lbl_last_date.text = formatter.string(from:date_converted!)
        
        ad_name = model.shop_name
        ad_details = model.shop_about
        image_link = model.shop_image
        lbl_shop_address.text = model.shop_address
        lbl_Ad_name.text = model.shop_name
        
        lbl_user_mobile.text = model.user_mobile
        
        if model.avg_rating.isEmpty {
            star.rating = 0
        } else {
            star.rating = Float(model.avg_rating)!
        }
        
        var shop_lat = 0.0
        var shop_lang = 0.0
        
        if !model.shop_lat.isEmpty {
            shop_lat = Double(model.shop_lat)!
            shop_lang = Double(model.shop_lang)!
        }
        
        map_view.clear()
        let position = CLLocationCoordinate2D(latitude:shop_lat, longitude:shop_lang)
        co_Ordinate_shop = position
        
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
        
        map_view.camera = camera
        map_view?.animate(to: camera)
        
        let marker_position = GMSMarker(position: position)
        
        marker_position.map =  self.map_view
        let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
        let img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
        img_marker.frame = view_hotel.frame
        view_hotel.addSubview(img_marker)
        marker_position.iconView = view_hotel
        
        Model_Details.shared.shop_id = model.shop_id
        
        Model_Details.shared.convict_user_id = model.user_id
        Model_Details.shared.shop_id = model.shop_id
        
        for model_fav in Model_Favorites.shared.arr_favo_list {
            if model.shop_id == model_fav.shop_id {
                btn_favorite.isSelected = true
                break
            } else {
                btn_favorite.isSelected = false
            }
        }
    }
    
    @IBAction func btn_login(_ button:UIButton!) {
        view_Please_login.isHidden = true
        let my_profile = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        my_profile.modalPresentationStyle = .fullScreen
        present(my_profile, animated: true, completion: nil)
    }
    
    @IBAction func btn_cancel(_ button:UIButton!) {
        view_Please_login.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_rating_hide(_ sender:Any) {
        view_rating.isHidden = true
    }
    
    @IBAction func btn_submit_rating(_ sender:Any) {
        self.view.endEditing(true)
        
        if star_rating.rating == 0.0 {
            self.func_ShowHud_Success(with: "Select star rating".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                self.func_HideHud()
            })
            return
        }
        
//        if txt_comment_for_rating.text == "...Put your comment here".localized {
//            txt_comment_for_rating.text = ""
//        }
        
        Model_Details.shared.feedback_cmt = txt_comment_for_rating.text
        Model_Details.shared.feedback_rating = "\(Int(star_rating.rating))"
        
        view_loading.isHidden = false
        Model_Details.shared.func_do_feedback { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                
                if status == "success" {
                    self.view_rating.isHidden=true
                    self.view_alert.isHidden = false
                    self.view_alert_1.isHidden = true
                    self.view_thank_you_alert.isHidden = false
                    self.lbl_msg_on_thankyou.text = Model_Details.shared.str_message
                    
//                    self.func_ShowHud_Success(with: Model_Details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.view_alert.isHidden = true
//                        self.func_HideHud()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
        
    }
    
    
    
    @IBAction func whatsappShareLink(_ sender: UIButton) {
        
        let str_1 = ",يحتوي على العديد من متاجر الأسر المنتجة,رائع و سهل الإستخدام,انضم معنا و تصفح العديد من المتاجر الرائعةHome Mallتطبيق"
        let str_2 = "يمكنك المشاركة و الطلب و التقييم. حمله الأن."
        let str_4 = ""
        
        
//        let str_1 = "اسم المتجر: \(ad_name)"
//        let str_2 = "عن المتجر: \(ad_details)"
//        let str_4 = "Home mall لمشاهدة المتجر والمزيد من المتاجر حمل تطبيق"
        
        let msg = "\(str_1) \n\(str_2) \n\(str_4) \n\(homemall_appstore_link)"
        
        let urlWhats = "https://api.whatsapp.com/send?text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.openURL(whatsappURL as URL)
                } else {
                    print("please install watsapp")
                }
            }
        }
    }
    
    
    
    @IBAction func tweetButtonShare(_ sender: Any) {
        let str_1 = "اسم المتجر: \(ad_name)"
        let str_2 = "عن المتجر: \(ad_details)"
        let str_4 = "Home mall لمشاهدة المتجر والمزيد من المتاجر حمل تطبيق"
        
        let msg = "\(str_1) \n\(str_2) \n\(str_4) \n\(homemall_appstore_link)"
        
        let tweetText = msg
        let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)"
        
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if let url = URL(string: escapedShareString),UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            navigation_title_tell_your_friends = "Homemall-Twitter"
            str_tell_your_friends = shareString

            let tell_your_friends = storyboard?.instantiateViewController(withIdentifier: "Tell_Your_Friends_ViewController") as! Tell_Your_Friends_ViewController
            tell_your_friends.modalPresentationStyle = .fullScreen
            present(tell_your_friends, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btn_message(_ sender:Any) {
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
            
            return
        }
        
        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        Model_Chat_Message_Fri.shared.fri_id = model.user_id
        Model_Chat_Message_Fri.shared.shop_id = model.shop_id
        
        if model.user_name.isEmpty {
            Model_Chat_Message_Fri.shared.fri_name = model.user_mobile
        } else {
            Model_Chat_Message_Fri.shared.fri_name = model.user_name
        }
        
        if model.user_id == Model_Splash.shared.user_id {
            let alert = UIAlertController (title: "This ad posted by you".localized, message: "No need to message yourself.".localized, preferredStyle: .alert)
            let no = UIAlertAction(title: "ok".localized, style: .default) { (yes) in
                
            }
            
            alert.addAction(no)
            
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        let chat_user_list = storyboard?.instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController") as! Chat_Message_Fri_ViewController
        chat_user_list.modalPresentationStyle = .fullScreen
        present(chat_user_list, animated: true, completion: nil)
    }
    
    @IBAction func btn_next(_ sender:Any) {
        Model_Details.shared.index_selected = Model_Details.shared.index_selected+1
        func_set_nav_bar_title()
    }
    
    @IBAction func btn_previous(_ sender:Any) {
            Model_Details.shared.index_selected = Model_Details.shared.index_selected-1
            func_set_nav_bar_title()
    }
    
    @IBAction func btn_favorite(_ sender:Any) {
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
            
            return
        }

        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        Model_Favorites.shared.shop_id = model.shop_id
        
        if model.user_id == Model_Splash.shared.user_id {
            let alert = UIAlertController (title: "This ad posted by you".localized, message: "It's already in your My Ads.".localized, preferredStyle: .alert)
            let no = UIAlertAction(title: "ok".localized, style: .default) { (yes) in

            }
            
            alert.addAction(no)

            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)

            return
        }
        
        if btn_favorite.isSelected {
            Model_Favorites.shared.Want_to = "2"
        } else {
            Model_Favorites.shared.Want_to = "1"
        }
        func_do_undo_fav()
    }
    
    @IBAction func btn_give_rating(_ sender:Any) {
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
            
            return
        }
        
        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        Model_Favorites.shared.shop_id = model.shop_id
        
        if model.user_id == Model_Splash.shared.user_id {
            let alert = UIAlertController (title: "This ad posted by you".localized, message: "Another user will rate you.".localized, preferredStyle: .alert)
            let no = UIAlertAction(title: "ok".localized, style: .default) { (yes) in
                
            }
            
            alert.addAction(no)
            
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
            
            return
        }
        view_rating.isHidden = false
    }
    
    @IBAction func btn_back(_ sender:Any) {
//        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "back"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_call(_ sender:UIButton) {
        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        Model_Favorites.shared.shop_id = model.shop_id
        
        if model.user_id == Model_Splash.shared.user_id {
            let alert = UIAlertController (title: "This ad posted by you".localized, message: "So no need to call your self".localized, preferredStyle: .alert)
            let no = UIAlertAction(title: "ok".localized, style: .default) { (yes) in
                
            }
            
            alert.addAction(no)
            
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        func_call()
    }
    
    @IBAction func redirect_to_google_maps(_ sender:UIButton) {
        if let aString = URL(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(aString) {
                if let aString = URL(string: "comgooglemaps://?saddr=\(co_OrdinateCurrent.latitude),\(co_OrdinateCurrent.longitude)&daddr=\(co_Ordinate_shop.latitude),\(co_Ordinate_shop.longitude)&zoom=14&views=traffic") {
                    UIApplication.shared.openURL(aString)
                }
            } else {
                print("Can't use comgooglemaps://")
            }
        }
        
    }
    
    func func_call() {
        if let phoneCallURL = URL(string: "tel://\(lbl_user_mobile.text!)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    application.openURL(phoneCallURL)
                }
            }
        }
    }
    
    @IBAction func btn_alert(_ sender:UIButton) {
        func_get_alert_type()
    }
    
    @IBAction func btn_send_alert(_ sender:UIButton) {
        if !is_alert_selected {
            func_ShowHud_Error(with: "Select an alert reason".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        }
        
        func_do_report()
    }
    
    @IBAction func btn_ok(_ sender:UIButton) {
        view_alert.isHidden = true
    }
    
    @IBAction func btn_hide_alert(_ sender:UIButton) {
        view_alert.isHidden = true
    }

}



//  MARK:- Api functions
extension Details_ViewController:UITextViewDelegate {
    
    func func_view_count() {
        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        
        Model_Details.shared.shop_id = model.shop_id
        Model_Details.shared.last_count = model .view_count
        
        view_loading.isHidden = false
        Model_Details.shared.func_view_count { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.func_set_data()
            }
        }
    }
    
    
    
    func func_do_undo_fav() {
        self.view_loading.isHidden = false
        Model_Favorites.shared.func_do_undo_fav { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    if self.btn_favorite.isSelected {
                        self.btn_favorite.isSelected = false
                    } else {
                        self.btn_favorite.isSelected = true
                    }
                    self.func_ShowHud_Success(with: Model_Favorites.shared.str_message)
                } else {
                    self.func_ShowHud_Error(with: Model_Favorites.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
//                    self.func_get_fav_list()
                })
            }
        }
    }
    
    func func_get_fav_list() {
        view_loading.isHidden = false
        
        Model_Favorites.shared.func_get_fav_list { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                
                if Model_Details.shared.arr_details.count > 0 {
                    self.is_SetAllData = false
                    let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
                    Model_Favorites.shared.shop_id = model.shop_id
                    
                    if model.user_id != Model_Splash.shared.user_id {
                        self.func_view_count()
                    } else {
                        self.func_set_data()
                    }
                } else {
                    self.is_SetAllData = true
                    self.func_get_shop_detail()
                }
            }
        }
    }
    
    func func_get_alert_type() {
        view_loading.isHidden = false
        Model_Details.shared.func_get_alert_type { (status) in
            DispatchQueue.main.async {
                for _ in 0..<Model_Details.shared.arr_get_alert_type.count {
                    self.arr_radio.append(false)
                }
                
                self.tbl_alert_1.reloadData()
                
                self.view_loading.isHidden = true
                
                self.view_alert.isHidden = false
                
                self.view_alert_1.isHidden = false
                self.view_thank_you_alert.isHidden = true
            }
        }
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "...Put your comment here".localized {
            textView.textColor = UIColor .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor .darkGray
            textView.text = "...Put your comment here".localized
        }
    }

}





//  MARK:- UICollectionView methods
extension Details_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func func_get_shop_detail() {
        self.view_loading.isHidden = false
        
        Model_Details.shared.func_get_shop_detail { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    
                } else {
                    
                }
                
                if self.is_SetAllData {
                    self.func_SetDetails()
                }
                
                self.coll_shop_images.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size_coll = collectionView.frame.size
        return CGSize (width: size_coll.width, height:size_coll.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Model_Details.shared.arr_shop_images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Review_this_ad_CollectionViewCell
        
        let model = Model_Details.shared.arr_shop_images[indexPath.row]
        
        if model.shop_image_tag == "0" {
            coll_Car.lbl_name_ad.text = ""
            coll_Car.lbl_Ad_name_black_patti.text = ""
            coll_Car.lbl_Ad_name_black_patti.isHidden = true
        } else {
            coll_Car.lbl_name_ad.text = model.shop_image_tag
            coll_Car.lbl_Ad_name_black_patti.text = model.shop_image_tag
            coll_Car.lbl_Ad_name_black_patti.isHidden = false
        }
        
        coll_Car.img_ad.sd_setShowActivityIndicatorView(true)
        coll_Car.img_ad.sd_setIndicatorStyle(.gray)
        coll_Car.img_ad.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"")))
        
        return coll_Car
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let select_full_image = storyboard?.instantiateViewController(withIdentifier: "Select_Full_Image_ViewController") as! Select_Full_Image_ViewController
        select_full_image.modalPresentationStyle = .fullScreen
        present(select_full_image, animated: true, completion: nil)
    }
    
}



extension Details_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if !self.is_updated_location {
            self.is_updated_location = true
            self.co_OrdinateCurrent = manager.location!.coordinate
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("",error)
    }

}




extension Details_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Details.shared.arr_get_alert_type.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Alert_TableViewCell
        
        cell.lbl_alert.text = Model_Details.shared.arr_get_alert_type[indexPath.row].alert_name
        
        if arr_radio[indexPath.row] {
            cell.btn_radio.isSelected = true
        } else {
            cell.btn_radio.isSelected = false
        }
        
        cell.btn_radio.tag = indexPath.row
        cell.btn_radio.addTarget(self, action: #selector(btn_radio(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func btn_radio(_ sender:UIButton) {
        for i in 0..<Model_Details.shared.arr_get_alert_type.count {
            if i == sender.tag {
                arr_radio[i] = true
            } else {
                arr_radio[i] = false
            }
        }
        
        is_alert_selected = true
        
        Model_Details.shared.alert_id = Model_Details.shared.arr_get_alert_type[sender.tag].alert_id
        tbl_alert_1.reloadData()
    }
    
    func func_do_report() {
        view_loading.isHidden = false
        Model_Details.shared.func_do_report { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    self.lbl_msg_on_thankyou.text = "We will check this post"
                    self.view_alert.isHidden = false
                    self.view_alert_1.isHidden = true
                    self.view_thank_you_alert.isHidden = false
                } else {
                    self.lbl_msg_on_thankyou.text = "Try again"
                    self.view_alert.isHidden = false
                    self.view_alert_1.isHidden = false
                    self.view_thank_you_alert.isHidden = true
                }
            }
        }
    }
    
}




