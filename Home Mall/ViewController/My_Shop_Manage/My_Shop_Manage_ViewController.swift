//
//  My_Shop_Manage_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 04/03/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

import UIKit
import SDWebImage



class My_Shop_Manage_ViewController: UIViewController {
    
    @IBOutlet weak var view_profile_container:UIView!
    @IBOutlet weak var view_addyourshop:UIView!
    @IBOutlet weak var view_shop_image:UIView!
    
    @IBOutlet weak var view_logout:UIView!
    @IBOutlet weak var view_logout_1:UIView!
    @IBOutlet weak var btn_no:UIButton!
    @IBOutlet weak var btn_yes:UIButton!
    
    @IBOutlet weak var lbl_rating_msg:UILabel!
    @IBOutlet weak var lbl_user_name:UILabel!
    @IBOutlet weak var lbl_date_join:UILabel!
    @IBOutlet weak var lbl_shop_active:UILabel!

    @IBOutlet weak var tbl_rating:UITableView!
    @IBOutlet weak var img_active:UIImageView!
    @IBOutlet weak var view_delete:UIView!
    
    
    var view_loader = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_delete.layer.cornerRadius = 10
        view_delete.layer.shadowOpacity = 3.0
        view_delete.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view_delete.layer.shadowRadius = 3.0
        view_delete.layer.shadowColor = color_app.cgColor
        
        func_set_shadow()
        
        img_active.layer.cornerRadius = img_active.frame.size.height/2
        img_active.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        let model = Model_My_Profile.shared.arr_My_Ads[0]
        lbl_user_name.text = model.shop_name
        let last_update = "Last Update".localized
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale (identifier:date_localization)
        let date_converted = formatter.date(from:model.shop_add_date)
        
        lbl_date_join.text = "\(last_update) : \(formatter.string(from:date_converted!))"
        
        if model.shop_status == "0" {
            img_active.backgroundColor = UIColor .red
            lbl_shop_active.text = "Your shop is unactive".localized
        } else {
            img_active.backgroundColor = UIColor .green
            lbl_shop_active.text = "Your shop is active".localized
        }
        
        self.get_feedback()
    }
    
    func get_feedback() {
        view_loader.isHidden = false
        Model_My_Shop_Manage.shared.func_get_feedback { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if Model_My_Shop_Manage.shared.arr_get_feedback.count > 0 {
                    self.tbl_rating.isHidden = false
                    self.lbl_rating_msg.isHidden = true
                } else {
                    self.tbl_rating.isHidden = true
                    self.lbl_rating_msg.isHidden = false
                }
                self.tbl_rating.reloadData()
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func func_set_shadow() {
        view_addyourshop.layer.cornerRadius = 10
        view_addyourshop.clipsToBounds = true
        
        view_profile_container.layer.shadowOpacity = 5.0
        view_profile_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_profile_container.layer.shadowRadius = 5.0
        view_profile_container.layer.shadowColor = UIColor .white.cgColor
        
        view_shop_image.layer.cornerRadius = 2
        view_shop_image.layer.shadowOpacity = 5.0
        view_shop_image.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view_shop_image.layer.shadowRadius = 6.0
        view_shop_image.layer.shadowColor = UIColor (red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 0.7).cgColor
        
        view_logout.isHidden = true
        
        view_logout_1.layer.cornerRadius = 6
        view_logout_1.clipsToBounds = true
        
        btn_no.layer.cornerRadius = 10
        btn_yes.layer.cornerRadius = 10
    }
    
    
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_edit(_ sender:UIButton) {
        let edit_profile = storyboard?.instantiateViewController(withIdentifier: "Edit_Profile_ViewController") as! Edit_Profile_ViewController
        edit_profile.modalPresentationStyle = .fullScreen
        present(edit_profile, animated: true, completion: nil)
//        navigationController?.pushViewController(edit_profile, animated:true)
    }
    
    @IBAction func btn_edit_your_shop(_ sender:UIButton) {
        Model_Details.shared.index_selected = 0
        Model_My_Ads.shared.is_edit = true
        let fav_VC = self.storyboard?.instantiateViewController(withIdentifier: "Category_list_ViewController") as! Category_list_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        self.present(fav_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_logout(_ sender:UIButton) {
        view_logout.isHidden = false
    }
    
    @IBAction func btn_delete(_ sender:UIButton) {
        let alert = UIAlertController (title: "WARNING!".localized, message: "Are you sure?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "yes".localized, style: .default) { (yes) in
            self.func_delete_shop()
        }
        
        let no = UIAlertAction(title: "Cancel".localized, style: .default) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.view.tintColor = UIColor .black
        present(alert, animated: true, completion: nil)
    }
    
    @objc func func_delete_shop() {
        view_loader.isHidden = false
        
        Model_My_Shop_Manage.shared.shop_id = Model_My_Profile.shared.arr_My_Ads[0].shop_id
        Model_My_Shop_Manage.shared.func_delete_shop { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_My_Shop_Manage.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_My_Shop_Manage.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    @IBAction func btn_yes_logout(_ sender:UIButton) {
        view_logout.isHidden = true
        UserDefaults.standard.removeObject(forKey: "login_Data")
        
        if is_from_profile {
            presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "after_login"), object: nil)
        NotificationCenter .default.post(name: NSNotification.Name .init(rawValue: "login_chat"), object: nil)
    }
    
    @IBAction func btn_no_logout(_ sender:UIButton) {
        view_logout.isHidden = true
    }
    
}



extension My_Shop_Manage_ViewController :UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_My_Shop_Manage.shared.arr_get_feedback.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell_Rating = tableView.dequeueReusableCell(withIdentifier: "cell_Rating", for:indexPath) as! My_Shop_Manage_TableViewCell
        
        let model = Model_My_Shop_Manage.shared.arr_get_feedback[indexPath.row]
        let namee = "\(model.user_name)"
//        if namee.count >= 5 {
            cell_Rating.lbl_name.text = namee.prefix(2) + "*****" + namee.suffix(2)
//        }
//        else{
//            cell_Rating.lbl_name.text = "Na*****ser"
//        }
        
        cell_Rating.lbl_Date_time.text = "\(model.insert_date) \(model.insert_time)"
        cell_Rating.lbl_Comment.text = model.feedback_comment
        
        let str_rating = model.feedback_rating
        cell_Rating.view_Rating.rating = Float(str_rating)!
        cell_Rating.img_Profile.sd_setImage(with: URL(string:model.user_profile), placeholderImage: UIImage(named: "ratelist.png"))
        
        return cell_Rating
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
}






