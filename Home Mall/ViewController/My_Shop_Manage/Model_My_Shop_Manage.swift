

//
//  File.swift
//  Home Mall
//
//  Created by iOS-Appentus on 04/03/19.
//  Copyright © 2019 appentus. All rights reserved.
//


class Model_My_Shop_Manage {
    static let shared = Model_My_Shop_Manage()
    
    var feedback_id = ""
    var feedback_comment = ""
    var sender_user_id = ""
    var receiver_shop_id = ""
    var feedback_rating = ""
    var insert_date = ""
    var insert_time = ""
    var shop_id = ""
    var user_id = ""
    var shop_name = ""
    var shop_about = ""
    var shop_image = ""
    var shop_lat = ""
    var shop_lang = ""
    var city_id = ""
    var shop_location = ""
    var shop_add_date = ""
    var category_code = ""
    var subcate_code = ""
    var shop_near_by = ""
    var shop_address = ""
    var view_count = ""
    var special_offer_status = ""
    var shop_status = ""
    var user_name = ""
    var user_about = ""
    var user_country_code = ""
    var user_mobile = ""
    var user_email = ""
    var user_dob = ""
    var user_gender = ""
    var user_join_date = ""
    var user_profile = ""
    var user_shop_id = ""
    var user_device_type = ""
    var user_device_token = ""
    var user_password = ""
    var user_status = ""
    var user_role = ""
    var payment_status = ""
    var user_added = ""
    var user_add_by = ""
    
    var str_message = ""
    
    var arr_get_feedback = [Model_My_Shop_Manage]()
    
    func func_get_feedback(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_feedback"
        let params = "shop_id=\(Model_My_Profile.shared.arr_My_Ads[0].shop_id)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            
            self.arr_get_feedback.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_feedback.append(self.func_set_get_feedback(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_set_get_feedback(dict:[String:Any]) -> Model_My_Shop_Manage {
        let model = Model_My_Shop_Manage()
        
        model.feedback_id = "\(dict["feedback_id"] ?? "")"
        model.feedback_comment = "\(dict["feedback_comment"] ?? "")"
        model.sender_user_id = "\(dict["sender_user_id"] ?? "")"
        model.receiver_shop_id = "\(dict["receiver_shop_id"] ?? "")"
        model.feedback_rating = "\(dict["feedback_rating"] ?? "")"
        model.insert_date = "\(dict["insert_date"] ?? "")"
        model.insert_time = "\(dict["insert_time"] ?? "")"
        model.shop_id = "\(dict["shop_id"] ?? "")"
        model.user_id = "\(dict["user_id"] ?? "")"
        model.shop_name = "\(dict["shop_name"] ?? "")"
        model.shop_about = "\(dict["shop_about"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        model.shop_lat = "\(dict["shop_lat"] ?? "")"
        model.shop_lang = "\(dict["shop_lang"] ?? "")"
        model.city_id = "\(dict["city_id"] ?? "")"
        model.shop_location = "\(dict["shop_location"] ?? "")"
        model.shop_add_date = "\(dict["shop_add_date"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        model.subcate_code = "\(dict["subcate_code"] ?? "")"
        model.shop_near_by = "\(dict["shop_near_by"] ?? "")"
        model.shop_address = "\(dict["shop_address"] ?? "")"
        model.view_count = "\(dict["view_count"] ?? "")"
        model.special_offer_status = "\(dict["special_offer_status"] ?? "")"
        model.shop_status = "\(dict["shop_status"] ?? "")"
        model.user_name = "\(dict["user_name"] ?? "")"
        model.user_about = "\(dict["user_about"] ?? "")"
        model.user_country_code = "\(dict["user_country_code"] ?? "")"
        model.user_mobile = "\(dict["user_mobile"] ?? "")"
        model.user_email = "\(dict["user_email"] ?? "")"
        model.user_dob = "\(dict["user_dob"] ?? "")"
        model.user_gender = "\(dict["user_gender"] ?? "")"
        model.user_join_date = "\(dict["user_join_date"] ?? "")"
        model.user_profile = "\(dict["user_profile"] ?? "")"
        model.user_shop_id = "\(dict["user_shop_id"] ?? "")"
        model.user_device_type = "\(dict["user_device_type"] ?? "")"
        model.user_device_token = "\(dict["user_device_token"] ?? "")"
        model.user_password = "\(dict["user_password"] ?? "")"
        model.user_status = "\(dict["user_status"] ?? "")"
        model.user_role = "\(dict["user_role"] ?? "")"
        model.payment_status = "\(dict["payment_status"] ?? "")"
        model.user_added = "\(dict["user_added"] ?? "")"
        model.user_add_by = "\(dict["user_add_by"] ?? "")"
        
        return model
    }
    
    
    
    func func_delete_shop(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"delete_shop"
        let params = "shop_id=\(shop_id)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if let msg = dict_JSON["message"] as? String {
                        self.str_message = msg
                    }
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    
}






