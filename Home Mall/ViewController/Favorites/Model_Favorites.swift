//
//  Model_Favorites.swift
//  Home Mall
//
//  Created by iOS-Appentus on 31/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation


class Model_Favorites {
    static let shared = Model_Favorites()
    
    
    var fav_id = ""
    var shop_id = ""
    var user_id = ""
    var user_name = ""
    var user_country_code = ""
    var user_mobile = ""
    var user_email = ""
    var user_join_date = ""
    var user_profile = ""
    var user_shop_id = ""
    var user_device_type = ""
    var user_device_token = ""
    var user_password = ""
    var user_status = ""
    var shop_name = ""
    var shop_about = ""
    var shop_image = ""
    var shop_lat = ""
    var shop_lang = ""
    var shop_location = ""
    var shop_add_date = ""
    var category_code = ""
    var subcate_code = ""
    var shop_near_by = ""
    var shop_address = ""
    var view_count = ""
    var feedback_id = ""
    var feedback_comment = ""
    var sender_user_id = ""
    var receiver_shop_id = ""
    var feedback_rating = ""
    var category_id = ""
    var category_name = ""
    var category_status = ""
    var subcate_id = ""
    var subcate_name = ""
    var subcate_status = ""
    var avg_rating = ""
    
    
    var Want_to = ""
//    var user_id = ""
//    var shop_id = ""
    
    var str_message = ""
    
    var arr_favo_list = [Model_Search_VC]()
    
    func func_do_undo_fav(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"do_undo_fav"
        let params = "want_to=\(Want_to)&user_id=\(Model_Splash.shared.user_id)&shop_id=\(shop_id)"
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = "\(dict_JSON["message"] ?? "")"
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    func func_get_fav_list(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_fav_list"
        let params = "user_id=\(Model_Splash.shared.user_id)"
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_favo_list.removeAll()
            if dict_JSON["status"] as? String == "success" {
                self.str_message = "\(dict_JSON["message"] ?? "")"
                let arr_result = dict_JSON["result"] as! [[String:Any]]
                for dict in arr_result {
                    self.arr_favo_list.append(self.func_set_fav_data(dict: dict))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_set_fav_data(dict:[String:Any]) -> Model_Search_VC {
        let model = Model_Search_VC()
        
        print(dict)
        
        model.fav_id = "\(dict["fav_id"] ?? "")"
        model.shop_id = "\(dict["shop_id"] ?? "")"
        model.user_id = "\(dict["user_id"] ?? "")"
        model.user_name = "\(dict["user_name"] ?? "")"
        model.user_country_code = "\(dict["user_country_code"] ?? "")"
        model.user_mobile = "\(dict["user_mobile"] ?? "")"
        model.user_email = "\(dict["user_email"] ?? "")"
        model.user_join_date = "\(dict["user_join_date"] ?? "")"
        model.user_profile = "\(dict["user_profile"] ?? "")"
        model.user_shop_id = "\(dict["user_shop_id"] ?? "")"
        model.user_device_type = "\(dict["user_device_type"] ?? "")"
        model.user_device_token = "\(dict["user_device_token"] ?? "")"
        model.user_password = "\(dict["user_password"] ?? "")"
        model.user_status = "\(dict["user_status"] ?? "")"
        model.shop_name = "\(dict["shop_name"] ?? "")"
        model.shop_about = "\(dict["shop_about"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        model.shop_lat = "\(dict["shop_lat"] ?? "")"
        model.shop_lang = "\(dict["shop_lang"] ?? "")"
        model.shop_location = "\(dict["shop_location"] ?? "")"
        model.shop_add_date = "\(dict["shop_add_date"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        model.subcate_code = "\(dict["subcate_code"] ?? "")"
        model.shop_near_by = "\(dict["shop_near_by"] ?? "")"
        model.shop_address = "\(dict["shop_address"] ?? "")"
        model.view_count = "\(dict["view_count"] ?? "")"
        model.feedback_id = "\(dict["feedback_id"] ?? "")"
        model.feedback_comment = "\(dict["feedback_comment"] ?? "")"
        model.sender_user_id = "\(dict["sender_user_id"] ?? "")"
        model.receiver_shop_id = "\(dict["receiver_shop_id"] ?? "")"
        model.feedback_rating = "\(dict["feedback_rating"] ?? "")"
        model.category_id = "\(dict["category_id"] ?? "")"
        model.category_name = "\(dict["category_name"] ?? "")"
        model.category_status = "\(dict["category_status"] ?? "")"
        model.subcate_id = "\(dict["subcate_id"] ?? "")"
        model.subcate_name = "\(dict["subcate_name"] ?? "")"
        model.subcate_status = "\(dict["subcate_status"] ?? "")"
        model.avg_rating = "\(dict["avg_rating"] ?? "")"
        
        return model
    }

    
}
