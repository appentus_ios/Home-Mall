//
//  Favorites_TableViewCell.swift
//  Home Mall
//
//  Created by iOS-Appentus on 12/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import ASStarRatingView

class Favorites_TableViewCell: UITableViewCell {
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var view_container_1:UIView!
    @IBOutlet weak var view_image_shadow:UIView!
    
    @IBOutlet weak var image_shadow:UIImageView!
    @IBOutlet weak var lbl_location_address:UILabel!
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var lbl_Owner_name:UILabel!
    @IBOutlet weak var lbl_date_join:UILabel!
    
    @IBOutlet weak var img_next_arrow:UIImageView!
    @IBOutlet weak var view_next_arrow:UIView!
    @IBOutlet weak var btn_for_select:UIButton!
    
    @IBOutlet weak var star_view:ASStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_name.layer.cornerRadius = lbl_name.frame.size.height/2
        lbl_name.clipsToBounds = true
        
        image_shadow.layer.cornerRadius = 3.0
        image_shadow.clipsToBounds = true
        
        view_image_shadow.layer.cornerRadius = 6.0
        view_image_shadow.layer.shadowOpacity = 3.0
        view_image_shadow.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view_image_shadow.layer.shadowRadius = 3.0
        view_image_shadow.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_container_1.layer .cornerRadius = 6
        view_container_1.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
}
