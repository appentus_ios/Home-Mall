//
//  Model_Ad_Image.swift
//  Home Mall
//
//  Created by iOS-Appentus on 15/02/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation
import Alamofire



class Model_Ad_Image {
    static let shared = Model_Ad_Image()
    
    var shop_id = ""
    
    var shop_image_id = ""
    var shop_image = ""
    var shop_image_tag = ""
    var shop_img_id = ""
    
    var img_shop_image = UIImage()
    var str_msg = ""
    
    var arr_images_shops = [Model_Ad_Image]()
    
    func func_get_shop_detail(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_detail"
        let params = "shop_id=\(shop_id)"
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_images_shops.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                if let arr_images = dict_JSON["images"] as? [[String:Any]] {
                    for dict_json in arr_images {
                        self.arr_images_shops.append(self.func_shops_images(dict: dict_json))
                    }
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    func func_delete_shop_photo(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"delete_shop_photo"
        let params = "shop_image_id=\(shop_image_id)"
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_msg = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_shops_images(dict:[String:Any]) -> Model_Ad_Image {
        let model = Model_Ad_Image()
        
        model.shop_image_id = "\(dict["shop_image_id"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        let shop_image_tag_1 = "\(dict["shop_image_tag"] ?? "")"
        if shop_image_tag_1 == "0" {
            model.shop_image_tag = ""
        } else {
            model.shop_image_tag = shop_image_tag_1
        }
        
        model.shop_img_id = "\(dict["shop_img_id"] ?? "")"
        
        return model

    }
    
    
    
    func func_update_shop_photo(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"update_shop_photo"
        
        let parameters = [
            "shop_image_id":shop_image_id,
        ]
        
        print(parameters)
        
        let url = URL (string: str_FullURL)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            var imageData = Data()
            
            imageData = UIImageJPEGRepresentation(self.img_shop_image, 0.2)!
            multipartFormData.append(imageData, withName: "file", fileName: "image.jpg", mimeType: "image/jpg")
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        let json =  try JSONSerialization .jsonObject(with:response.data!
                            , options: .allowFragments)
                        
                        let dict_JSON =  cleanJsonToObject(data: json as AnyObject) as! [String:Any]
                        print(dict_JSON)
                        
                        self.str_msg = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    }
                    catch let error as NSError {
                        print("error is:-",error)
                        completionHandler("false")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    
    func func_update_shop_image_tag(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"update_shop_image_tag"
        let params = "shop_image_id=\(shop_image_id)&shop_image_tag=\(shop_image_tag)"
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
                        
            if dict_JSON["status"] as? String == "success" {
                  self.str_msg = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    
    
}
