//
//  Ad_image_TableViewCell.swift
//  Home Mall
//
//  Created by iOS-Appentus on 07/02/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Ad_image_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_home:UIImageView!
    @IBOutlet weak var txt_ad_name:UITextField!
    @IBOutlet weak var btn_delete_image:UIButton!
    @IBOutlet weak var btn_edit_image:UIButton!
    @IBOutlet weak var view_edit_image:UIView!

    @IBOutlet weak var lbl_ad_name:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
