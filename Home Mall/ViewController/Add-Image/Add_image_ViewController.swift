//  Add_image_ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import ImagePicker

var arr_ad_name_images = [[String:Any]]()

class Add_image_ViewController: UIViewController {
    
//    @IBOutlet weak var btn_add_image:UIButton!
//    @IBOutlet weak var btn_continue:UIButton!
    
    @IBOutlet weak var tbl_add_image:UITableView!
    
    var is_images_selected = false
    
    var dict_images = ["shop_image_id":"",
                       "shop_img_id":"",
                       "shop_image_tag":"",
                       "shop_image":UIImage (named: "image-add-button.png")!] as! [String : Any]
    
    var arr_images = [[String:Any]]()
    var arr_images_added = [[String:Any]]()
    var arr_images_selected = [[String:Any]]()
    
    var is_image = false
    var shop_image_tag = ""
    var arr_shop_image_tag = [String]()
    
    let imagePickerController = ImagePickerController()
    var view_loading = UIView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController.imageLimit = 100
        
//        imagePickerController.image
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        view_loading.isHidden = true
        
        if Model_My_Ads.shared.is_edit {
            let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
            Model_Ad_Image.shared.shop_id = model.shop_id
            
            func_get_shop_detail()
        } else {
            arr_images.append(dict_images)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }
    
    @objc func func_shop_Count() {
        if Model_My_Ads.shared.is_edit {
            func_ShowHud_Error(with:Model_Tabbar.shared.message)
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.func_HideHud()
                let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                tabbar.modalPresentationStyle = .fullScreen
                self.present(tabbar, animated: true, completion: nil)
            }
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }

    func func_get_shop_detail() {
            view_loading.isHidden = false
           Model_Ad_Image.shared.func_get_shop_detail { (status) in
                DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.arr_images_added.removeAll()
                    
                if Model_Ad_Image.shared.arr_images_shops.count == 0 && self.arr_images_selected.count == 0 {
                    self.is_images_selected = false
                    self.arr_images.append(self.dict_images)
                    
                    self.tbl_add_image.reloadData()
                } else {
                    self.is_images_selected = true
                    self.is_image = true
                    
                    for _ in 0..<Model_Ad_Image.shared.arr_images_shops.count {
                        self.arr_images_added.append(self.dict_images)
                    }
                    
                    for i in 0..<Model_Ad_Image.shared.arr_images_shops.count {
                        let model = Model_Ad_Image.shared.arr_images_shops[i]
                        
                        self.arr_images_added[i]["shop_image_id"] = model.shop_image_id
                        self.arr_images_added[i]["shop_img_id"] = model.shop_img_id
                        self.arr_images_added[i]["shop_image"] = model.shop_image
                        self.arr_images_added[i]["shop_image_tag"] = model.shop_image_tag
                        
                        self.arr_shop_image_tag.append(model.shop_image_tag)
                    }
                    
                    self.arr_images = self.arr_images_added+self.arr_images_selected

                    self.tbl_add_image.reloadData()
                }
                    
            }
        }
    }
    
    func func_update_shop_image_tag() {
        view_loading.isHidden = false
        Model_Ad_Image.shared.func_update_shop_image_tag { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Ad_Image.shared.str_msg)
                } else {
                    self.func_ShowHud_Error(with: Model_Ad_Image.shared.str_msg)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
//                    self.func_get_shop_detail()
                })
            }
        }
        
    }

    
    func func_delete_shop_photo() {
        view_loading.isHidden = false
        Model_Ad_Image.shared.func_delete_shop_photo { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Ad_Image.shared.str_msg)
                } else {
                    self.func_ShowHud_Error(with: Model_Ad_Image.shared.str_msg)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
                    self.func_get_shop_detail()
                })
            }
        }
        
    }
    
    func func_update_shop_photo() {
        view_loading.isHidden = false
        Model_Ad_Image.shared.func_update_shop_photo { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Ad_Image.shared.str_msg)
                } else {
                    self.func_ShowHud_Error(with: Model_Ad_Image.shared.str_msg)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
                    self.func_get_shop_detail()
                })
            }
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_add_image(_ sender:UIButton) {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func btn_continue(_ sender:UIButton) {
        if is_image {
            arr_ad_name_images = arr_images
        }
        
        let select_your_location_VC = storyboard?.instantiateViewController(withIdentifier: "Select_Your_Location_ViewController") as! Select_Your_Location_ViewController
        select_your_location_VC.modalPresentationStyle = .fullScreen
        present(select_your_location_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_edit_image (_ sender:UIButton) {
            if arr_shop_image_tag[sender.tag] == "\(arr_images[sender.tag]["shop_image_tag"] ?? "")" {
                let alert = UIAlertController (title: "", message: "Please edit this text".localized, preferredStyle: .alert)
                
                let no = UIAlertAction(title: "ok".localized, style: .default) { (yes) in
                    
                }
                
                alert.addAction(no)
                
                alert.view.tintColor = color_app
                present(alert, animated: true, completion: nil)
                
                return
            }
        print(arr_images)
        
        let dict_image = self.arr_images[sender.tag]
        print(dict_image)
        
        Model_Ad_Image.shared.shop_image_id = "\(dict_image["shop_image_id"] ?? "")"
        Model_Ad_Image.shared.shop_image_tag = shop_image_tag
        
        self.func_update_shop_image_tag()
    }
    
    @IBAction func btn_delete_image (_ sender:UIButton) {
//        func_ChooseImage()
        
        let alert = UIAlertController (title: "", message: "Dou want to Delete?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "yes".localized, style: .default) { (yes) in
            
            if Model_My_Ads.shared.is_edit {
                let dict_image = self.arr_images[sender.tag]
                
                if let _ = dict_image["shop_image"] as? UIImage {
//                    self.arr_images.remove(at: sender.tag)
//
//                    if self.arr_images.count == 0 {
//                        self.is_images_selected = false
//                        self.is_image = false
//                        self.arr_images = [self.dict_images] as [[String : Any]]
//                    }
                    
                    self.arr_images_selected.remove(at:sender.tag-self.arr_images_added.count)
                    
                    self.arr_images = self.arr_images_added+self.arr_images_selected
                    
                    if self.arr_images.count == 0 {
                        self.is_images_selected = false
                        self.is_image = false
                        self.arr_images = [self.dict_images] as [[String : Any]]
                    }
                    
                    self.tbl_add_image.reloadData()
                } else {
                    Model_Ad_Image.shared.shop_image_id = "\(dict_image["shop_image_id"] ?? "")"
                    
                    self.func_delete_shop_photo()
                }
            } else {
//                self.arr_images.remove(at: sender.tag)
                self.arr_images_selected.remove(at:sender.tag-self.arr_images_added.count)
                
                self.arr_images = self.arr_images_added+self.arr_images_selected
                
                if self.arr_images.count == 0 {
                    self.is_images_selected = false
                    self.is_image = false
                    self.arr_images = [self.dict_images] as [[String : Any]]
                }
                
                self.tbl_add_image.reloadData()
            }
            
        }
        
        let no = UIAlertAction(title: "no".localized, style: .default) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.view.tintColor = UIColor .black
        present(alert, animated: true, completion: nil)
    }
    
}



extension Add_image_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == arr_images.count + 1 || indexPath.row == arr_images.count {
            return 60
        } else {
            return 222
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_images_selected {
            return arr_images.count+2
        } else {
            return arr_images.count+1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if is_images_selected {
            if indexPath.row == arr_images.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath)
                
                let btn_continue = cell.viewWithTag(1) as! UIButton
                
                btn_continue.layer.cornerRadius = 10
                btn_continue.layer.shadowOpacity = 3.0
                btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                btn_continue.layer.shadowRadius = 3.0
                btn_continue.layer.shadowColor = color_app.cgColor
                
                btn_continue.setTitle("Continue".localized, for: .normal)
                
                btn_continue.addTarget(self, action: #selector(btn_continue(_:)), for: .touchUpInside)
                
                return cell
            }   else if indexPath.row == arr_images.count + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-4", for: indexPath)
                
                let btn_continue = cell.viewWithTag(11) as! UIButton
                
                btn_continue.layer.cornerRadius = 10
                btn_continue.layer.shadowOpacity = 3.0
                btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                btn_continue.layer.shadowRadius = 3.0
                btn_continue.layer.shadowColor = color_app.cgColor
                btn_continue.setTitle("Add image".localized, for: .normal)
                
                btn_continue.addTarget(self, action: #selector(btn_add_image(_:)), for: .touchUpInside)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath) as! Ad_image_TableViewCell
                
                if Model_My_Ads.shared.is_edit {
                    if let img_found = arr_images[indexPath.row]["shop_image"] as? UIImage {
                        cell.img_home.image = img_found
                        cell.view_edit_image.isHidden = true
                    } else {
                        cell.view_edit_image.isHidden = false
                        cell.img_home.sd_setShowActivityIndicatorView(true)
                        cell.img_home.sd_setIndicatorStyle(.gray)
                        cell.img_home.sd_setImage(with:URL (string: "\(arr_images[indexPath.row]["shop_image"]!)"), placeholderImage:(UIImage(named:"image-add-button.png")))
                    }
                } else {
                    cell.view_edit_image.isHidden = true
                    cell.img_home.image = arr_images[indexPath.row]["shop_image"] as? UIImage
                }
                
                cell.txt_ad_name.tag = indexPath.row
                cell.txt_ad_name.delegate = self
                cell.txt_ad_name.text = (arr_images[indexPath.row]["shop_image_tag"] as! String)
                cell.lbl_ad_name.text = cell.txt_ad_name.text!
                
                if cell.lbl_ad_name.text!.isEmpty {
                    cell.lbl_ad_name.isHidden = true
                } else {
                    cell.lbl_ad_name.isHidden = false
                }
                
                cell.btn_delete_image.tag = indexPath.row
                cell.btn_delete_image.addTarget(self, action: #selector(btn_delete_image(_:)), for: .touchUpInside)
                
                cell.btn_edit_image.tag = indexPath.row
                cell.btn_edit_image.addTarget(self, action: #selector(btn_edit_image(_:)), for: .touchUpInside)
                
                return cell
            }
            
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath)
                
                return cell
            } else if indexPath.row == arr_images.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath)
                
                let btn_continue = cell.viewWithTag(1) as! UIButton
                
                btn_continue.layer.cornerRadius = 10
                btn_continue.layer.shadowOpacity = 3.0
                btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                btn_continue.layer.shadowRadius = 3.0
                btn_continue.layer.shadowColor = color_app.cgColor
                
                btn_continue.addTarget(self, action: #selector(btn_continue(_:)), for: .touchUpInside)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath) as! Ad_image_TableViewCell
                
                cell.img_home.image = arr_images[indexPath.row]["shop_image"] as? UIImage
                
                return cell
            }

        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if Model_My_Ads.shared.is_edit {
            if let _ = arr_images[indexPath.row]["shop_image"] as? UIImage {
                if !is_images_selected {
                    let imagePickerController = ImagePickerController()
                    imagePickerController.delegate = self
                    present(imagePickerController, animated: true, completion: nil)
                }
            } else {
                let dict_image = self.arr_images[indexPath.row]
                Model_Ad_Image.shared.shop_image_id = "\(dict_image["shop_image_id"] ?? "")"
                
                func_ChooseImage()
            }
        } else {
            if !is_images_selected {
                if indexPath.row == 0 {
                    let imagePickerController = ImagePickerController()
                    imagePickerController.delegate = self
                    present(imagePickerController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
}



extension Add_image_ViewController : ImagePickerDelegate {
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }

    
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
//        if arr_shop_image_tag[sender.tag] == "\(arr_images[sender.tag]["shop_image_tag"] ?? "")" {

        if Model_My_Ads.shared.is_edit {
            if Model_Ad_Image.shared.arr_images_shops.count == 0 {
                if !is_image {
                    arr_images.remove(at: 0)
                }
            }
        } else {
            if !is_image {
                arr_images.remove(at: 0)
            }
        }
        
        is_images_selected = true
        self.is_image = true
        
//        arr_images_selected.removeAll()
        
        for image in images {
            dict_images = ["shop_image_id":"",
                           "shop_img_id":"",
                           "shop_image_tag":"",
                           "shop_image":image]
            arr_images_selected.append(dict_images)
            arr_shop_image_tag.append("")
        }
        
        arr_images = arr_images_added+arr_images_selected
        
        tbl_add_image.reloadData()
        
        dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}



extension Add_image_ViewController:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        arr_images[textField.tag]["shop_image_tag"] = textField.text
        shop_image_tag = textField.text!
        
        let indexpath = IndexPath (row: textField.tag, section: 0)
        tbl_add_image.reloadRows(at: [indexpath], with: .none)
        
        if Model_My_Ads.shared.is_edit {
            let btn = UIButton()
            btn.tag = textField.tag
            btn_edit_image(btn)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
//        let indexpath = IndexPath (row: textField.tag, section: 0)
//        tbl_add_image.reloadRows(at: [indexpath], with: .none)
        
        return true
    }
    
}


extension Add_image_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func func_ChooseImage() {
        let alert = UIAlertController(title: "You Can Edit This Shop Image!".localized, message: "Please select option!".localized, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos".localized, style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        
        alert.view.tintColor = color_app
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func func_OpenCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary()    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            Model_Ad_Image.shared.img_shop_image = pickedImage
            func_update_shop_photo()
        }
        
        dismiss(animated: true, completion: nil)
    }
}











