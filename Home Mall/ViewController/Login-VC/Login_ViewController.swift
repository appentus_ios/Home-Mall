//
//  Login_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import CountryPicker
import SVProgressHUD

class Login_ViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var view_fone_number:UIView!
    @IBOutlet weak var btn_ok:UIButton!
    
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var view_CountryPicker: UIView!
    @IBOutlet weak var btn_CountryCode: UIButton!
    @IBOutlet weak var txt_phone_number: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_CountryPicker.isHidden = true
        func_AddCountryPicker()
        
        view_fone_number.layer.borderWidth = 1
        view_fone_number.layer.borderColor = color_app .cgColor
        view_fone_number.layer.cornerRadius = 16
        view_fone_number.clipsToBounds = true
        
        btn_ok.layer.cornerRadius = 10
        btn_ok.layer.shadowOpacity = 1.0
        btn_ok.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        btn_ok.layer.shadowRadius = 3.0
        btn_ok.layer.shadowColor = color_app.cgColor
        
        txt_phone_number.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        view_CountryPicker.isHidden = true
    }
    
   @IBAction func btn_ok(_ sender:UIButton) {
    let loc_MyLocation = NSLocalizedString("Enter your mobile number", comment: "")
    
    if txt_phone_number.text!.isEmpty {
            func_ShowHud_Error(with: loc_MyLocation)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        }
        
        Model_Confirm_Number.shared.str_country_code = btn_CountryCode.currentTitle!
        Model_Confirm_Number.shared.str_mobile_numbe = txt_phone_number.text!
        
        Model_Verify_Code.shared.str_country_code = btn_CountryCode.currentTitle!
        Model_Verify_Code.shared.str_mobile_numbe = txt_phone_number.text!
    
        let confirm_number = storyboard?.instantiateViewController(withIdentifier: "Confirmation_Your_Number_ViewController") as! Confirmation_Your_Number_ViewController
        confirm_number.modalPresentationStyle = .fullScreen
        present(confirm_number, animated: true, completion: nil)
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if is_from_chat {
            NotificationCenter.default.post(name: Notification.Name("hide_login_view"), object: nil)
        }
    }
    
}



extension Login_ViewController:CountryPickerDelegate {
    
    func func_AddCountryPicker() {
        let locale = Locale.current

        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)
        picker.theme = theme
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        print(phoneCode)
        print(countryCode)
        
        btn_CountryCode.setTitle(phoneCode, for:.normal)
    }

    @IBAction func btn_DoneCoutrnyPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_CountryCode(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
        self.view.endEditing(true)
    }

    
}



