//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 09/01/19.
//  Copyright © 2019 appentus. All rights reserved.

//    func func_get_shop_ad_list_lat_long() {
//    func func_get_shop_ad_list_category() {
//    func func_get_shop_ad_list_subcategory() {
//    func func_get_shop_ad_list() {


    
import UIKit



import ASStarRatingView
import GoogleMaps
import GooglePlaces
import GooglePlacesAPI



import SDWebImage



var is_From_city_myloc = ""

var is_enter_city = Bool()
var is_myLocation_tapped = Bool()

var str_city_list = ""
var str_city_my_location = ""
var cord_city_my_location:CLLocationCoordinate2D!

let color_light_gray = UIColor (red: 96.0/255.0, green: 96.0/255.0, blue: 96.0/255.0, alpha: 1.0)

class List_ViewController: UIViewController ,GMSMapViewDelegate {
    @IBOutlet weak var nav_bar:UINavigationItem!
    @IBOutlet weak var coll_cate_1:UICollectionView!
    @IBOutlet weak var coll_cate_2:UICollectionView!
    @IBOutlet weak var tbl_list:UITableView!
    
    @IBOutlet weak var mapview:GMSMapView!
    @IBOutlet weak var view_title:UIView!
    
    @IBOutlet weak var view_list:UIView!
    @IBOutlet weak var view_map:UIView!
    
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_title_1:UILabel!
    
    @IBOutlet weak var lbl_morethan_ads:UILabel!
    @IBOutlet weak var lbl_shop_are_not_avail:UILabel!
    
    @IBOutlet weak var view_cate:UIView!
    
    var arr_list = [CLLocationCoordinate2D]()
    
    var is_show_marker_window = false
    var arr_markers = [GMSMarker]()
    
    var  arr_cate = [Model_Category_List]()
    var  arr_sub_cate = [Model_Category_List]()
    
    var arr_is_selected_cell = [Bool]()
    
    var arr_cate_is_selected_cell = [Bool]()
    var arr_subcate_is_selected_cell = [Bool]()
    
    var locationManager = CLLocationManager()
    var arr_selected_marker_gray = [CLLocationCoordinate2D]()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    var selected_lat = 0.0
    var selected_long = 0.0
    
    var selected_marker_index = -1
    
    var is_tap_window = 0
    var is_updated_location = false
    var is_loaded_first_time = false
     
    var is_padding = false
    
    var view_loader = UIView()
    var marker_SelectedLocation = GMSMarker()
    
    var arr_cate_code = [String]()
    var arr_sub_cate_code = [String]()
    
    var arr_selected_marker = [Bool]()
    
    var str_there_are_more_than = ""
    var str_ads = ""
    
    var is_zoom = false
    var is_Idle = true
    
    var is_from_apear = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for _ in Model_Search_VC.shared.arr_list_View {
//            arr_is_selected_cell.append(false)
//        }
//        tbl_list.reloadData()
        
        is_tap_window = 1
        
        view_cate.layer.shadowOpacity = 2.5
        view_cate.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_cate.layer.shadowRadius = 1.5
        view_cate.layer.shadowColor = UIColor .gray.cgColor
        
        str_there_are_more_than = "There are more than".localized
        str_ads = "ads".localized
        
        self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
        
        NotificationCenter.default.addObserver(self,selector: #selector(func_show_list_map),name:NSNotification.Name (rawValue: "map_or_list"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_user_mobile), name: NSNotification.Name (rawValue:"user_mobile_search"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(search_by_cate), name: NSNotification.Name (rawValue:"search_by_cate_list"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selected_location), name: NSNotification.Name (rawValue:"selected_location_list"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_image_ads_city), name: NSNotification.Name (rawValue:"image_ads_list"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_back), name: NSNotification.Name (rawValue:"back"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_selected_location), name: NSNotification.Name (rawValue: "selected_location_google_list"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_open_google), name: NSNotification.Name (rawValue: "open_google_list"), object: nil)
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        coll_cate_1.layer.cornerRadius = coll_cate_1.frame.size.height/2
        coll_cate_2.layer.cornerRadius = coll_cate_2.frame.size.height/2
        
        view_title.layer.shadowOpacity = 3.0
        view_title.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_title.layer.shadowRadius = 3.0
        view_title.layer.shadowColor = UIColor .lightGray.cgColor
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        mapview.isMyLocationEnabled = true
        mapview.settings.myLocationButton = true
        
//        Model_Search_VC.shared.arr_list_View.removeAll()
//        Model_Search_VC.shared.category_code = ""
//        Model_Search_VC.shared.subcate_code = ""
        
        is_loaded_first_time = false
//        arr_markers.removeAll()
        
        view_map.isHidden = true
        
//        get_shop_ad_list()
        
        if arr_selected_marker_gray.count > 0 {
            arr_selected_marker_gray.removeAll()
        }
        
        self.mapview.clear()
        self.lbl_shop_are_not_avail.isHidden = true
        
        is_show_marker_window = false
        mapview.delegate = self
//        Model_Search_VC.shared.arr_list_View.removeAll()
        
//        is_loaded_first_time = false
        is_padding = false
        
//        arr_markers.removeAll()
        func_get_category()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        is_zoom = false
        if !func_IsLocationEnable() {
            return 
        }
        
        func_list_view()
    }
    
    func func_list_view() {
        is_from_apear = true
        
        self.lbl_shop_are_not_avail.isHidden = true
        self.arr_list.removeAll()
        self.arr_is_selected_cell.removeAll()
        self.arr_selected_marker.removeAll()
        
        self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_Search_VC.shared.arr_list_View.count) \(self.str_ads)"
        
        is_Idle = false
        is_tap_window = 0
        is_padding = false
        
        if is_From_city_myloc == "mylocation" {
            self.lbl_title.text = str_city_my_location
            self.lbl_title_1 .text = str_city_my_location
            self.nav_bar.title = str_city_my_location
            
            co_OrdinateCurrent = cord_city_my_location
            
            let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
            let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
            
            mapview.camera = camera
            mapview.animate(to: camera)
            
            mapview.clear()
            
            is_zoom = true
        } else {
            if is_enter_city || is_myLocation_tapped {
                mapview.clear()
                
                is_myLocation_tapped = false
                
                self.lbl_title.text = str_city_my_location
                self.lbl_title_1 .text = str_city_my_location
                self.nav_bar.title = str_city_my_location
                
                co_OrdinateCurrent = cord_city_my_location
                
                var zoomlevel = 0.0
                if is_From_city_myloc == "mylocation" {
                    zoomlevel = 13.9
                } else {
                    zoomlevel = 9.0
                }
                
                let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
                let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: Float(zoomlevel))
                
                mapview.camera = camera
                mapview.animate(to: camera)
                
                if is_enter_city {
                    if Model_Search_VC.shared.arr_list_View.count == 0 {
                        is_zoom = true
                    }
                } else {
                    is_zoom = true
                }
            }
        }
        
        if Model_Search_VC.shared.arr_list_View.count > 0 {
            if cord_city_my_location != nil {
                self.lbl_title.text = str_city_my_location
                self.lbl_title_1 .text = str_city_my_location
                self.nav_bar.title = str_city_my_location
                
                co_OrdinateCurrent = cord_city_my_location
                
                let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
                let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
                
                mapview.camera = camera
                mapview.animate(to: camera)
                
                for model in Model_Search_VC.shared.arr_list_View {
                    var shop_lat = 0.0
                    var shop_lang = 0.0
                    
                    if !model.shop_lat.isEmpty {
                        shop_lat = Double(model.shop_lat)!
                    }
                    
                    if !model.shop_lang.isEmpty {
                        shop_lang = Double(model.shop_lang)!
                    }
                    
                    self.arr_selected_marker.append(false)
                    self.arr_is_selected_cell.append(false)
                    self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                }
                
                self.func_list()
                self.tbl_list.reloadData()
            }
        }
        
        is_Idle = true

//        if Model_Search_VC.shared.arr_list_View.count == 0 {
//            is_updated_location = false
//            locationManager.startUpdatingLocation()
//        }

    }
    
    @objc func func_open_google() {
        var gmsautocomplete = GMSAutocompleteViewController()
        
//        let filter = GMSAutocompleteFilter()
//        filter.type = .establishment
//        filter.country = "AE"
//
//
//        gmsautocomplete.autocompleteFilter = filter
        
        gmsautocomplete.delegate = self
        present(gmsautocomplete, animated: true, completion: nil)
    }
    
    
    
    @objc func func_image_ads_city()  {
        is_loaded_first_time = false
        func_call_api()
    }

    @objc func func_back()  {
        mapview.clear()
        func_get_category()
        func_call_api()
    }

    @objc func func_show_list_map() {
        is_loaded_first_time = false
        if is_list {
//            view_list.isHidden = true
            tbl_list.isHidden = true
            view_map.isHidden = false
        } else {
//            view_list.isHidden = false
            tbl_list.isHidden = false
            view_map.isHidden = true
            
            self.tbl_list.reloadData()
        }
    }
    
    @IBAction func btn_search_location(_ sender:UIButton) {
        str_city_list = "list"
        
        if is_menu_open {
            
        } else {
            onSlideMenuButtonPressed(sender)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func func_user_mobile() {
        is_zoom = false
        is_padding = false
        is_loaded_first_time = false
        
        is_zoom = false
        is_padding = false
        is_loaded_first_time = false
        
        func_get_shop_ad_list_mobile { (_) in
            self.tbl_list.reloadData()
        }
        
    }
    
    @objc func func_get_category() {
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_cate.removeAll()
                
                if status == "success" {
                    self.arr_cate = Model_Category_List.shared.arr_category_list
                }
                
                for i in 0..<self.arr_cate.count {
//                    if i == 0 {
//                        self.arr_cate_is_selected_cell.append(true)
//                        Model_Category_List.shared.category_code = Model_Category_List.shared.arr_category_list[1].category_code
//                    } else {
                        self.arr_cate_is_selected_cell.append(false)
//                    }
                }
//                self.func_get_sub_category()
                self.coll_cate_1.reloadData()
            }
        }
    }
    
    func func_get_sub_category() {
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_sub_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_sub_cate.removeAll()
                if status == "success" {
                    self.arr_sub_cate = Model_Category_List.shared.arr_sub_category_list
                }
                for _ in 0..<self.arr_sub_cate.count {
                    self.arr_subcate_is_selected_cell.append(false)
                }
                self.coll_cate_2.reloadData()
            }
        }
    }
    
    @objc func search_by_cate() {
        is_loaded_first_time = false
        
//        arr_cate_is_selected_cell.removeAll()
        
        arr_sub_cate.removeAll()
        coll_cate_2.reloadData()
        
        func_get_shop_ad_list_category { (_) in
            self.tbl_list.reloadData()
        }
        
//        func_get_category()
    }
    
    @objc func selected_location() {
        is_loaded_first_time = false
        
        co_OrdinateCurrent = CLLocationCoordinate2D (latitude: Double(Model_Search_VC.shared.location_lat)!, longitude: Double(Model_Search_VC.shared.location_lang)!)
        
        let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
        
        mapview.camera = camera
        mapview.animate(to: camera)
        
        if is_Idle {
            func_Geocoder(co_OrdinateCurrent) { (address) in
                DispatchQueue.main.async {
                    self.lbl_title.text = address
                    self.lbl_title_1 .text = address
                    self.nav_bar.title = address
                }
            }
            
            func_get_shop_ad_list_lat_long { (_) in
                self.tbl_list.reloadData()
            }

        }
    }

}



extension List_ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 157
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Search_VC.shared.arr_list_View .count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! List_TableViewCell
        
        if Model_Search_VC.shared.arr_list_View .count > 0 {
            let model = Model_Search_VC.shared.arr_list_View[indexPath.row]
            
            cell.image_shadow.sd_setShowActivityIndicatorView(true)
            cell.image_shadow.sd_setIndicatorStyle(.gray)
            cell.image_shadow.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
            
            cell.lbl_name.text = model.shop_name
            cell.lbl_owner_name.text = model.user_name
            cell.lbl_location_address.text = model.shop_location
            cell.lbl_join_date.text = model.shop_add_date.date_localized
            
            if model.avg_rating.isEmpty {
                cell.star_view.rating = 0
            } else {
                cell.star_view.rating = Float(model.avg_rating)!
            }
            
            if arr_is_selected_cell.count > 0 {
                if arr_is_selected_cell[indexPath.row] {
                    cell.lbl_name.textColor = UIColor .white
                    cell.lbl_location_address.textColor = color_light_gray
                    
                    cell.img_next_arrow.image = cell.img_next_arrow.image?.imageWithColor(color1: UIColor .white)
                    
                    cell.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                    cell.view_container.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                    cell.view_container_1.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                    cell.view_name.layer.cornerRadius = cell.view_name.frame.size.height/2
                    cell.view_name.clipsToBounds = true
                    
                    cell.view_next_arrow.backgroundColor = UIColor (red: 89.0/255.0, green: 227.0/255.0, blue: 186.0/255.0, alpha: 1.0)
                    
                    cell.view_container.layer.shadowOpacity = 0.0
                    cell.view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                    cell.view_container.layer.shadowRadius = 0.0
                    cell.view_container.layer.shadowColor = UIColor .clear.cgColor
                    
                    cell.view_name.backgroundColor = UIColor .darkGray
                    func_set(view_container: cell.view_container,color: UIColor .gray)
                    //                func_set_view_name(view_container: cell.view_name,color: UIColor .clear, cornerRadius: cell.view_name.frame.size.height/2)
                } else {
                    cell.lbl_name.textColor = color_light_gray
                    cell.lbl_location_address.textColor = UIColor (red: 255.0/255.0, green: 197.0/255.0, blue: 58.0/255.0, alpha: 1.0)
                    
                    cell.img_next_arrow.image = cell.img_next_arrow.image?.imageWithColor(color1: color_light_gray)
                    
                    cell.backgroundColor = UIColor .clear
                    cell.view_container_1.backgroundColor = UIColor .white
                    cell.view_name.layer.cornerRadius = cell.view_name.frame.size.height/2
                    cell.view_name.clipsToBounds = true
                    
                    cell.view_container.backgroundColor = UIColor .white
                    cell.view_next_arrow.backgroundColor = UIColor (red: 241.0/255.0, green: 242.0/255.0, blue: 243.0/255.0, alpha: 1.0)
                    
                    func_set(view_container: cell.view_container,color: UIColor.gray)
                }
            }

        }
        
        return cell
    }
    
    func shadowOnviewWithcornerRadius(YourView:AnyObject ,shadowcolor:UIColor) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: false)
        
        for i in 0..<Model_Search_VC.shared.arr_list_View.count {
            if i == indexPath.row {
                arr_is_selected_cell[i] = true
            } else {
                arr_is_selected_cell[i] = false
            }
            
        }
        
        Model_Details.shared.index_selected = indexPath.row
        Model_Details.shared.arr_details = Model_Search_VC.shared.arr_list_View
        
        tbl_list.reloadData()
        func_present_Fav_VC()
    }
    
    @IBAction func btn_select_rows(_sender:UIButton) {
        
    }
    
    func func_set(view_container:UIView ,color:UIColor) {
//        view_container.layer.cornerRadius = 6
        view_container.layer.shadowOpacity = 5.0
        view_container.layer.shadowOffset = CGSize(width: 8.0, height:8.0)
        view_container.layer.shadowRadius = 5.0
        view_container.layer.shadowColor = color.cgColor
    }
    
    func func_set_view_name(view_container:UIView ,color:UIColor,cornerRadius:CGFloat) {
        view_container.layer .cornerRadius = cornerRadius
        view_container.layer.shadowOpacity = 10.0
        view_container.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        view_container.layer.shadowRadius = 10.0
        view_container.layer.shadowColor = UIColor .black.cgColor
    }
    
}

extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}


//  MARK:- UICollectionView methods
extension List_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets (top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == coll_cate_1 {
            let model = arr_cate[indexPath.row]
            let width = func_size(width:100, messageText: model.category_name).width+14
            return CGSize (width: width, height: collectionView.frame.size.height)
            
            //            return CGSize (width: 60, height: collectionView.frame.size.height)
        } else {
            let model = arr_sub_cate[indexPath.row]
            let width = func_size(width:100, messageText: model.subcate_name).width+14
            return CGSize (width: width, height: collectionView.frame.size.height)
            
            //            return CGSize (width: 80, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == coll_cate_1 {
            return arr_cate.count
        } else {
            return arr_sub_cate.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == coll_cate_1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell-1", for: indexPath)
            
            let lbl_cate = cell.viewWithTag(1) as? UILabel
            let model = arr_cate[indexPath.row]
            lbl_cate?.text = model.category_name
            
            if arr_cate_is_selected_cell[indexPath.row] {
                lbl_cate?.textColor = UIColor .black
            } else {
                lbl_cate?.textColor = UIColor .white
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell-2", for: indexPath)
            
            let lbl_cate = cell.viewWithTag(1) as? UILabel
            let model = arr_sub_cate[indexPath.row]
            lbl_cate?.text = model.subcate_name
            
            if arr_subcate_is_selected_cell[indexPath.row] {
                lbl_cate?.textColor = UIColor .black
            } else {
                lbl_cate?.textColor = UIColor .white
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Model_Search_VC.shared.raduis_city = "\(mapview.getRadius())"
        
        if !is_list {
            is_loaded_first_time = false
        }
        
        if collectionView == coll_cate_1 {
            is_padding = false
            is_loaded_first_time = false
            
            for i in 0..<arr_cate_is_selected_cell.count {
                if i == indexPath.row {
                    arr_cate_is_selected_cell[i] = true
                }  else {
                    arr_cate_is_selected_cell[i] = false
                }
            }
            
            
            Model_Search_VC.shared.subcate_code = ""
            Model_My_Location.shared.subcate_code = ""
            Model_City.shared.subcate_code = ""

            Model_Search_VC.shared.category_code = arr_cate[indexPath.row].category_code
            Model_Category_List.shared.category_code = arr_cate[indexPath.row].category_code
            
            coll_cate_1 .reloadData()
            Model_Search_VC.shared.subcate_code = ""
            Model_City.shared.subcate_code = ""
            arr_subcate_is_selected_cell.removeAll()
            
            if Model_Category_List.shared.category_code.isEmpty {
                arr_sub_cate.removeAll()
                arr_subcate_is_selected_cell.removeAll()
                self.coll_cate_2.reloadData()
            } else {
                func_get_sub_category()
            }
        } else {
            for i in 0..<arr_subcate_is_selected_cell.count {
                if i == indexPath.row {
                    arr_subcate_is_selected_cell[i] = true
                }  else {
                    arr_subcate_is_selected_cell[i] = false
                }
            }
            
            Model_Search_VC.shared.subcate_code  = arr_sub_cate[indexPath.row].subcate_code
            Model_My_Location.shared.subcate_code = arr_sub_cate[indexPath.row].subcate_code
            Model_City.shared.subcate_code = Model_Search_VC.shared.subcate_code
            
            coll_cate_2.reloadData()
        }
        func_call_api()
    }

    
}


// MARK:-  API METHODS
extension List_ViewController {
    func func_get_shop_ad_list_lat_long(completion:@escaping (Bool)->()) {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_Search_VC.shared.func_get_shop_ad_list_lat_long { (status) in
            DispatchQueue.main.async {
            self.func_HideHud()
                
            self.view_loader.isHidden = true
            self.is_loaded_first_time = true
            
            self.lbl_shop_are_not_avail.isHidden = true
            self.arr_list.removeAll()
            self.arr_is_selected_cell.removeAll()
            self.arr_selected_marker.removeAll()
                
                if status == "success" {
                    for model in Model_Search_VC.shared.arr_list_View {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_selected_marker.append(false)
                        self.arr_is_selected_cell.append(false)
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    completion(true)
                } else {
                    let title = NSLocalizedString("Welcome", comment: "")
//                    self.func_ShowHud_Error(with: title)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.lbl_shop_are_not_avail.isHidden = false
                    })
                    completion(true)
                }
                
                if Model_Search_VC.shared.arr_list_View.count > 0 {
                    self.tbl_list.isHidden = false
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_Search_VC.shared.arr_list_View.count) \(self.str_ads)"
                } else {
                    let title = NSLocalizedString("Welcome", comment: "")
                    self.lbl_shop_are_not_avail.text = title
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                    self.tbl_list.isHidden = true
                }
                
                self.func_list()
                
            }
        }
    }
    
    
    func func_get_shop_ad_list_category(completion:@escaping (Bool)->()) {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_Search_VC.shared.func_get_shop_ad_list_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.lbl_shop_are_not_avail.isHidden = true
                self.func_HideHud()
                
                self.arr_list.removeAll()
                self.arr_is_selected_cell.removeAll()
                self.arr_selected_marker.removeAll()
                
                if status == "success" {
                    for model in Model_Search_VC.shared.arr_list_View {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_selected_marker.append(false)
                        
                        self.arr_is_selected_cell.append(false)
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    completion(true)
                }  else {
                    let title = NSLocalizedString("Welcome", comment: "")
//                    self.func_ShowHud_Error(with: title)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.lbl_shop_are_not_avail.isHidden = false
                    })
                    completion(true)
                }
                
                if Model_Search_VC.shared.arr_list_View.count > 0 {
                    self.tbl_list.isHidden = false
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_Search_VC.shared.arr_list_View.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                    self.tbl_list.isHidden = true
                }
                self.func_list()
                
            }
        }
    }
    
    
    func func_get_shop_ad_list_subcategory(completion:@escaping (Bool)->()) {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_Search_VC.shared.func_get_shop_ad_list_subcategory { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
               self.func_HideHud()
                self.lbl_shop_are_not_avail.isHidden = true
                self.arr_list.removeAll()
                self.arr_is_selected_cell.removeAll()
                self.arr_selected_marker.removeAll()
                
                if status == "success" {
                    for model in Model_Search_VC.shared.arr_list_View {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_selected_marker.append(false)
                        
                        self.arr_is_selected_cell.append(false)
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    completion(true)
                } else {
                    let title = NSLocalizedString("Welcome", comment: "")
//                    self.func_ShowHud_Error(with: title)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.lbl_shop_are_not_avail.isHidden = false
                    })
                    completion(true)
                }
                
                if Model_Search_VC.shared.arr_list_View.count > 0 {
                    self.tbl_list.isHidden = false
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_Search_VC.shared.arr_list_View.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                    self.tbl_list.isHidden = true
                }
                self.func_list()
                
            }
        }
    }
    
    func func_get_shop_ad_list_mobile(completion:@escaping (Bool)->()) {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_Search_VC.shared.func_get_shop_ad_list_mobile { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                
                self.lbl_shop_are_not_avail.isHidden = true
                self.arr_list.removeAll()
                self.arr_is_selected_cell.removeAll()
                self.arr_selected_marker.removeAll()
                
                if status == "success" {
                    for model in Model_Search_VC.shared.arr_list_View {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_selected_marker.append(false)
                        
                        self.arr_is_selected_cell.append(false)
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    completion(true)
                }  else {
                    let title = NSLocalizedString("Welcome", comment: "")
//                    self.func_ShowHud_Error(with: title)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.lbl_shop_are_not_avail.isHidden = false
                    })
                    completion(true)
                }
                
                if Model_Search_VC.shared.arr_list_View.count > 0 {
                    self.tbl_list.isHidden = false
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_Search_VC.shared.arr_list_View.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                    self.tbl_list.isHidden = true
                }
                self.func_list()
            }
        }
    }
    
    
    
    func func_get_shop_ad_list(completion:@escaping (Bool)->()) {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_Search_VC.shared.func_get_shop_ad_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.arr_list.removeAll()
                self.arr_is_selected_cell.removeAll()
                self.lbl_shop_are_not_avail.isHidden = true
                self.arr_selected_marker.removeAll()
                
                if status == "success" {
                    for model in Model_Search_VC.shared.arr_list_View {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_selected_marker.append(false)
                        
                        self.arr_is_selected_cell.append(false)
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    completion(true)
                } else {
                    let title = NSLocalizedString("Welcome", comment: "")
//                    self.func_ShowHud_Error(with: title)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.lbl_shop_are_not_avail.isHidden = false
                    })
                    completion(true)
                }
                
                if Model_Search_VC.shared.arr_list_View.count > 0 {
                    self.tbl_list.isHidden = false
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_Search_VC.shared.arr_list_View.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                    self.tbl_list.isHidden = true
                }
                self.func_list()
            }
        }
    }
    
    
    
    @objc func func_call_api() {
        if Model_Search_VC.shared.category_code.isEmpty && Model_Search_VC.shared.subcate_code.isEmpty {
            func_get_shop_ad_list_lat_long { (_) in
                self.tbl_list.reloadData()
            }
        } else if Model_Search_VC.shared.category_code.isEmpty || Model_Search_VC.shared.subcate_code.isEmpty {
            if !Model_Search_VC.shared.category_code.isEmpty {
                func_get_shop_ad_list_category{ (_) in
                    self.tbl_list.reloadData()
                }
            } else {
                func_get_shop_ad_list_subcategory { (_) in
                    self.tbl_list.reloadData()
                }
            }
        } else if !Model_My_Location.shared.category_code.isEmpty && !Model_My_Location.shared.subcate_code.isEmpty {
            func_get_shop_ad_list_subcategory { (_) in
                self.tbl_list.reloadData()
            }
        } else if Model_Search_VC.shared.subcate_code.contains(",") || Model_Search_VC.shared.category_code.contains(",") {
            func_get_shop_ad_list_subcategory{ (_) in
                self.tbl_list.reloadData()
            }
        } else {
            func_get_shop_ad_list_subcategory{ (_) in
                self.tbl_list.reloadData()
            }
            
//            func_get_shop_ad_list{ (_) in
//                self.tbl_list.reloadData()
//            }
        }
//        self.tbl_list .reloadData()
    }
}



// MARK:-  GMSMAP VIEW DELEGATE METHODS
extension  List_ViewController {
    func func_list() {
        if marker_SelectedLocation != nil {
            mapview.clear()
        }
        
        var marker = GMSMarker()
        var bounds = GMSCoordinateBounds()
        
        arr_markers.removeAll()
        
        for i in 0..<arr_list.count {
            let cord = arr_list[i]
            marker = GMSMarker(position: cord)
            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
            var img_marker = UIImageView()
            
            img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
            
            if let selected_locations = loadCoordinates() {
                arr_selected_marker_gray = selected_locations
                
                for ii in 0..<arr_selected_marker_gray.count {
                    if arr_selected_marker_gray[ii].latitude == cord.latitude && arr_selected_marker_gray[ii].latitude == cord.latitude {
                        img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
                        
                        break
                    }
                }
            }
            
            img_marker.frame = view_hotel.frame
            
            view_hotel.addSubview(img_marker)
            marker.iconView = view_hotel
            mapview.bringSubview(toFront:view_hotel)
            
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 2.5)
            marker.zIndex = Int32(i)
            
            arr_markers.append(marker)
            
            marker.map = self.mapview
            bounds = bounds .includingCoordinate(cord)
        }
        
        if !is_padding {
            is_padding = true
            
            if arr_list.count == 1 {
                let cord = arr_list[0]
                marker = GMSMarker(position: cord)
                
                let position = CLLocationCoordinate2D(latitude:cord.latitude, longitude:cord.longitude)
                
                var zoomlevel = 0.0
                if is_From_city_myloc == "mylocation" {
                    zoomlevel = 13.9
                } else {
                    zoomlevel = 9.0
                }
                
                let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom:Float(zoomlevel))
                
                mapview.camera = camera
                mapview.animate(to: camera)
            } else if arr_list.count > 1 {
                if is_From_city_myloc == "mylocation" {
                    
                } else {
                    let update = GMSCameraUpdate.fit(bounds, withPadding: 160)
                    mapview.animate(with: update)
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.is_zoom = true
        }
        
    }
    
    func func_present_Fav_VC() {
        Model_Details.shared.arr_details = Model_Search_VC.shared.arr_list_View
        
        let fav_VC = storyboard?.instantiateViewController(withIdentifier: "Details_ViewController") as! Details_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        present(fav_VC, animated: true, completion: nil)
    }
    
//    func highlightMarker (marker:GMSMarker) {
//        if self.mapview .selectedMarker == marker {
//            marker.icon =
//        }
//    }
    
    
    
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if !func_IsLocationEnable() {
            return false
        }
        
        is_padding = false
        
        if let _ = mapView.myLocation?.coordinate {
            
        } else { return false }
        
        co_OrdinateCurrent = (mapView.myLocation?.coordinate)!
        
        Model_Search_VC.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
        Model_Search_VC.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
        
        co_OrdinateCurrent = CLLocationCoordinate2D (latitude: Double(Model_Search_VC.shared.location_lat)!, longitude: Double(Model_Search_VC.shared.location_lang)!)
        
        selected_location()
        
        func_Geocoder(co_OrdinateCurrent) { (address) in
            DispatchQueue.main.async {
                    self.lbl_title.text = address
                    self.lbl_title_1 .text = address
                    self.nav_bar.title = address
            }
        }
        
        func_call_api()
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !func_IsLocationEnable() {
            return
        }
        
        Model_Search_VC.shared.raduis_city = "\(mapview.getRadius())"
        
        if is_zoom {
            if is_tap_window == 0 {
                co_OrdinateCurrent = position.target
                
                Model_Search_VC.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
                Model_Search_VC.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
                
                if is_Idle {
                    func_Geocoder(co_OrdinateCurrent) { (address) in
                        DispatchQueue.main.async {
                            self.lbl_title.text = address
                            self.lbl_title_1 .text = address
                            self.nav_bar.title = address
                        }
                    }
                    func_call_api()
                }
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        func_present_Fav_VC()
    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        is_tap_window = 0
        
//        if arr_selected_marker
//        let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
//        let img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
//        img_marker.frame = view_hotel.frame
//        view_hotel.addSubview(img_marker)
//        marker.iconView = view_hotel
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//        if let index = arr_markers.index(of: marker) {
//            print(index)
//
//            for i in 0..<arr_markers.count {
//                if i == index {
//                    arr_selected_marker[i] = true
//                } else {
//                    arr_selected_marker[i] = false
//                }
//            }
//            func_reload_map()
//        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
//        for i in 0..<arr_markers.count{
//            let img = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
//            img.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//            arr_markers[i].iconView = img
//        }
        let img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
        
        img_marker.frame = view_hotel.frame
        view_hotel.addSubview(img_marker)
        marker.iconView = view_hotel
        
        is_tap_window = 1
        
        let marker_Info = Bundle.main.loadNibNamed("Marker_Info", owner: self, options: nil)?.first as! Marker_Info
        
        marker_Info.frame = CGRect(x: self.view.center.x, y: self.view.center.y + 150, width: self.view.frame.width-30, height: 150)
        if let index = arr_markers.index(of: marker) {
            print(index)
            
            selected_long = arr_list[index].longitude
            selected_lat = arr_list[index].latitude
            
//            if arr_selected_marker_gray.count == 0 {
//                arr_selected_marker_gray.append(CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude))
//            } else {
//                for i in 0..<arr_selected_marker_gray.count {
//                    let location_cord = arr_selected_marker_gray[i]
//                    if location_cord.latitude == arr_list[index].latitude && location_cord.latitude == arr_list[index].latitude  {
//                        arr_selected_marker_gray[i] = CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude)
//
//                        break
//                    } else {
//                        arr_selected_marker_gray.append(CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude))
//
//                        break
//                    }
//                }
//            }
            
            if let selected_locations = loadCoordinates() {
                arr_selected_marker_gray = selected_locations
                
                for i in 0..<arr_selected_marker_gray.count {
                    let location_cord = arr_selected_marker_gray[i]
                    if location_cord.latitude == arr_list[index].latitude && location_cord.latitude == arr_list[index].latitude  {
                        arr_selected_marker_gray[i] = CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude)
                        
                        break
                    } else {
                        arr_selected_marker_gray.append(CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude))
                        
                        break
                    }
                }
            } else {
                arr_selected_marker_gray.append(CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude))
            }
            
            storeCoordinates(arr_selected_marker_gray)
            
            Model_Details.shared.index_selected = index
            let model = Model_Search_VC.shared.arr_list_View[index]
            
            marker_Info.img.sd_setShowActivityIndicatorView(true)
            marker_Info.img.sd_setIndicatorStyle(.gray)
            marker_Info.img.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
            
            marker_Info.lbl_name.text = model.shop_name
            marker_Info.lbl_owner_name.text = model.user_name
            marker_Info.lbl_location_address.text = model.shop_address
            marker_Info.lbl_date_of_update.text = model.shop_add_date.date_localized
            
            if model.avg_rating.isEmpty {
                marker_Info.star.rating = 0
            } else  {
                marker_Info.star.rating = Float(model.avg_rating)!
            }
        }
        return marker_Info
    }
    
    func func_reload_map() {
//        if marker_SelectedLocation != nil {
//            mapview.clear()
//        }
        
        var marker = GMSMarker()
        var bounds = GMSCoordinateBounds()
        
        arr_markers.removeAll()
        
        for i in 0..<arr_list.count {
            let cord = arr_list[i]
            marker = GMSMarker(position: cord)
            
            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
            var img_marker = UIImageView()
            
            if arr_selected_marker[i] {
                let position = CLLocationCoordinate2D(latitude:cord.latitude, longitude:cord.longitude)
                let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: mapview.camera.zoom)
                
                mapview = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
                
                img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
            } else {
                img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
            }
            
            img_marker.frame = view_hotel.frame
            
            view_hotel.addSubview(img_marker)
            marker.iconView = view_hotel
            mapview.bringSubview(toFront:view_hotel)
            
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 2.5)
            marker.zIndex = Int32(i)
            
//            arr_markers[i] = marker
            arr_markers.append(marker)
            marker.map = self.mapview
            
            bounds = bounds .includingCoordinate(cord)
        }
        
    }
    
    
}



extension List_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//            if !self.is_updated_location {
//                self.is_updated_location = true
//
//                self.co_OrdinateCurrent = manager.location!.coordinate
//                Model_Search_VC.shared.location_lat = "\(self.co_OrdinateCurrent.latitude)"
//                Model_Search_VC.shared.location_lang = "\(self.co_OrdinateCurrent.longitude)"
//
//                let position = CLLocationCoordinate2D(latitude:self.co_OrdinateCurrent.latitude, longitude:self.co_OrdinateCurrent.longitude)
//                let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
//
//                self.mapview.camera = camera
//                self.mapview.animate(to: camera)
//
//                self.func_Geocoder(self.co_OrdinateCurrent) { (address) in
//                    DispatchQueue.main.async {
//                        self.lbl_title.text = address
//                        self.lbl_title_1 .text = address
//                        self.nav_bar.title = address
//                        self.is_tap_window = 0
//                    }
//                }
//            }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("",error)
    }
    
    func func_Geocoder(_ coordinate: CLLocationCoordinate2D,completion:@escaping (String)->()) {
        let cllocation = CLLocation (latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        //        print("\(Locale.current.regionCode)")
        
        var languageCode = ""
        if "\(Locale.current.regionCode ?? "")".lowercased() == "in" {
            languageCode = "en"
        } else {
            languageCode = "ar"
        }
        
        if #available(iOS 11.0, *) {
            CLGeocoder().reverseGeocodeLocation(cllocation, preferredLocale:Locale.init(identifier: languageCode), completionHandler: {
                (placemarks, error) -> Void in
                
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    let dict_Address = pm.addressDictionary
                    //                    print("\(dict_Address!["Country"]!)")
                    //                    print("\(dict_Address!["State"]!)")
                    //                    print("\(dict_Address!["SubAdministrativeArea"]!)")
                    
//                                        print("\(dict_Address!["SubLocality"]!)")
//                                        print("\(dict_Address!["City"]!)")
                    
                    var string_final = "\(dict_Address!["SubLocality"] ?? "") \(dict_Address!["City"] ?? "")"
                    
//                    if dict_Address!["FormattedAddressLines"] != nil{
//                        let str = dict_Address!["FormattedAddressLines"]! as! NSArray
//
//                        for i in 0..<str.count{
//                            string_final += "\(str[i]) "
//                        }
//                    }
                    
//                    string_final = string_final.replacingOccurrences(of:"\(dict_Address!["Country"]!)", with: "")
                    
                    //                    let str_Street = dict_Address!["Street"] ?? ""
                    //                    let str_Name = dict_Address!["Name"] ?? ""
                    //                    let str_City = dict_Address!["City"] ?? ""
                    //
                    //                    let str_state = dict_Address!["State"] ?? ""
                    //                    let str_country = dict_Address!["Country"] ?? ""
                    
                    completion(string_final)
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
        } else {
            // Fallback on earlier versions
        }
        
        
        
//        let geocoder = GMSGeocoder()
        
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            guard let address = response?.firstResult() else {
//                let localized = "Address not found ".localized
//                completion(localized)
//                return
//            }
//
//            var str_Address = ""
//
//            if let _ = address.subLocality {
//                str_Address = "\(address.subLocality ?? "") , \(address.locality ?? "")"
//            } else if let _ = address.locality {
//                str_Address = "\(address.locality ?? "") , \(address.administrativeArea ?? "")"
//            } else if let _ = address.administrativeArea {
//                str_Address = "\(address.administrativeArea ?? "") , \(address.country ?? "")"
//            } else if let _ = address.country {
//                str_Address = "\(address.locality ?? "")"
//            }
//
//            completion(str_Address)
//        }
    }
    
}



extension List_ViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let localized = "Address not found ".localized
        let str_selectYourLocations = place.formattedAddress ?? localized
        Model_Last_Searched.shared.city_name = str_selectYourLocations
        
        Model_Last_Searched.shared.city_id = place.placeID
        Model_Last_Searched.shared.city_lat = "\(place.coordinate.latitude)"
        Model_Last_Searched.shared.city_long = "\(place.coordinate.longitude)"
        
        func_save_city()
        
        Model_Search_VC.shared.location_lat = "\(place.coordinate.latitude)"
        Model_Search_VC.shared.location_lang = "\(place.coordinate.longitude)"
        
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_google_list"), object: nil)
    }
    
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        
        
//        Log.debug("Place name: \(String(describing: place.name))")
//        Log.debug("Place address: \(String(describing: place.formattedAddress))")
//        Log.debug("Place attributions: \(String(describing: place.attributions))")
        
        let placeID = place.placeID
        
//        guard let placeID = place.placeID else {
//            return
//        }
        
        /// Use Google Place API to lookup place information in English
        /// https://developers.google.com/places/web-service/intro
        GooglePlacesAPI.GooglePlaces.placeDetails(forPlaceID: placeID, language: "AE") { (response, error) -> Void in
            
            guard let place = response?.result, response?.status == GooglePlaces.StatusCode.ok else {
                print("Error = \(String(describing: response?.errorMessage))")
                
                return
            }
            
            let postalCode = place.addressComponents.filter({ (components) -> Bool in
                return components.types.contains(kGMSPlaceTypePostalCode)
            }).first?.longName
            
            print(place)
            print(place.formattedAddress)
            print(place.name)
        }
    }
        
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func func_selected_location() {
        selected_location()
    }
    
    func func_save_city() {
        Model_Last_Searched.shared.func_save_city { (status) in
            
        }
    }
    
    func storeCoordinates(_ coordinates: [CLLocationCoordinate2D]) {
        let locations = coordinates.map { coordinate -> CLLocation in
            return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        let archived = NSKeyedArchiver.archivedData(withRootObject: locations)
        UserDefaults.standard.set(archived, forKey: "coordinates")
        UserDefaults.standard.synchronize()
    }
    
    // Return an array of CLLocationCoordinate2D
    func loadCoordinates() -> [CLLocationCoordinate2D]? {
        guard let archived = UserDefaults.standard.object(forKey: "coordinates") as? Data,
            let locations = NSKeyedUnarchiver.unarchiveObject(with: archived) as? [CLLocation] else {
                return nil
        }
        
        let coordinates = locations.map { location -> CLLocationCoordinate2D in
            return location.coordinate
        }
        return coordinates
    }

}



