
//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

var is_myshop_login = false

class Terms_of_Advertising_ViewController: UIViewController {
    
    @IBOutlet weak var btn_continue:UIButton!
    @IBOutlet weak var txt_terms_condition:UITextView!
    
    var str_terms_conditions = ""
    
//    let str_terms_conditions = "١- إرفاق صورة حقيقية للمنتج وليست وهمية. \n\n ٢- تحديد السعر إجباري ويمنع التقسيط . \n\n ٣-الجدية والمصداقية . \n\n ٤- تطبيق Home mall لا يبيع ولا يشتري منتجات ، ولا يتحمل البيع والشراء من الطرفين . \n\n ٥- تطبيق Home mall يمنع بيع وشراء المنتجات الممنوعة ( المقلدة و الأدوية والمنتحات \n\n الطبية ، الأسلحة ، المنتجات الجنسية ، الأسهم ، أجهزة الليزر والتجسس"
    
    
    let str_1 = "١- إرفاق صورة حقيقية للمنتج وليست وهمية."
    let str_2 = "٢- تحديد السعر إجباري ويمنع التقسيط ."
    let str_3 = "٣-الجدية والمصداقية ."
    let str_4 = "٤- تطبيق Home mall لا يبيع ولا يشتري منتجات ، ولا يتحمل البيع والشراء من الطرفين."
    let str_5 = "٥- تطبيق Home mall يمنع بيع وشراء المنتجات الممنوعة ( المقلدة و الأدوية والمنتحات الطبية ، الأسلحة ، المنتجات الجنسية ، الأسهم ، أجهزة الليزر والتجسس )"
    let str_6 = "٦- أقسم بالله أن أدفع عمولة التطبيق بعد أي عملية بيع  التطبيق واسطة لها ."
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        str_terms_conditions = "\(str_1) \n\(str_2) \n\(str_3) \n\(str_4) \n\(str_5) \n\(str_6) \n"
        
        btn_continue.layer.cornerRadius = 10
        btn_continue.layer.shadowOpacity = 3.0
        btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_continue.layer.shadowRadius = 3.0
        btn_continue.layer.shadowColor = color_app.cgColor
        
        Model_My_Ads.shared.is_edit = false
        
        
        txt_terms_condition.text = str_terms_conditions
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:Any) {
        if is_myshop_login {
            is_myshop_login = false
            
            let tabbar = storyboard?.instantiateViewController(withIdentifier: "TabBar_Controller") as! TabBar_Controller
            tabbar.modalPresentationStyle = .fullScreen
            present(tabbar, animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }

    }

    
}



extension Terms_of_Advertising_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
        return cell
    }
}

