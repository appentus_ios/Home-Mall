//  Verify_Code_ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import KWVerificationCodeView
import SVProgressHUD



var is_myshop = true
var is_chat_login = false



class Verify_Code_ViewController: UIViewController ,KWVerificationCodeViewDelegate {
    @IBOutlet weak var codeView: KWVerificationCodeView!
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var btn_resend:UIButton!
    @IBOutlet weak var btn_ok:UIButton!
    
    @IBOutlet weak var view_fone_number:UIView!
    
    var str_code_view = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_fone_number.layer.borderWidth = 1
        view_fone_number.layer.borderColor = color_app .cgColor
        view_fone_number.layer.cornerRadius = 16
        view_fone_number.clipsToBounds = true
        
        btn_resend.layer.cornerRadius = 10
        btn_resend.layer.shadowOpacity = 1.0
        btn_resend.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        btn_resend.layer.shadowRadius = 3.0
        btn_resend.layer.shadowColor = color_app.cgColor
        
        btn_ok.layer.cornerRadius = 10
        btn_ok.layer.shadowOpacity = 1.0
        btn_ok.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        btn_ok.layer.shadowRadius = 3.0
        btn_ok.layer.shadowColor = color_app.cgColor
        
        numberLabel.text = Model_Confirm_Number.shared.str_country_code+" "+Model_Confirm_Number.shared.str_mobile_numbe
//        numberLabel.text = "+966 "+Model_Confirm_Number.shared.str_mobile_numbe
        codeView.delegate = self
        codeView.becomeFirstResponder()
        
        is_logged_in = true
        let title = "Your OTP is".localized
        
//        self.func_ShowHud_Success(with:"\(title):-" + Model_Verify_Code.shared.str_your_OTP)
//        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
//            self.func_HideHud()
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didChangeVerificationCode() {
        str_code_view = "\(codeView.getVerificationCode())"
    }
    
    @IBAction func btn_resend(_ sender:UIButton) {
        func_send_otp()
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func func_user_register() {
//        if txt_phone_number.text!.isEmpty {
//            func_ShowHud_Error(with: "Enter mobile number")
//            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
//                self.func_HideHud()
//            }
//            return
//        }
        
        let view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        Model_Confirm_Number.shared.func_user_register { (status) in
            DispatchQueue.main.async {
                view_loading.isHidden = true
                if status == "success" {
                    let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: Model_Confirm_Number.shared.dict_login_data)
                    UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                    
                    self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "after_login"), object: nil)
                    self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion:nil)
                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "chat_hide"), object: nil)
                } else {
                    self.func_ShowHud_Error(with: Model_Confirm_Number.shared.str_msg)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        self.func_HideHud()
                    }
                }
            }
        }
        
    }
    
    func func_send_otp() {
        let view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        Model_Verify_Code.shared.func_send_otp { (status) in
            DispatchQueue.main.async {
                view_loading.isHidden = true
                if status == "success" {
//                    let localized = "Your OTP is".localized
//                    self.func_ShowHud_Success(with:"\(localized):-" + Model_Verify_Code.shared.str_your_OTP)
//                    DispatchQueue.main.asyncAfter(deadline: .now()+2) {
//                        self.func_HideHud()
//                    }
                } else {
                    self.func_ShowHud_Error(with: Model_Confirm_Number.shared.str_msg)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        self.func_HideHud()
                    }
                }
            }
        }
    }
    
    @IBAction func btn_ok(_ sender:UIButton) {
        self.view.endEditing(true)
        if !func_validation() {
            return
        }
        
        func_user_register()
    }
    
    func func_validation() -> Bool {
        if str_code_view.isEmpty {
            let localized = "Your OTP is".localized
            self.func_ShowHud_Error(with:localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        }  else if str_code_view != Model_Verify_Code.shared.str_your_OTP {
            let localized = "OTP not matched".localized
            self.func_ShowHud_Error(with:localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else {
            return true
        }
    }
    
}
