//
//  ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Model_Verify_Code {
    static let shared = Model_Verify_Code()
    
    var str_country_code = ""
    var str_mobile_numbe = ""
    var str_your_OTP = ""
    var str_msg = ""
    
    func func_send_otp(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"send_otp"
        print(str_FullURL)
        
//        if str_selected_lang == "ar" {
//            str_country_code = "+966"
//        } else {
//            str_country_code = "+91"
//        }
        
        let params = [
            "user_country_code":"\(str_country_code)",
            "user_mobile":str_mobile_numbe
        ]
        
        print(params)
        
        API_Home_Mall .postAPI(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_msg = dict_JSON["message"] as! String
                self.str_your_OTP = "\(dict_JSON["result"]!)"
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_msg = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
}





