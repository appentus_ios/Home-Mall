//  ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 09/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import ASStarRatingView


class My_Ads_ViewController: UIViewController {
    @IBOutlet weak var navbar:UINavigationItem!
    @IBOutlet weak var tbl_vc:UITableView!
    
    var arr_is_selected_cell = [Bool]()
    
    var view_loader = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            
            print(dict_LoginData)
            
            Model_My_Ads.shared.user_mobile = "\(dict_LoginData["user_mobile"] ?? "")"
        }
        
        Model_My_Ads.shared.location_lat = ""
        Model_My_Ads.shared.location_lang = ""
        Model_My_Ads.shared.category_code = ""
        Model_My_Ads.shared.subcate_code = ""
        
        self.arr_is_selected_cell.removeAll()
        Model_My_Ads.shared.arr_My_Ads.removeAll()
        
        get_shop_ad_list()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.arr_is_selected_cell.removeAll()
//        Model_My_Ads.shared.arr_My_Ads.removeAll()
//
//        get_shop_ad_list()
    }
    
    @objc func get_shop_ad_list() {
        view_loader.isHidden = false
        
        Model_My_Ads.shared.get_shop_ad_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_is_selected_cell.removeAll()
                
                if status == "success" {
                    for _ in Model_My_Ads.shared.arr_My_Ads {
                        self.arr_is_selected_cell.append(false)
                    }
                }
                
                if self.arr_is_selected_cell.count > 0 {
                    self.tbl_vc.isHidden = false
                } else {
                    self.tbl_vc.isHidden = true
                }
                
                self.tbl_vc.reloadData()
            }
        }
    }

    @objc func func_delete_shop() {
        view_loader.isHidden = false
        
        Model_My_Shop_Manage.shared.func_delete_shop { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_My_Shop_Manage.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.get_shop_ad_list()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_My_Shop_Manage.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
}




extension My_Ads_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_My_Ads.shared.arr_My_Ads .count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! List_TableViewCell
        
        if Model_My_Ads.shared.arr_My_Ads .count > 0 {
            let model = Model_My_Ads.shared.arr_My_Ads[indexPath.row]
            
            cell.image_shadow.sd_setShowActivityIndicatorView(true)
            cell.image_shadow.sd_setIndicatorStyle(.gray)
            cell.image_shadow.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
            
            cell.lbl_name.text = model.shop_name
            cell.lbl_owner_name.text = model.user_name
            cell.lbl_location_address.text = model.shop_location
            cell.lbl_join_date.text = model.shop_add_date.date_localized
            
//            var rect: CGRect = cell.view_name.frame //get frame of label
//            //        model.shop_name ()
//            rect.size = (model.shop_name.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: cell.lbl_name.font.fontName ,size: cell.lbl_name.font.pointSize+3)!]))
//
//            if rect.width > cell.view_container_1.frame.width-150 {
//                cell.width_name.constant = cell.view_container_1.frame.width-150
//            } else {
//                cell.width_name.constant = rect.width+5
//            }
            
            if model .avg_rating.isEmpty {
                cell.star_view.rating = 0
            } else {
                cell.star_view.rating = Float(model.avg_rating)!
            }
            
            if arr_is_selected_cell[indexPath.row] {
                cell.lbl_name.textColor = UIColor .white
                cell.lbl_location_address.textColor = color_light_gray
                
                cell.img_next_arrow.image = cell.img_next_arrow.image?.imageWithColor(color1: UIColor .white)
                
                cell.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                cell.view_container.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                cell.view_container_1.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
                cell.view_name.layer.cornerRadius = cell.view_name.frame.size.height/2
                cell.view_name.clipsToBounds = true
                
                cell.view_next_arrow.backgroundColor = UIColor (red: 89.0/255.0, green: 227.0/255.0, blue: 186.0/255.0, alpha: 1.0)
                
                cell.view_container.layer.shadowOpacity = 0.0
                cell.view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                cell.view_container.layer.shadowRadius = 0.0
                cell.view_container.layer.shadowColor = UIColor .clear.cgColor
                
                cell.view_name.backgroundColor = UIColor .darkGray
                func_set(view_container: cell.view_container,color: UIColor .gray)
            } else {
                cell.lbl_name.textColor = color_light_gray
                cell.lbl_location_address.textColor = UIColor (red: 255.0/255.0, green: 197.0/255.0, blue: 58.0/255.0, alpha: 1.0)
                
                cell.img_next_arrow.image = cell.img_next_arrow.image?.imageWithColor(color1: color_light_gray)
                
                cell.backgroundColor = UIColor .clear
                cell.view_container_1.backgroundColor = UIColor .white
                cell.view_name.layer.cornerRadius = cell.view_name.frame.size.height/2
                cell.view_name.clipsToBounds = true
                
                cell.view_container.backgroundColor = UIColor .white
                cell.view_next_arrow.backgroundColor = UIColor (red: 241.0/255.0, green: 242.0/255.0, blue: 243.0/255.0, alpha: 1.0)
                
                func_set(view_container: cell.view_container,color: UIColor.gray)
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: false)
        
        for i in 0..<Model_My_Ads.shared.arr_My_Ads.count {
            if i == indexPath.row {
                arr_is_selected_cell[i] = true
            } else {
                arr_is_selected_cell[i] = false
            }
        }
        
        Model_My_Ads.shared.view_count = Model_My_Ads.shared.arr_My_Ads[indexPath.row].view_count
        
        Model_Details.shared.index_selected = indexPath.row
        Model_Details.shared.arr_details = Model_My_Ads.shared.arr_My_Ads
        
        func_present_Fav_VC()
        
        tbl_vc.reloadData()
        
//        func_option()
    }
    
    func func_present_Fav_VC() {
        let fav_VC = storyboard?.instantiateViewController(withIdentifier: "Details_ViewController") as! Details_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        present(fav_VC, animated: true, completion: nil)
    }

    
    func func_set_view_name(view_container:AnyObject ,color:UIColor,cornerRadius:CGFloat) {
        if #available(iOS 13.0, *) {
        view_container.layer .cornerRadius = cornerRadius
        view_container.layer.shadowOpacity = 3.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 3.0
            view_container.layer.shadowColor = color.cgColor
            
        }
    }
    
    func func_set(view_container:AnyObject ,color:UIColor) {
        if #available(iOS 13.0, *) {
        view_container.layer .cornerRadius = 6
        view_container.layer.shadowOpacity = 5.0
        view_container.layer.shadowOffset = CGSize(width: 8.0, height: 8.0)
        view_container.layer.shadowRadius = 5.0
        view_container.layer.shadowColor = color.cgColor
        }
    }
    
}


extension My_Ads_ViewController {
    func func_option() {
        let alert = UIAlertController (title: "My Ads!", message: "Select an Option!", preferredStyle: .actionSheet)
        let yes = UIAlertAction(title: "View Ad", style: .default) { (yes) in
            self.func_present_Fav_VC()
        }
        
        let no = UIAlertAction(title: "Edit Ad", style: .default) { (yes) in
            Model_My_Ads.shared.is_edit = true
            let fav_VC = self.storyboard?.instantiateViewController(withIdentifier: "Category_list_ViewController") as! Category_list_ViewController
            fav_VC.modalPresentationStyle = .fullScreen
            self.present(fav_VC, animated: true, completion: nil)
        }
        
        let delete = UIAlertAction(title: "Delete Ad", style: .default) { (yes) in
            Model_My_Ads.shared.shop_id = Model_My_Ads.shared.arr_My_Ads[Model_Details.shared.index_selected].shop_id
            self.func_delete_surance()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.addAction(delete)
        alert.addAction(cancel)
        
        alert.view.tintColor = color_app
        present(alert, animated: true, completion: nil)
    }
    
    func func_delete_surance() {
        let alert = UIAlertController (title: "", message: "Are you sure?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default) { (yes) in
            self.func_delete_shop()
        }
        
        let no = UIAlertAction(title: "No", style: .default) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.view.tintColor = color_app
        present(alert, animated: true, completion: nil)
    }
    
    
}




