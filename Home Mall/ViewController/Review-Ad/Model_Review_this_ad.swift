
import Foundation
import UIKit
import Alamofire

class Model_Review_this_ad {
    static let shared = Model_Review_this_ad()
    
    var shop_id = ""
    
//    var shop_lat =  ""
//    var shop_lang =  ""
//    var shop_location =  ""
//    var category_code =  ""
//    var subcate_code =  ""
//    //    var subcate_name =  ""
//    var shop_near_to =  ""
//    var shop_name =  ""
//    var shop_address =  ""
//    var shop_about =  ""
//    var city_id = ""
//    var image_tag = ""
//
//    var arr_img_works = [UIImage]()
    
    var shopfile = UIImage (named: "image-add-button.png")
    
    var str_message = ""
    
    func func_update_shop(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"update_shop"
        
        let parameters = [
            "user_id":Model_Splash.shared.user_id,
            "shop_id":shop_id,
            "shop_lat":Model_Ad_Shop.shared.shop_lat,
            "shop_lang":Model_Ad_Shop.shared.shop_lang,
            "shop_location":Model_Ad_Shop.shared.shop_location,
            "category_code":Model_Category_List.shared.category_code,
            "subcate_code":Model_Ad_Shop.shared.subcate_code,
            "shop_name":Model_Ad_Shop.shared.shop_name,
            "shop_near_to":Model_Ad_Shop.shared.shop_near_to,
            "shop_about":Model_Ad_Shop.shared.shop_about,
            "shop_address":Model_Ad_Shop.shared.shop_address,
            "city_id":Model_Ad_Shop.shared.city_id,
            "image_tag":Model_Ad_Shop.shared.image_tag
        ]
        
        print(parameters)
        
        let url = URL (string: str_FullURL)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            var arrayImgData = [Data]()
            
            var imageData = Data()
            if Model_Ad_Shop.shared.arr_img_works.count > 0 {
                for i in 0 ..< Model_Ad_Shop.shared.arr_img_works.count {
                    imageData = UIImageJPEGRepresentation(Model_Ad_Shop.shared.arr_img_works[i], 0.2)!
                    arrayImgData.append(imageData)
                }
                
                for i in 0..<arrayImgData.count {
                    multipartFormData.append(arrayImgData[i], withName: "shopfile[\(i)]", fileName: "image\(i).jpg", mimeType: "image/jpg")
                }
            }
            
//            if let data = UIImageJPEGRepresentation(self.shopfile!, 0.2) {
//                multipartFormData.append(data, withName:"shopfile", fileName: "image.jpg", mimeType: "image/jpg")
//            }
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        let json =  try JSONSerialization .jsonObject(with:response.data!
                            , options: .allowFragments)
                        
                        let dict_JSON =  cleanJsonToObject(data: json as AnyObject) as! [String:Any]
                        print(dict_JSON)
                        
                        if "\(dict_JSON["status"]!)" == "success" {
                            self.str_message = "shop update".localized
                        } else {
                            self.str_message = dict_JSON["message"] as! String
                        }
                        
                        completionHandler(dict_JSON["status"] as! String)
                    }
                    catch let error as NSError {
                        print("error is:-",error)
                        completionHandler("false")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    var is_user_active = false
    var shop_count = ""
    
    func func_is_user_active(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"is_user_active"
        //        print(str_FullURL)
        
        let params = [
            "user_id":Model_Splash.shared.user_id
        ]
        
        //        print(params)
        API_Home_Mall .postAPI_check_login_data(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.is_user_active = true
                self.shop_count = "\(dict_JSON["shop_count"]!)"
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if "\(dict_JSON["message"]!)" != "Error In API" {
                    self.str_message = "This user is delete from Admin"
                    self.is_user_active = false
                    UserDefaults.standard.removeObject(forKey: "login_Data")
                }
                
                if let _ = dict_JSON["status"] as? String {
                    completionHandler(dict_JSON["status"] as! String)
                } else {
                    completionHandler("failed")
                }
                
            }
            
        }
    }
    
    
}
