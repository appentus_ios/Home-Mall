
//  ViewController.swift
//  Home Mall

//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit
import GoogleMaps


class Review_this_Ad_ViewController: UIViewController {
    @IBOutlet weak var view_message:UIView!
    @IBOutlet weak var btn_continue:UIButton!
    
    @IBOutlet weak var map_view:GMSMapView!
    @IBOutlet weak var lbl_Ad_name:UILabel!
    @IBOutlet weak var img_shopImage:UIImageView!
    @IBOutlet weak var lbl_category:UILabel!
    @IBOutlet weak var lbl_sub_category:UILabel!
    
    @IBOutlet weak var lbl_views:UILabel!
    @IBOutlet weak var lbl_last_date:UILabel!
    @IBOutlet weak var lbl_about_ad:UILabel!
    
    @IBOutlet weak var img_user:UIImageView!
    
    @IBOutlet weak var lbl_user_name:UILabel!
    @IBOutlet weak var lbl_user_mobile:UILabel!
    @IBOutlet weak var coll_shop_images:UICollectionView!
    @IBOutlet weak var view_shop_images:UIView!
    
    @IBOutlet weak var height_About:NSLayoutConstraint!
    @IBOutlet weak var height_containerView:NSLayoutConstraint!
    
    var view_loader = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_shadow(view_shadow:view_shop_images)
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        view_message.layer.cornerRadius = 4
        view_message.clipsToBounds = true
        
        btn_continue.layer.cornerRadius = 10
        btn_continue.layer.shadowOpacity = 3.0
        btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_continue.layer.shadowRadius = 3.0
        btn_continue.layer.shadowColor = color_app.cgColor
        
        // Do any additional setup after loading the view.
        func_set_data()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }
    
    @objc func func_shop_Count() {
        if Model_My_Ads.shared.is_edit {
                func_ShowHud_Error(with:Model_Tabbar.shared.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                    self.func_HideHud()
                   
                    let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                    tabbar.modalPresentationStyle = .fullScreen
                    self.present(tabbar, animated: true, completion: nil)
                }
        }
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }
    
    func func_shadow(view_shadow:UIView) {
        view_shadow.layer.shadowOpacity = 5.0
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowRadius = 5.0
        view_shadow.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    
    
    func func_set_data() {
        lbl_category.text = Model_Category_List.shared.category_name
        lbl_sub_category.text = Model_Ad_Shop.shared.subcate_name
        lbl_about_ad.text = Model_Ad_Shop.shared.shop_about
        
        height_About.constant = func_size(width: Int(self.view.bounds.width-50), messageText:Model_Ad_Shop.shared.shop_about).height
        height_containerView.constant = height_containerView.constant + height_About.constant
        
        lbl_Ad_name.text = Model_Ad_Shop.shared.shop_name
        
        
        
        if Model_My_Ads.shared.is_edit {
            if Model_My_Ads.shared.view_count.isEmpty {
                lbl_views.text = "0"
            } else {
                lbl_views.text = Model_My_Ads.shared.view_count
            }
        }
        
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            
            print(dict_LoginData)
            
            lbl_user_name.text = "\(dict_LoginData["user_name"] ?? "")"
            lbl_last_date.text = "\(dict_LoginData["user_join_date"] ?? "")"
            lbl_user_mobile.text = "\(dict_LoginData["user_mobile"] ?? "")"
            let user_profile = "\(dict_LoginData["user_profile"] ?? "")"
            
            img_user.sd_setShowActivityIndicatorView(true)
            img_user.sd_setIndicatorStyle(.gray)
            img_user.sd_setImage(with:URL (string: user_profile), placeholderImage:(UIImage(named:"image-add-button.png")))
        }
        
        let shop_lat = Double(Model_Ad_Shop.shared.shop_lat)!
        let shop_lang = Double(Model_Ad_Shop.shared.shop_lang)!
        
        map_view.clear()
        
        let position = CLLocationCoordinate2D(latitude:shop_lat, longitude:shop_lang)
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
        
        map_view.camera = camera
        map_view?.animate(to: camera)
        
        let marker_position = GMSMarker(position: position)
        
        marker_position.map =  self.map_view
        let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
        let img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
        img_marker.frame = view_hotel.frame
        view_hotel.addSubview(img_marker)
        marker_position.iconView = view_hotel
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Continue(_ sender:UIButton) {
        let alert = UIAlertController (title: "", message: "Do you want to publish your store?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "yes".localized, style: .default) { (yes) in
            self.view_loader.isHidden = false
            
            if Model_My_Ads.shared.is_edit {
                let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
                Model_Review_this_ad.shared.shop_id = model.shop_id
            }
            
            self.func_wrap_images_and_title()
        }
        
        let no = UIAlertAction(title: "no".localized, style: .default) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.view.tintColor = UIColor .black
        present(alert, animated: true, completion: nil)
    }
        
    func func_wrap_images_and_title() {
        Model_Ad_Shop.shared.arr_img_works.removeAll()
        var ad_names = ""
        for dict in arr_ad_name_images {
            if let image = dict["shop_image"] as? UIImage {
                Model_Ad_Shop.shared.arr_img_works.append(image)
                
                let ad_name = "\(dict["shop_image_tag"]!)"
                if ad_names.isEmpty {
                    if ad_name.isEmpty {
                        ad_names = "0"
                    } else {
                        ad_names = ad_name
                    }
                } else {
                    if ad_name.isEmpty {
                        ad_names = ad_names+","+"0"
                    } else {
                        ad_names = ad_names+","+ad_name
                    }
                }
            }
        }
        
        print("ad_names is:-",ad_names)
        
        Model_Ad_Shop.shared.image_tag = ad_names
        Model_Ad_Shop.shared.category_code = Model_Category_List.shared.category_id
        Model_Ad_Shop.shared.subcate_code = Model_Ad_Shop.shared.subcate_code
        Model_Ad_Shop.shared.shop_about = Model_Ad_Shop.shared.shop_about
        
//        if Model_My_Ads.shared.is_edit {
//            self.func_update_shop()
//        } else {
//            func_Add_shop()
//        }
        
        func_is_user_active()
    }
        
    func func_Add_shop() {
        view_loader.isHidden = false
        Model_Ad_Shop.shared.func_Add_shop { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Ad_Shop.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                        self.func_dimiss_after_add()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Ad_Shop.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    
    
    func func_is_user_active() {
        view_loader.isHidden = false
        Model_Review_this_ad.shared.func_is_user_active { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if Model_Review_this_ad.shared.is_user_active {
                    if Model_My_Ads.shared.is_edit {
                        if Model_Review_this_ad.shared.shop_count == "1" {
                            self.func_update_shop()
                        } else {
                            self.func_ShowHud_Error(with:Model_Review_this_ad.shared.str_message)
                            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                                self.func_HideHud()
                                let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                               tabbar.modalPresentationStyle = .fullScreen
                                self.present(tabbar, animated: true, completion: nil)
                            }
                        }
                    } else {
                        self.func_Add_shop()
                    }
                } else {
                    self.func_ShowHud_Error(with:Model_Review_this_ad.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                        self.func_HideHud()
                        
                        let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                        tabbar.modalPresentationStyle = .fullScreen
                        self.present(tabbar, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func func_dimiss_after_add() {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

        })
    }
    
    func func_update_shop() {
        view_loader.isHidden = false
        Model_Review_this_ad.shared.func_update_shop { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Review_this_ad.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                        self.func_dimiss_after_add()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Review_this_ad.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
        
    }
    
    func func_dimiss_after_update() {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

        })
    }

    
    func func_call() {
        if let phoneCallURL = URL(string: "tel://\(lbl_user_mobile.text!)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    application.openURL(phoneCallURL)
                }
            }
        }
    }
    
}



//  MARK:- UICollectionView methods
extension Review_this_Ad_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_ad_name_images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Review_this_ad_CollectionViewCell
        
        coll_Car.lbl_name_ad.text = arr_ad_name_images[indexPath.row]["shop_image_tag"] as? String
        coll_Car.lbl_Ad_name_black_patti.text = coll_Car.lbl_name_ad.text
        
        if coll_Car.lbl_Ad_name_black_patti.text == "" {
            coll_Car.lbl_Ad_name_black_patti.isHidden = true
        } else {
            coll_Car.lbl_Ad_name_black_patti.isHidden = false
        }
        
        if let img_found = arr_ad_name_images[indexPath.row]["shop_image"] as? UIImage {
            coll_Car.img_ad.image = img_found
        } else {
            coll_Car.img_ad.sd_setShowActivityIndicatorView(true)
            coll_Car.img_ad.sd_setIndicatorStyle(.gray)
            coll_Car.img_ad.sd_setImage(with:URL (string: "\(arr_ad_name_images[indexPath.row]["shop_image"]!)"), placeholderImage:(UIImage(named:"image-add-button.png")))
        }
        
        return coll_Car
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}










