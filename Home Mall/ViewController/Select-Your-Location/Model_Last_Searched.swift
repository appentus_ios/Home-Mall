//
//  ModelViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 07/02/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import AVKit



import Foundation


class Model_Last_Searched {
    static let shared = Model_Last_Searched()
    
    var city_id = ""
    var city_name = ""
    var city_nicname = ""
    var city_lat = ""
    var city_long = ""
    
    var str_msg = ""
    
    var keyword = ""
    
    var arr_get_city = [Model_Last_Searched]()
    var arr_get_all_save_city = [Model_Last_Searched]()
    
    func func_get_city(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_city"
        let param = "keyword=\(keyword)"
        print(param)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: param) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_city.removeAll()
            Model_Search_VC.shared.arr_list_View.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_city.append(self.func_get_city(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_get_city(dict:[String:Any]) -> Model_Last_Searched {
        let model = Model_Last_Searched()
        
        model.city_id = "\(dict["city_id"] ?? "")"
        model.city_name = "\(dict["city_name"] ?? "")"
        model.city_nicname = "\(dict["city_nicname"] ?? "")"
        model.city_lat = "\(dict["city_lat"] ?? "")"
        model.city_long = "\(dict["city_long"] ?? "")"
        
        return model
    }

    func func_get_all_save_city(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_all_save_city"
        let param = "user_id=\(Model_Splash.shared.user_id)"
        print(param)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: param) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_all_save_city.removeAll()
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_all_save_city.append(self.func_arr_get_all_save_city(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    private func func_arr_get_all_save_city(dict:[String:Any]) -> Model_Last_Searched {
        let model = Model_Last_Searched()
        
        model.city_id = "\(dict["city_id"] ?? "")"
        model.city_name = "\(dict["city_name"] ?? "")"
        model.city_lat = "\(dict["lat"] ?? "")"
        model.city_long = "\(dict["lang"] ?? "")"
        
        return model
    }
    
    func func_save_city(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"save_city"
        let param = "user_id=\(Model_Splash.shared.user_id)&city_id=\(city_id)&city_name=\(city_name)&lat=\(city_lat)&lang=\(city_long)"
        print(param)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: param) {
            (dict_JSON) in
            print(dict_JSON)
            
//            self.arr_get_all_save_city.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    func func_delete_save_city_post(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"delete_save_city"
        let param = "user_id=\(Model_Splash.shared.user_id)"
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: param) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_all_save_city.removeAll()
            if dict_JSON["status"] as? String == "success" {
                self.str_msg = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
}






