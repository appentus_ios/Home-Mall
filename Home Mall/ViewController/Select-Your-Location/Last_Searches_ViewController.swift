//  Last_Searches_ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 07/02/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacesAPI



class Last_Searches_ViewController: UIViewController {
    @IBOutlet weak var tbl_last_searched:UITableView!
    @IBOutlet weak var tbl_searched:UITableView!
    
    @IBOutlet weak var view_last_search:UIView!
    @IBOutlet weak var txt_selected_city:CustomTextField!
    @IBOutlet weak var btn_ok:UIButton!
    
    var view_loading = UIView()
    var is_back_press = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Model_Search_VC.shared.arr_get_city.removeAll()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        
        view_loading.isHidden = true
        tbl_searched.isHidden = true
        
        btn_ok.layer.cornerRadius = 10
        btn_ok.layer.shadowOpacity = 3.0
        btn_ok.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_ok.layer.shadowRadius = 3.0
        btn_ok.layer.shadowColor = color_app.cgColor
        
        btn_ok.isHidden = true
        
        txt_selected_city.backSpaceDelegate = self
        
        func_get_all_save_city()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func func_get_all_save_city() {
        view_loading.isHidden = false
        Model_Last_Searched.shared.func_get_all_save_city { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    if Model_Last_Searched.shared.arr_get_all_save_city.count > 0 {
                        self.view_last_search.isHidden = false
                    } else {
                        self.view_last_search.isHidden = true
                    }
                } else {
                    self.view_last_search.isHidden = true
                }
                self.tbl_last_searched.reloadData()
            }
        }
    }
    
    func func_get_city() {
//        view_loading.isHidden = false
        
        Model_Last_Searched.shared.func_get_city { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    if Model_Last_Searched.shared.arr_get_city.count > 0 {
                        self.tbl_searched.isHidden = false
                    } else {
                        self.tbl_searched.isHidden = true
                    }
                }
                
                if self.is_back_press {
                    self.tbl_searched.isHidden = true
                }
                
                self.tbl_searched.reloadData()
            }
        }
    }
    
    func func_save_city() {
        view_loading.isHidden = false
        Model_Last_Searched.shared.func_save_city { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
            }
        }
    }
    
    
    
    func func_delete_save_city_post() {
        view_loading.isHidden = false
        Model_Last_Searched.shared.func_delete_save_city_post { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                self.tbl_last_searched.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: "Deleted successfully".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.view_last_search.isHidden = true
                    })
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
               
            }
        }
        
    }
    
    @IBAction func btn_ok(_ sender:UIButton) {
        func_after_select_location()
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_delete_city(_ sender:Any) {
        let alert = UIAlertController (title: "", message: "Are you sure?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "yes".localized, style: .default) { (yes) in
            self.func_delete_save_city_post()
        }
        
        let no = UIAlertAction(title: "no".localized, style: .default) { (yes) in
            
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        
        alert.view.tintColor = UIColor .black
        present(alert, animated: true, completion: nil)
    }
    
}

extension Last_Searches_ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_searched {
            return Model_Last_Searched.shared.arr_get_city.count
        } else {
            return Model_Last_Searched.shared.arr_get_all_save_city.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbl_searched {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath)
            
            let lbl_city_name = cell.viewWithTag(1) as! UILabel
            
            let model = Model_Last_Searched.shared.arr_get_city[indexPath.row]
            lbl_city_name.text = "\(model .city_name) ( \(model.city_nicname) )"
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            let lbl_city_name = cell.viewWithTag(1) as! UILabel
            
            let model = Model_Last_Searched.shared.arr_get_all_save_city[indexPath.row]
            lbl_city_name.text = model .city_name
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        
        btn_ok.isHidden = false
        
        if tableView == tbl_searched {
            tbl_searched.isHidden = true
            
            let model = Model_Last_Searched.shared.arr_get_city[indexPath.row]
            
            Model_Last_Searched.shared.city_id = model .city_id
            Model_Last_Searched.shared.city_lat = model .city_lat
            Model_Last_Searched.shared.city_long = model .city_long
            Model_Last_Searched.shared.city_name = "\(model.city_name) ( \(model.city_nicname) )"
            
            Model_Ad_Shop.shared.shop_lat = model .city_lat
            Model_Ad_Shop.shared.shop_lang = model .city_long
            
            txt_selected_city.text = "\(model.city_name) ( \(model.city_nicname) )"
            
            func_save_city()
        } else {
            let model = Model_Last_Searched.shared.arr_get_all_save_city[indexPath.row]
            Model_Last_Searched.shared.city_id = model .city_id
            Model_Last_Searched.shared.city_lat = model .city_lat
            Model_Last_Searched.shared.city_long = model .city_long
            
            Model_Ad_Shop.shared.shop_lat = model .city_lat
            Model_Ad_Shop.shared.shop_lang = model .city_long
            
            txt_selected_city.text = model.city_name
        }
    }
    
    func func_after_select_location() {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location"), object: nil)
        
        if str_city_list == "list" {
//            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_list"), object: nil)
        } else {
//            NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_city"), object: nil)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
}



extension Last_Searches_ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let localized = "Address not found ".localized
        let str_selectYourLocations = place.formattedAddress ?? localized
        
        Model_Last_Searched.shared.city_id = place.placeID
        Model_Last_Searched.shared.city_lat = "\(place.coordinate.latitude)"
        Model_Last_Searched.shared.city_long = "\(place.coordinate.longitude)"
        Model_Ad_Shop.shared.shop_lat = "\(place.coordinate.latitude)"
        Model_Ad_Shop.shared.shop_lang = "\(place.coordinate.longitude)"
        
        Model_Last_Searched.shared.city_name = str_selectYourLocations
        
        txt_selected_city.text = str_selectYourLocations
        dismiss(animated: true, completion: nil)
        
        func_save_city()
        btn_ok.isHidden = false
    }
    
    
    
//    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
//                           didAutocompleteWith place: GMSPlace) {
//
//
//        //        Log.debug("Place name: \(String(describing: place.name))")
//        //        Log.debug("Place address: \(String(describing: place.formattedAddress))")
//        //        Log.debug("Place attributions: \(String(describing: place.attributions))")
//
//        let placeID = place.placeID
//
//        //        guard let placeID = place.placeID else {
//        //            return
//        //        }
//
//        /// Use Google Place API to lookup place information in English
//        /// https://developers.google.com/places/web-service/intro
//        GooglePlacesAPI.GooglePlaces.placeDetails(forPlaceID: placeID, language: "AE") { (response, error) -> Void in
//
//            guard let place = response?.result, response?.status == GooglePlaces.StatusCode.ok else {
//                print("Error = \(String(describing: response?.errorMessage))")
//
//                return
//            }
//
//            let postalCode = place.addressComponents.filter({ (components) -> Bool in
//                return components.types.contains(kGMSPlaceTypePostalCode)
//            }).first?.longName
//
//            print(place)
//            print(place.formattedAddress)
//            print(place.name)
//        }
//    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension Last_Searches_ViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.isEmpty {
            let gmsautocomplete = GMSAutocompleteViewController()
            
//            let filter = GMSAutocompleteFilter()
//            filter.type = .establishment
//            filter.country = "AE"
//
//            gmsautocomplete.autocompleteFilter = filter
            
            gmsautocomplete.delegate = self
            present(gmsautocomplete, animated: true, completion: nil)
            
//            Model_Last_Searched.shared.arr_get_city.removeAll()
//            tbl_searched.isHidden = false
//            tbl_searched.reloadData()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text!.isEmpty {
            Model_Last_Searched.shared.arr_get_city.removeAll()
            tbl_searched.reloadData()
            tbl_searched.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        is_back_press = false
        
        Model_Last_Searched.shared.keyword = textField.text!
        func_get_city()
        
        return true
    }
}



protocol BackSpaceDelegate {
//    func deleteBackWord(textField: CustomTextField)
    func deleteBackWord()
    
    
}

class CustomTextField: UITextField {
    var backSpaceDelegate: BackSpaceDelegate?
    override func deleteBackward() {
        super.deleteBackward()
        // called when textfield is empty. you can customize yourself.
        if text?.isEmpty ?? false {
//            backSpaceDelegate?.deleteBackWord(textField: self)
            backSpaceDelegate?.deleteBackWord()
        }
    }
    
}

extension Last_Searches_ViewController:BackSpaceDelegate {
//    func deleteBackWord(textField: CustomTextField) {
//        print("textfield is empty")
//        /// do your stuff here. That means resign or become first responder your expected textfield.
//    }
    
    func deleteBackWord() {
        tbl_searched.isHidden = true
        is_back_press = true
    }
    
}




