//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacesAPI

//import GoogleMapsCore

class Select_Your_Location_ViewController: UIViewController {
    
    @IBOutlet weak var btn_continue:UIButton!
    @IBOutlet weak var view_satelite:UIView!
    @IBOutlet weak var view_current_location:UIView!
    @IBOutlet weak var view_O_search:UIView!
    
    @IBOutlet weak var mapView:GMSMapView!
    
    @IBOutlet weak var view_continue:UIView!
    
    @IBOutlet weak var lbl_selectYourLocations:UILabel!
    
    var marker_SelectedLocation = GMSMarker()
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    
    var is_search_location = false
    var is_updated_location = false
    
    var is_satelite = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_continue.layer.cornerRadius = 10
        btn_continue.layer.shadowOpacity = 3.0
        btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_continue.layer.shadowRadius = 3.0
        btn_continue.layer.shadowColor = color_app.cgColor
        
        view_continue.layer.shadowOpacity = 3.0
        view_continue.layer.shadowOffset = CGSize(width: -2.0, height: -2.0)
        view_continue.layer.shadowRadius = 3.0
        view_continue.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_satelite.layer.cornerRadius = 4
        view_satelite.layer.shadowOpacity = 1.0
        view_satelite.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_satelite.layer.shadowRadius = 1.0
        view_satelite.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_current_location.layer.cornerRadius = 4
        view_current_location.layer.shadowOpacity = 3.0
        view_current_location.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_current_location.layer.shadowRadius = 3.0
        view_current_location.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_O_search.layer.cornerRadius = 10
        view_O_search.layer.shadowOpacity = 1.0
        view_O_search.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_O_search.layer.shadowRadius = 1.0
        view_O_search.layer.shadowColor = UIColor .lightGray.cgColor
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_selected_location), name: NSNotification.Name (rawValue: "selected_location"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }
    
    @objc func func_shop_Count() {
        if Model_My_Ads.shared.is_edit {
            func_ShowHud_Error(with:Model_Tabbar.shared.message)
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.func_HideHud()
                
                let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                tabbar.modalPresentationStyle = .fullScreen
                self.present(tabbar, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func func_selected_location() {
        let city_lat = Double(Model_Last_Searched.shared.city_lat)
        let city_long = Double(Model_Last_Searched.shared.city_long)
        
        let cordinate = CLLocationCoordinate2D(latitude: city_lat!, longitude: city_long!)
        
        func_camera_position(cordinate: cordinate)
    }
    
    func func_camera_position(cordinate:CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude:cordinate.latitude, longitude:cordinate.longitude, zoom: 15)
        mapView.camera = camera
        mapView?.animate(to: camera)
        
        marker_SelectedLocation = GMSMarker(position: cordinate)
        marker_SelectedLocation.map = mapView
        marker_SelectedLocation.icon = UIImage (named:"target.png")
        
        Model_Ad_Shop.shared.shop_lat =  "\(cordinate.latitude)"
        Model_Ad_Shop.shared.shop_lang =  "\(cordinate.longitude)"
    }
    
    @IBAction func btn_current_location(_ sender:Any) {
        func_camera_position(cordinate: co_OrdinateCurrent)
        func_ShowMarker(cordinate: co_OrdinateCurrent)
    }
    
    @IBAction func btn_satelite(_ sender:Any) {
        if is_satelite {
            is_satelite = false
            mapView.mapType = .normal
        } else {
            is_satelite = true
            mapView.mapType = .satellite
        }
    }
    
    @IBAction func btn_continue(_ sender:Any) {
            let ad_about_VC = storyboard?.instantiateViewController(withIdentifier: "Ad_About_ViewController") as! Ad_About_ViewController
        ad_about_VC.modalPresentationStyle = .fullScreen
            present(ad_about_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
}



extension Select_Your_Location_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        co_OrdinateCurrent = manager.location!.coordinate
        
        if !is_updated_location {
            if Model_My_Ads.shared.is_edit {
                let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
                
                let shop_lat = Double(model.shop_lat)
                let shop_lang = Double(model.shop_lang)
                
                let cordinate = CLLocationCoordinate2DMake(shop_lat!, shop_lang!)
                func_camera_position(cordinate: cordinate)
                func_ShowMarker(cordinate: cordinate)
            } else {
                func_camera_position(cordinate: co_OrdinateCurrent)
                func_ShowMarker(cordinate: co_OrdinateCurrent)
            }
            
            is_updated_location = true
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}


extension Select_Your_Location_ViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        is_search_location = true
        let localized = "Address not found ".localized
        lbl_selectYourLocations.text = place.formattedAddress ?? localized
        func_ShowMarker(cordinate: place.coordinate)
        Model_Ad_Shop.shared.shop_location = lbl_selectYourLocations.text!
        
        dismiss(animated: true, completion: nil)
    }
    
    
    
//    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
//                           didAutocompleteWith place: GMSPlace) {
//        //        Log.debug("Place name: \(String(describing: place.name))")
//        //        Log.debug("Place address: \(String(describing: place.formattedAddress))")
//        //        Log.debug("Place attributions: \(String(describing: place.attributions))")
//
//        let placeID = place.placeID
//
//        //        guard let placeID = place.placeID else {
//        //            return
//        //        }
//
//        /// Use Google Place API to lookup place information in English
//        /// https://developers.google.com/places/web-service/intro
//        GooglePlacesAPI.GooglePlaces.placeDetails(forPlaceID: placeID, language: "AE") { (response, error) -> Void in
//
//            guard let place = response?.result, response?.status == GooglePlaces.StatusCode.ok else {
//                print("Error = \(String(describing: response?.errorMessage))")
//
//                return
//            }
//
//            let postalCode = place.addressComponents.filter({ (components) -> Bool in
//                return components.types.contains(kGMSPlaceTypePostalCode)
//            }).first?.longName
//
//            print(place)
//            print(place.formattedAddress)
//            print(place.name)
//        }
//    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension Select_Your_Location_ViewController:GMSMapViewDelegate {
    func set_GoogleMap() {
        
        let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
        
        mapView.camera = camera
        mapView?.animate(to: camera)
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print(position.target)
        func_ShowMarker(cordinate: position.target)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        func_ShowMarker(cordinate: position.target)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        func_ShowMarker(cordinate: coordinate)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        func_ShowMarker(cordinate: (mapView.myLocation?.coordinate)!)
        return true
    }
    
    func func_ShowMarker(cordinate:CLLocationCoordinate2D) {
        if marker_SelectedLocation != nil {
            mapView.clear()
        }
        
        marker_SelectedLocation = GMSMarker(position: cordinate)
        marker_SelectedLocation.map = mapView
        marker_SelectedLocation.icon = UIImage (named:"target.png")
                
        Model_Ad_Shop.shared.shop_lat =  "\(cordinate.latitude)"
        Model_Ad_Shop.shared.shop_lang =  "\(cordinate.longitude)"
        
        if !is_search_location {
            func_Geocoder(cordinate) { (str_Address) in
                DispatchQueue.main.async {
                    self.lbl_selectYourLocations.text = str_Address
                    Model_Ad_Shop.shared.shop_location =  "\(str_Address)"
                }
            }
        }
        
        is_search_location = false
    }
    
//    let str_Address = "\(address.locality ?? ""), \(address.administrativeArea ?? ""), \(address.country ?? "")"

//    ,locality:String
    func func_Geocoder(_ coordinate: CLLocationCoordinate2D,completion:@escaping (String)->()) {
        let cllocation = CLLocation (latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        //        print("\(Locale.current.regionCode)")
        
        var languageCode = ""
        if "\(Locale.current.regionCode ?? "")".lowercased() == "in" {
            languageCode = "en"
        } else {
            languageCode = "ar"
        }
        
        if #available(iOS 11.0, *) {
            CLGeocoder().reverseGeocodeLocation(cllocation, preferredLocale:Locale.init(identifier: languageCode), completionHandler: {
                (placemarks, error) -> Void in
                
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    let dict_Address = pm.addressDictionary
                        
                    //                    print("\(dict_Address!["Country"]!)")
                    //                    print("\(dict_Address!["State"]!)")
                    //                    print("\(dict_Address!["SubAdministrativeArea"]!)")
                    
                    var string_final = "\(dict_Address!["SubLocality"] ?? "") \(dict_Address!["City"] ?? "")"
                    
//                    var string_final = ""
//                    if dict_Address!["FormattedAddressLines"] != nil{
//                        let str = dict_Address!["FormattedAddressLines"]! as! NSArray
//
//                        for i in 0..<str.count{
//                            string_final += "\(str[i]) "
//                        }
//                    }
//                    string_final = string_final.replacingOccurrences(of:"\(dict_Address!["Country"]!)", with: "")
                
                //                    let str_Street = dict_Address!["Street"] ?? ""
                //                    let str_Name = dict_Address!["Name"] ?? ""
                //                    let str_City = dict_Address!["City"] ?? ""
                //
                //                    let str_state = dict_Address!["State"] ?? ""
                //                    let str_country = dict_Address!["Country"] ?? ""
                    
                    completion(string_final)
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
        } else {
            // Fallback on earlier versions
        }
        
        
        
//        let geocoder = GMSGeocoder()
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            guard let address = response?.firstResult(), let lines = address.lines else {
//                return
//            }
//            let str_Address = lines.joined(separator: "\n")
//            print(str_Address)
//
//            Model_Ad_Shop.shared.city_id = address.locality ?? ""
//            if Model_Ad_Shop.shared.city_id.isEmpty {
//                Model_Ad_Shop.shared.city_id = address.administrativeArea ?? ""
//            }
//            Model_Ad_Shop.shared.shop_address = str_Address
//            if Model_Ad_Shop.shared.city_id.isEmpty {
//                Model_Ad_Shop.shared.city_id = address.country ?? ""
//            }
//            Model_Ad_Shop.shared.shop_location = "\(address.locality ?? ""), \(address.administrativeArea ?? ""), \(address.country ?? "")"
//
//            completion(str_Address)
//        }
    }
    
    
}













