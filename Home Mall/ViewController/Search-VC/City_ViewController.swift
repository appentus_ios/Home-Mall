//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 09/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacesAPI

import SDWebImage
import AlamofireImage

var is_loaded_first_time = false
var is_my_location_enable = false

var co_OrdinateCurrent_selected = CLLocationCoordinate2DMake(0.0, 0.0)

class City_ViewController: UIViewController,GMSMapViewDelegate {
    @IBOutlet weak var mapview:GMSMapView!
    @IBOutlet weak var coll_cate_1:UICollectionView!
    @IBOutlet weak var coll_cate_2:UICollectionView!
    
    @IBOutlet weak var lbl_morethan_ads:UILabel!
    
    @IBOutlet weak var lbl_title_1:UILabel!
    @IBOutlet weak var nav_bar:UINavigationItem!
    @IBOutlet weak var view_cate:UIView!
    
    var arr_list = [CLLocationCoordinate2D]()
    var img_m : UIImage?
    var arr_markers = [GMSMarker]()
    
    var  arr_cate = [Model_Category_List]()
    var  arr_sub_cate = [Model_Category_List]()
    
    var arr_cate_is_selected_cell = [Bool]()
    var arr_subcate_is_selected_cell = [Bool]()
    var selected_marker_index = -1
    
    var selected_lat = 0.0
    var selected_long = 0.0
    
    var arr_selected_marker = [CLLocationCoordinate2D]()
    
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    var my_location_co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    
    var is_tap_window = 0
    var is_padding = false
    var is_updated_location = false
    
    var view_loader = UIView()
    
    var marker_SelectedLocation = GMSMarker()
    
    var str_there_are_more_than = ""
    var str_ads = ""
    
    var is_zoom = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        view_cate.layer.shadowOpacity = 2.5
        view_cate.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_cate.layer.shadowRadius = 1.5
        view_cate.layer.shadowColor = UIColor .gray.cgColor
        
        str_there_are_more_than = "There are more than".localized
        str_ads = "ads".localized
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        NotificationCenter.default.addObserver(self, selector: #selector(func_my_location), name: NSNotification.Name (rawValue:"my_location"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_user_mobile), name: NSNotification.Name (rawValue:"user_mobile_search"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(search_by_cate), name: NSNotification.Name (rawValue:"search_by_cate_city"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selected_location), name: NSNotification.Name (rawValue:"selected_location_city"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_image_ads_city), name: NSNotification.Name (rawValue:"image_ads_city"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_back), name: NSNotification.Name (rawValue:"back"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_selected_location), name: NSNotification.Name (rawValue: "selected_location_google_city"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_open_google), name: NSNotification.Name (rawValue: "open_google_city"), object: nil)
        
        Model_City.shared.category_code = ""
        Model_City.shared.subcate_code = ""
        
        func_get_category()
//        func_get_sub_category()
        
        coll_cate_1.layer.cornerRadius = coll_cate_1.frame.size.height/2
        coll_cate_2.layer.cornerRadius = coll_cate_2.frame.size.height/2
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        mapview.isMyLocationEnabled = true
        mapview.settings.myLocationButton = true
        
        is_loaded_first_time = false
        
        func_is_first_time_launch()
        is_first_time_launch = false
        
//      viewWillApear
        if arr_selected_marker.count > 0 {
            arr_selected_marker.removeAll()
        }
        
        self.mapview.clear()
        
//        nav_bar.title = "Select a location".localized
        self.lbl_title_1 .text = "Select a location".localized
        is_padding = false
        is_loaded_first_time = false
        
        func_get_category()
        func_is_first_time_launch()
        
        view_loader.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        is_my_location_enable = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        is_my_location_enable = true
        is_From_city_myloc = "mylocation"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    @objc func func_back()  {
        mapview.clear()
        
        func_get_category()
        func_call_api()
    }
    
    @objc func func_image_ads_city()  {
        is_loaded_first_time = false
        func_call_api()
    }
    
    func func_is_first_time_launch() {
        if is_first_time_launch {
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                NotificationCenter.default.post(name: NSNotification.Name (rawValue: "is_first_time_launch"), object: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    
    @IBAction func btn_search(_ sender:UIButton) {
        str_city_list = "city"
        
//        onSlideMenuButtonPressed(sender)
        
        if is_menu_open {
            
        } else {
            onSlideMenuButtonPressed(sender)
        }

    }
    
//    func func_search_by_cate() {
//        get_shop_ad_list()
//    }
    
    @objc func func_user_mobile() {
        is_zoom = false
        is_padding = false
        is_loaded_first_time = false
        func_get_shop_ad_list_mobile()
    }
    
    @objc func search_by_cate() {
        is_loaded_first_time = false
        
        arr_sub_cate.removeAll()
        coll_cate_2.reloadData()
        
        func_get_shop_ad_list_category()
    }
    
    @objc func selected_location() {
        co_OrdinateCurrent = CLLocationCoordinate2D (latitude: Double(Model_Search_VC.shared.location_lat)!, longitude: Double(Model_Search_VC.shared.location_lang)!)
        
        let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
        
        mapview.camera = camera
        mapview.animate(to: camera)
        
        cord_city_my_location = co_OrdinateCurrent
        func_Geocoder(co_OrdinateCurrent) { (address) in
            DispatchQueue.main.async {
//                self.nav_bar.title = address
                self.lbl_title_1 .text = address
                str_city_my_location = address
            }
        }
        
        Model_City.shared.location_lang = Model_Search_VC.shared.location_lang
        Model_City.shared.location_lat = Model_Search_VC.shared.location_lat
        
        func_get_shop_ad_list_lat_long()
    }
    
}



//  MARK:- UICollectionView methods
extension City_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == coll_cate_1 {
            let model = arr_cate[indexPath.row]
            let width = func_size(width:100, messageText: model.category_name).width+14
            return CGSize (width: width, height: collectionView.frame.size.height)
            
//            return CGSize (width: 60, height: collectionView.frame.size.height)
        } else {
            let model = arr_sub_cate[indexPath.row]
            let width = func_size(width:100, messageText: model.subcate_name).width+14
            return CGSize (width: width, height: collectionView.frame.size.height)
            
//            return CGSize (width: 80, height: collectionView.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == coll_cate_1 {
            return arr_cate.count
        } else {
            return arr_sub_cate.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == coll_cate_1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell-1", for: indexPath)
            
            let lbl_cate = cell.viewWithTag(1) as? UILabel
            let model = arr_cate[indexPath.row]
            lbl_cate?.text = model.category_name
            
            if arr_cate_is_selected_cell[indexPath.row] {
                lbl_cate?.textColor = UIColor .black
            } else {
                lbl_cate?.textColor = UIColor .white
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell-2", for: indexPath)
            
            let lbl_cate = cell.viewWithTag(1) as? UILabel
            let model = arr_sub_cate[indexPath.row]
            lbl_cate?.text = model.subcate_name
            
            if arr_subcate_is_selected_cell[indexPath.row] {
                lbl_cate?.textColor = UIColor .black
            } else {
                lbl_cate?.textColor = UIColor .white
            }
            
            return cell
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Model_City.shared.raduis_city = "\(mapview.getRadius())"
        
        if collectionView == coll_cate_1 {
            is_padding = false
            is_loaded_first_time = false
            
            for i in 0..<arr_cate_is_selected_cell.count {
                if i == indexPath.row {
                    arr_cate_is_selected_cell[i] = true
                }  else {
                    arr_cate_is_selected_cell[i] = false
                }
            }
            
            
            Model_Search_VC.shared.subcate_code = ""
            Model_My_Location.shared.subcate_code = ""
            Model_City.shared.subcate_code = ""
            
            Model_Search_VC.shared.category_code = arr_cate[indexPath.row].category_code
            Model_City.shared.category_code = arr_cate[indexPath.row].category_code
            Model_Category_List.shared.category_code = arr_cate[indexPath.row].category_code
            
            coll_cate_1 .reloadData()
            
            Model_Search_VC.shared.subcate_code = ""
            Model_City.shared.subcate_code = ""
            
            arr_subcate_is_selected_cell.removeAll()
            
            if Model_Category_List.shared.category_code.isEmpty {
                arr_sub_cate.removeAll()
                arr_subcate_is_selected_cell.removeAll()
                self.coll_cate_2.reloadData()
            } else {
                func_get_sub_category()
            }
        } else {
            for i in 0..<arr_subcate_is_selected_cell.count {
                if i == indexPath.row {
                    arr_subcate_is_selected_cell[i] = true
                }  else {
                    arr_subcate_is_selected_cell[i] = false
                }
            }
            
            Model_Search_VC.shared.subcate_code  = arr_sub_cate[indexPath.row].subcate_code
            Model_My_Location.shared.subcate_code = arr_sub_cate[indexPath.row].subcate_code
            Model_City.shared.subcate_code = Model_Search_VC.shared.subcate_code
            
            coll_cate_2.reloadData()
        }
        func_call_api()
    }
    
}


extension City_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        my_location_co_OrdinateCurrent = manager.location!.coordinate
        view_loader.isHidden = true
        
        if !is_updated_location {
            is_updated_location = true
            
            co_OrdinateCurrent = manager.location!.coordinate
            cord_city_my_location = co_OrdinateCurrent
            Model_City.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
            Model_City.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
            
            let position = CLLocationCoordinate2D(latitude:co_OrdinateCurrent.latitude, longitude:co_OrdinateCurrent.longitude)
            let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
            
            mapview.camera = camera
            mapview.animate(to: camera)
            
            func_get_shop_ad_list_lat_long()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    
}



// MARK:- Custom functions

extension City_ViewController {
//    func func_ShowMarker(cordinate:CLLocationCoordinate2D) {
//        if marker_SelectedLocation != nil {
//            mapview.clear()
//        }
//        
//        marker_SelectedLocation = GMSMarker(position: cordinate)
//        marker_SelectedLocation.map = mapview
//        marker_SelectedLocation.icon = UIImage (named:"target.png")
//        
//        Model_Ad_Shop.shared.shop_lat =  "\(cordinate.latitude)"
//        Model_Ad_Shop.shared.shop_lang =  "\(cordinate.longitude)"
//        
//        co_OrdinateCurrent = cordinate
//        Model_City.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
//        Model_City.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
//        
//        get_shop_ad_list()
//    }
//    
//    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
//        print(position.target)
//        func_ShowMarker(cordinate: position.target)
//    }

    func func_Geocoder(_ coordinate: CLLocationCoordinate2D,completion:@escaping (String)->()) {
        let cllocation = CLLocation (latitude: coordinate.latitude, longitude: coordinate.longitude)
        
//        print("\(Locale.current.regionCode)")
        
        var languageCode = ""
        if "\(Locale.current.regionCode ?? "")".lowercased() == "in" {
            languageCode = "en"
        } else {
            languageCode = "ar"
        }
        
        if #available(iOS 11.0, *) {
            CLGeocoder().reverseGeocodeLocation(cllocation, preferredLocale:Locale.init(identifier: languageCode), completionHandler: {
                (placemarks, error) -> Void in
                
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    let dict_Address = pm.addressDictionary
                    print(dict_Address)

                    //                    print("\(dict_Address!["Country"]!)")
                    //                    print("\(dict_Address!["State"]!)")
                    //                    print("\(dict_Address!["SubAdministrativeArea"]!)")
                    
                    var string_final = "\(dict_Address!["SubLocality"] ?? "") \(dict_Address!["City"] ?? "")"
                    
//                    var string_final = ""
//
//                    if dict_Address!["FormattedAddressLines"] != nil{
//                        let str = dict_Address!["FormattedAddressLines"]! as! NSArray
//
//                        for i in 0..<str.count{
//                            string_final += "\(str[i]) "
//                        }
//                    }
//
//                    string_final = string_final.replacingOccurrences(of:"\(dict_Address!["Country"]!)", with: "")
                    
//                    let str_Street = dict_Address!["Street"] ?? ""
//                    let str_Name = dict_Address!["Name"] ?? ""
//                    let str_City = dict_Address!["City"] ?? ""
//
//                    let str_state = dict_Address!["State"] ?? ""
//                    let str_country = dict_Address!["Country"] ?? ""
                    
                    completion(string_final)
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
        } else {
            // Fallback on earlier versions
        }
        
//        let geocoder = GMSGeocoder()
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            guard let address = response?.firstResult() else {
//                let localized = "Address not found ".localized
//                completion(localized)
//                return
//            }
//
//            var str_Address = ""
//
//            if let _ = address.subLocality {
//                str_Address = "\(address.subLocality ?? "") , \(address.locality ?? "")"
//            } else if let _ = address.locality {
//                str_Address = "\(address.locality ?? "") , \(address.administrativeArea ?? "")"
//            } else if let _ = address.administrativeArea {
//                str_Address = "\(address.administrativeArea ?? "") , \(address.country ?? "")"
//            } else if let _ = address.country {
//                str_Address = "\(address.locality ?? "")"
//            }
//
//            completion(str_Address)
//        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if !func_IsLocationEnable() {
            return false
        }
        
        is_padding = false
//        is_loaded_first_time = false
        if let _ = mapView.myLocation?.coordinate {
            
        } else { return false }
        
        my_location_co_OrdinateCurrent = (mapView.myLocation?.coordinate)!
        co_OrdinateCurrent = (mapView.myLocation?.coordinate)!
        Model_City.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
        Model_City.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
        
        Model_Search_VC.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
        Model_Search_VC.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
        
        selected_location()
        
        func_Geocoder(co_OrdinateCurrent) { (address) in
//            self.nav_bar.title = address
            self.lbl_title_1 .text = address
        }
        
        func_call_api()
        
        return true
    }
    
    @objc func func_my_location() {
        if !func_IsLocationEnable() {
            return
        }
        
        is_padding = false
        is_myLocation_tapped = true
        
        co_OrdinateCurrent = my_location_co_OrdinateCurrent
        cord_city_my_location = co_OrdinateCurrent
        
        Model_City.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
        Model_City.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
        
        Model_Search_VC.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
        Model_Search_VC.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
        
        selected_location()
        
        func_Geocoder(co_OrdinateCurrent) { (address) in
//            self.nav_bar.title = address
            self.lbl_title_1 .text = address
            str_city_my_location = address
        }
        
        func_call_api()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !func_IsLocationEnable() {
            return
        }
        
        Model_City.shared.raduis_city = "\(mapview.getRadius())"
        
        if is_zoom {
            if is_updated_location {
                if is_tap_window == 0 {
                    co_OrdinateCurrent = position.target
                    Model_City.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
                    Model_City.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
                    
                    cord_city_my_location = co_OrdinateCurrent
                    func_Geocoder(co_OrdinateCurrent) { (address) in
//                        self.nav_bar.title = address
                        self.lbl_title_1 .text = address
                        str_city_my_location = address
                    }
                    func_call_api()
                }
            }
        }

    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        is_tap_window = 0
        
//        let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
//        let img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
//        
//        img_marker.frame = view_hotel.frame
//        view_hotel.addSubview(img_marker)
//        marker.iconView = view_hotel
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        func_present_Fav_VC()
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return markerr(marker: marker)
    }
    
    func markerr(marker: GMSMarker) -> UIView{
//        let queue = DispatchQueue(label: "img_marker_window", qos: .background)
//        queue.async {
            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
            
            let img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
            img_marker.frame = view_hotel.frame
            view_hotel.addSubview(img_marker)
            marker.iconView = view_hotel
            
            self.mapview.sendSubview(toBack: view_hotel)
            self.mapview.bringSubview(toFront: view_hotel)
            
            self.is_tap_window = 1
            
            let marker_Info = Bundle.main.loadNibNamed("Marker_Info", owner: self, options: nil)?.first as! Marker_Info
            
            var frame = marker_Info.frame
            frame.size.width = self.view.frame.width-30
            marker_Info.frame = frame
            
            if let index = self.arr_markers.index(of: marker) {
                print(index)
                
                self.selected_long = self.arr_list[index].longitude
                self.selected_lat = self.arr_list[index].latitude
                
                if let selected_locations = self.loadCoordinates() {
                    self.arr_selected_marker = selected_locations
                    
                    for i in 0..<self.arr_selected_marker.count {
                        let location_cord = self.arr_selected_marker[i]
                        if location_cord.latitude == self.arr_list[index].latitude && location_cord.latitude == self.arr_list[index].latitude  {
                            self.arr_selected_marker[i] = CLLocationCoordinate2D(latitude: self.arr_list[index].latitude, longitude: self.arr_list[index].longitude)
                            
                            break
                        } else {
                            self.arr_selected_marker.append(CLLocationCoordinate2D(latitude: self.arr_list[index].latitude, longitude: self.arr_list[index].longitude))
                            
                            break
                        }
                    }
                } else {
                    self.arr_selected_marker.append(CLLocationCoordinate2D(latitude: self.arr_list[index].latitude, longitude: self.arr_list[index].longitude))
                }
                
                self.storeCoordinates(self.arr_selected_marker)
                
                Model_Details.shared.index_selected = index
                let model = Model_City.shared.arr_city_list[index]
                
                //            marker_Info.img.afset
                
                //            marker_Info.img.sd_setShowActivityIndicatorView(true)
                //            marker_Info.img.sd_setIndicatorStyle(.gray)
                //            marker_Info.img.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
                
                //            marker_Info.img.af_setImage(withURL:URL (string: model.shop_image)!, placeholderImage: (UIImage(named:"image-add-button.png")))
                
                //            marker_Info.img.downloadedFrom(link:URL (string: model.shop_image)!)
                
                //            marker_Info.img.downloaded(from:URL (string: model.shop_image)!)
                
                //            if let imageURL = URL(string:model.shop_image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
                ////                marker_Info.img.sd_setImage(with:imageURL, placeholderImage:(UIImage(named:"image-add-button.png")))
                //                marker_Info.img.af_setImage(withURL: imageURL, placeholderImage: UIImage(named: "personImage.png"))
                //            }
                
                
                //            marker_Info.img.sd_setShowActivityIndicatorView(true)
                //            marker_Info.img.sd_setIndicatorStyle(.gray)
//                            marker_Info.img.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
                
                //            imageFromURL(urlString: model.shop_image, imageView: marker_Info.img)
                //            marker_Info.img.imageFromURL(urlString: model.shop_image)
               
                if !model.shop_image.isEmpty {
                    let url = URL(string:model.shop_image)
                    let data = try? Data(contentsOf: url!)
                    self.img_m = UIImage(data: data!)
                }
                
                marker_Info.img.image = self.img_m
                marker_Info.lbl_name.text = model.shop_name
                marker_Info.lbl_owner_name.text = model.user_name
                marker_Info.lbl_location_address.text = model.shop_address
                marker_Info.lbl_date_of_update.text = model.shop_add_date.date_localized
                
                if model.avg_rating.isEmpty {
                    marker_Info.star.rating = 0
                } else {
                    marker_Info.star.rating = Float(model.avg_rating)!
                }
            }
//        }
        return marker_Info
    }
    
    
    
    func func_list_1() {
        mapview.clear()
        
        var bounds = GMSCoordinateBounds()
        var marker = GMSMarker()
        arr_markers.removeAll()
        
        for cord in arr_list {
            marker = GMSMarker(position: cord)
            
            let custom_marker = UIView (frame: CGRect (x: 0, y: 0, width: 60, height: 60))
            custom_marker.backgroundColor = color_app
            
            custom_marker.layer.cornerRadius = custom_marker.frame.size.height/2
            custom_marker.clipsToBounds = true
            
            let lbl_name_of_location = UILabel (frame: custom_marker.frame)
            lbl_name_of_location.text = "Available"
            lbl_name_of_location.textColor = UIColor .white
            lbl_name_of_location.font = UIFont (name: "Lato-Bold", size: 12.0)
            lbl_name_of_location.textAlignment = .center
            lbl_name_of_location.numberOfLines = 2
            lbl_name_of_location.adjustsFontSizeToFitWidth = true
            custom_marker.addSubview(lbl_name_of_location)
            
            marker.iconView = custom_marker
            arr_markers.append(marker)
            marker.map = self.mapview
            bounds = bounds .includingCoordinate(cord)
        }
        
//        if !is_padding {
//            is_padding = true
//            let update = GMSCameraUpdate.fit(bounds, withPadding: 160)
//            mapview.animate(with: update)
//        }
        
    }
    
    
    
    func func_list() {
        mapview.clear()
        
        var marker = GMSMarker()
        var bounds = GMSCoordinateBounds()
        arr_markers.removeAll()
        
        for i in 0..<arr_list.count {
            let cord = arr_list[i]
            marker = GMSMarker(position: cord)
            
            let custom_marker = UIView (frame: CGRect (x: 0, y: 0, width: 100, height: 50))
            custom_marker.backgroundColor = color_app
            
            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
            var img_marker = UIImageView()
            
            img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
            
            if let selected_locations = loadCoordinates() {
                arr_selected_marker = selected_locations
                for ii in 0..<arr_selected_marker.count {
                    if arr_selected_marker[ii].latitude == cord.latitude && arr_selected_marker[ii].latitude == cord.latitude {
                        img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
                        
                        break
                    }
                }
            }
            
            img_marker.frame = view_hotel.frame
            view_hotel.addSubview(img_marker)
            marker.iconView = view_hotel
            
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 5.2)
            marker.zIndex = Int32(i)
            
            if !arr_markers.contains(marker) {
                arr_markers.append(marker)
            }
            
            marker.map = self.mapview
            bounds = bounds .includingCoordinate(cord)
        }
        
        
        
//        if arr_list.count == 1 {
//            let cord = arr_list[0]
//            marker = GMSMarker(position: cord)
//
//            let position = CLLocationCoordinate2D(latitude:cord.latitude, longitude:cord.longitude)
//            let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
//
//            mapview.camera = camera
//            mapview.animate(to: camera)
//        }

        
        
//        if !is_padding {
//            is_padding = true
//            let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
//            mapview.animate(with: update)
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.is_zoom = true
            self.is_tap_window = 0
        }
        
    }
    
    
    func func_selected_marker() {
        
    }
    
    func func_present_Fav_VC() {
        Model_Details.shared.arr_details = Model_City.shared.arr_city_list

        let fav_VC = storyboard?.instantiateViewController(withIdentifier: "Details_ViewController") as! Details_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        present(fav_VC, animated: true, completion: nil)
    }
    
}

// MARK:- api functions

extension City_ViewController {
    @objc func func_get_category() {
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_cate.removeAll()
                
                if status == "success" {
                    self.arr_cate = Model_Category_List.shared.arr_category_list
                }
                
                for i in 0..<self.arr_cate.count {
//                    if i == 0 {
//                        self.arr_cate_is_selected_cell.append(true)
//                        Model_Category_List.shared.category_code = Model_Category_List.shared.arr_category_list[1].category_code
//                    } else {
                        self.arr_cate_is_selected_cell.append(false)
//                    }
                }
//                self.func_get_sub_category()
                self.coll_cate_1.reloadData()
            }
        }
    }
    
    func func_get_sub_category() {
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_sub_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_sub_cate.removeAll()
                if status == "success" {
                    self.arr_sub_cate = Model_Category_List.shared.arr_sub_category_list
                }
                for _ in 0..<self.arr_sub_cate.count {
                    self.arr_subcate_is_selected_cell.append(false)
                }
                self.coll_cate_2.reloadData()
            }
        }
    }
    
    
    @objc func get_shop_ad_list() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        }
        
        Model_City.shared.func_get_shop_ad_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                is_loaded_first_time = true
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_City.shared.arr_city_list {

                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    print(self.arr_list.count)
                }
                
                if Model_City.shared.arr_city_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_City.shared.arr_city_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list_1()
            }
        }
    }

}




// MARK:-  API METHODS
extension City_ViewController {
    func func_get_shop_ad_list_lat_long() {
        func_set_latlong_for_list()
        
        if !is_loaded_first_time {
            view_loader.isHidden = false
        } else {
            func_ShowHud()
        }
        
        Model_City.shared.func_get_shop_ad_list_lat_long { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                is_loaded_first_time = true
                self.func_HideHud()
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_City.shared.arr_city_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                } else {
                    let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_City.shared.arr_city_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_City.shared.arr_city_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list()
            }
        }
    }
    
    
    
    func func_get_shop_ad_list_category() {
        func_set_latlong_for_list()
        
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_City.shared.func_get_shop_ad_list_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                is_loaded_first_time = true
                self.func_HideHud()
                
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_City.shared.arr_city_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                }  else {
                                        let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_City.shared.arr_city_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_City.shared.arr_city_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list()
            }
        }
    }
    
    
    func func_get_shop_ad_list_subcategory() {
        func_set_latlong_for_list()
        
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_City.shared.func_get_shop_ad_list_subcategory { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                is_loaded_first_time = true
                self.func_HideHud()
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_City.shared.arr_city_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                }  else {
                                        let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_City.shared.arr_city_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_City.shared.arr_city_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list()
            }
        }
    }
    
    func func_get_shop_ad_list_mobile() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_City.shared.func_get_shop_ad_list_mobile { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                is_loaded_first_time = true
                self.func_HideHud()
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_City.shared.arr_city_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                }  else {
                    let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_City.shared.arr_city_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_City.shared.arr_city_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list()
            }
        }
    }
    
    func func_get_shop_ad_list() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_City.shared.func_get_shop_ad_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                is_loaded_first_time = true
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_City.shared.arr_city_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                } else {
                    let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_City.shared.arr_city_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_City.shared.arr_city_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list()
            }
        }
    }
    
    func func_set_latlong_for_list() {
        Model_Search_VC.shared.location_lat =  Model_City.shared.location_lat
        Model_Search_VC.shared.location_lang =  Model_City.shared.location_lang
    }
    
    @objc func func_call_api() {
        
        if Model_City.shared.category_code.isEmpty && Model_City.shared.subcate_code.isEmpty {
            func_get_shop_ad_list_lat_long()
        } else if Model_City.shared.category_code.isEmpty || Model_City.shared.subcate_code.isEmpty {
            if !Model_City.shared.subcate_code.isEmpty {
                 func_get_shop_ad_list_subcategory()
            } else {
                func_get_shop_ad_list_category()
            }
        } else if !Model_My_Location.shared.category_code.isEmpty && !Model_My_Location.shared.subcate_code.isEmpty {
            func_get_shop_ad_list_subcategory()
        } else if Model_City.shared.subcate_code.contains(",") || Model_City.shared.category_code.contains(",") {
            func_get_shop_ad_list_subcategory()
        } else {
            func_get_shop_ad_list_subcategory()
//            func_get_shop_ad_list()
        }
    }
    
}



extension City_ViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        place.accessibilityLanguage = "ar"
        
        let localized = "Address not found ".localized
        let str_selectYourLocations = place.formattedAddress ?? localized
        Model_Last_Searched.shared.city_name = str_selectYourLocations
        Model_Last_Searched.shared.city_id = place.placeID
        Model_Last_Searched.shared.city_lat = "\(place.coordinate.latitude)"
        Model_Last_Searched.shared.city_long = "\(place.coordinate.longitude)"
        
        func_save_city()
        
        Model_Search_VC.shared.location_lat = "\(place.coordinate.latitude)"
        Model_Search_VC.shared.location_lang = "\(place.coordinate.longitude)"
        
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "selected_location_google_city"), object: nil)
    }
    
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        //        Log.debug("Place name: \(String(describing: place.name))")
        //        Log.debug("Place address: \(String(describing: place.formattedAddress))")
        //        Log.debug("Place attributions: \(String(describing: place.attributions))")
        
        let placeID = place.placeID
        
        //        guard let placeID = place.placeID else {
        //            return
        //        }
        
        /// Use Google Place API to lookup place information in English
        /// https://developers.google.com/places/web-service/intro
        GooglePlacesAPI.GooglePlaces.placeDetails(forPlaceID: placeID, language: "AE") { (response, error) -> Void in
            
            guard let place = response?.result, response?.status == GooglePlaces.StatusCode.ok else {
                print("Error = \(String(describing: response?.errorMessage))")
                
                return
            }
            
            let postalCode = place.addressComponents.filter({ (components) -> Bool in
                return components.types.contains(kGMSPlaceTypePostalCode)
            }).first?.longName
            
            print(place)
            print(place.formattedAddress)
            print(place.name)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func func_selected_location() {
        is_tap_window = 1
        selected_location()
    }
    
    @objc func func_open_google() {
        var gmsautocomplete = GMSAutocompleteViewController()
        
//        let filter = GMSAutocompleteFilter()
//        filter.type = .establishment
//        filter.country = "AE"
//        
//        gmsautocomplete.autocompleteFilter = filter
        gmsautocomplete.delegate = self

        present(gmsautocomplete, animated: true, completion: nil)
    }
    
    func func_save_city() {
        Model_Last_Searched.shared.func_save_city { (status) in
            
        }
    }

    func storeCoordinates(_ coordinates: [CLLocationCoordinate2D]) {
        let locations = coordinates.map { coordinate -> CLLocation in
            return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        let archived = NSKeyedArchiver.archivedData(withRootObject: locations)
        UserDefaults.standard.set(archived, forKey: "coordinates")
        UserDefaults.standard.synchronize()
    }
    
    // Return an array of CLLocationCoordinate2D
    func loadCoordinates() -> [CLLocationCoordinate2D]? {
        guard let archived = UserDefaults.standard.object(forKey: "coordinates") as? Data,
            let locations = NSKeyedUnarchiver.unarchiveObject(with: archived) as? [CLLocation] else {
                return nil
        }
        
        let coordinates = locations.map { location -> CLLocationCoordinate2D in
            return location.coordinate
        }
        return coordinates
    }
    
    
}



extension UIViewController {
    func func_IsLocationEnable() -> Bool {
        var func_IsLocationEnable = false
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                
                let alert = UIAlertController(title: "Allow Location", message:"To show shops according to selected location", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction (title: "Cancel", style: .cancel, handler: nil)
                let allow = UIAlertAction (title:"Allow", style: .default, handler: { (alert: UIAlertAction!) in
                    UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
                })
                
                alert.addAction(ok)
                alert.addAction(allow)
                present(alert, animated: true, completion: nil)
                
                func_IsLocationEnable = false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                func_IsLocationEnable = true
                
            default:
                break
            }
        }
        return func_IsLocationEnable
    }
}




extension UIViewController {
    public func imageFromURL(urlString: String,imageView:UIImageView) {
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height)
        activityIndicator.startAnimating()
//        if self.image == nil {
            imageView.addSubview(activityIndicator)
//        }
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview()
                imageView.image = image
            })
        }).resume()
    }
}
