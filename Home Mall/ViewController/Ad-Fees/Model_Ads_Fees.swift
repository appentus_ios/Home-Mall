
//  Model_Feedback.swift
//  Home Mall
//
//  Created by iOS-Appentus on 31/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import Foundation

class Model_Ads_Fees {
    static let shared = Model_Ads_Fees()
    
    var package_id = ""
    var package_name = ""
    var package_price = ""
    var package_offer = ""
    
    var condtion = ""
    var condtion_Submit = ""
    
    var arr_get_packages = [Model_Ads_Fees]()
    
    func func_get_packages(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_packages"
        
        API_Home_Mall.func_API_Call_GET(apiName: str_FullURL) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.arr_get_packages.removeAll()
                
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_packages.append(self.func_set_get_packages(dict: dict_json))
                }
                
                if let get_condition = dict_JSON["condtion"] as? [String:Any] {
                    self.func_set_condition(dict: get_condition)
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_set_get_packages(dict:[String:Any]) -> Model_Ads_Fees {
        let model = Model_Ads_Fees()
        
        model.package_id = "\(dict["package_id"] ?? "")"
        model.package_name = "\(dict["package_name"] ?? "")"
        model.package_price = "\(dict["package_price"] ?? "")"
        model.package_offer = "\(dict["package_offer"] ?? "")"
        
        return model
    }
    
    private func func_set_condition(dict:[String:Any]) {
        print(dict)
        
        let one = "\(dict["1"] ?? "")"
        let two = "\(dict["2"] ?? "")"
        let three = "\(dict["3"] ?? "")"
        let four = "\(dict["4"] ?? "")"
        let five = "\(dict["5"] ?? "")"
        let six = "\(dict["6"] ?? "")"
        
        let arr_Six = six.components(separatedBy: "\n")
        print(arr_Six)
        
        self.condtion_Submit = arr_Six[1]
        
        self.condtion = "\(one) \n\(two) \n\(three) \n\(four) \n\(five) \n\(arr_Six[0])"
     }
    
}





