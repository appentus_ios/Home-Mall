
//
//  Ads_Fees_TableViewCell.swift
//  Home Mall
//
//  Created by iOS-Appentus on 28/02/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Ads_Fees_TableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_package_name:UILabel!
    @IBOutlet weak var lbl_sr_pr_month:UILabel!
    @IBOutlet weak var lbl_package_price:UILabel!
    
    @IBOutlet weak var lbl_package_offer:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_package_price.layer.borderColor = UIColor .darkGray .cgColor
        lbl_package_price.layer.borderWidth = 2.3
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }

    
    
}
