

//
//  ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Ads_Fees_ViewController: UIViewController {
  @IBOutlet weak var btn_pay_method:UIButton!
    
    @IBOutlet weak var lbl_condition:UILabel!
//    @IBOutlet weak var lbl_800_2:UILabel!
    
    @IBOutlet weak var tbl_ads_fees:UITableView!
    @IBOutlet weak var view_condition_container:UIView!
    @IBOutlet weak var height_condition:NSLayoutConstraint!
    
    @IBOutlet weak var lbl_pay_method:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_pay_method.layer.cornerRadius = 10
        lbl_pay_method.layer.shadowOpacity = 3.0
        lbl_pay_method.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        lbl_pay_method.layer.shadowRadius = 3.0
        lbl_pay_method.layer.shadowColor = color_app.cgColor
        
        lbl_pay_method.text = "وسائل الدفع"
        
        func_get_packages()
    }
    
    func func_get_packages() {
        let view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = false
        Model_Ads_Fees.shared.func_get_packages { (status) in
            DispatchQueue.main.async {
                view_loader.isHidden = true
                
                self.lbl_pay_method.text = "أتعهد وأوافق على الشروط و أقسم وسائل الدفع\n"
                
                self.lbl_condition.text = Model_Ads_Fees.shared.condtion
                self.height_condition.constant = self.func_height_text(textString:Model_Ads_Fees.shared.condtion as NSString)+40
                self.view_condition_container.frame = CGRect (x: 0, y: 0, width: self.view.bounds.width, height: self.height_condition.constant+60)
                
                self.tbl_ads_fees.reloadData()
            }
        }
    }
    
    func func_height_text(textString:NSString) -> CGFloat {
        let font = UIFont .systemFont(ofSize: 18)
        let textAttributes = [NSAttributedStringKey.font: font]
        
        let textRect = textString.boundingRect(with: CGSize (width: self.view.frame.size.width-30, height: 2000), options: .usesLineFragmentOrigin, attributes: textAttributes, context: nil)
        return textRect.size.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_pay_method(_ sender:Any) {
        let pay_methods = storyboard?.instantiateViewController(withIdentifier: "Pay_Method_ViewController") as! Pay_Method_ViewController
        pay_methods.modalPresentationStyle = .fullScreen
        present(pay_methods, animated: true, completion: nil)
    }

}




extension Ads_Fees_ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Ads_Fees.shared.arr_get_packages.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Ads_Fees_TableViewCell
        
        let model = Model_Ads_Fees.shared.arr_get_packages[indexPath.row]
        
        cell.lbl_package_name.text = "\(model.package_name)"
        cell.lbl_package_offer.text = model.package_offer
        cell.lbl_package_price.text = model.package_price
        
        let str_pkg_name = model.package_name.components(separatedBy: " ")
        
        if indexPath.row == 0 {
            cell.lbl_sr_pr_month.text = "% / طلب"
        } else {
            cell.lbl_sr_pr_month.text = "SR / طلب"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}


