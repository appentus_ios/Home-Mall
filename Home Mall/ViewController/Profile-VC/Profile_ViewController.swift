
//  MenuViewController.swift
//  AKSwiftSlideMenu
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.


import UIKit
import SDWebImage


protocol Profile_SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}



class Profile_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_Please_login:UIView!
    @IBOutlet weak var view_logout:UIView!
    @IBOutlet weak var view_logout_pop_up:UIView!
    
    @IBOutlet weak var btn_no:UIButton!
    @IBOutlet weak var btn_yes:UIButton!
    
    var view_my_profile = UIView()
    
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    var btnMenu : UIButton!
    
    var delegate : Profile_SlideMenuDelegate?
    
    var view_loader = UIView()
    
    @IBOutlet weak var view_login_container:UIView!
    @IBOutlet weak var btn_cancel:UIButton!
    @IBOutlet weak var btn_login:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_rounded_corner(view: view_login_container)
        func_rounded_corner(view: btn_cancel)
        func_rounded_corner(view: btn_login)
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        btn_no.layer.cornerRadius = 10
        btn_no.clipsToBounds = true
        
        btn_yes.layer.cornerRadius = 10
        btn_yes.clipsToBounds = true
        
        view_logout_pop_up.isHidden = true
        
        view_2.layer.cornerRadius = view_2.frame.size.height/2
        view_2.clipsToBounds = true
        
        view_Please_login.isHidden = true
        
        NotificationCenter.default.addObserver(self,selector: #selector(func_for_login),name:NSNotification.Name (rawValue: "for_login"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(func_for_cancel(_:)),name:NSNotification.Name (rawValue: "for_cancel"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(func_after_login),name:NSNotification.Name (rawValue: "after_login"),object: nil)
        
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            view_logout.isHidden = false
        } else {
            view_logout.isHidden = true
        }
        
    }
    
    func func_rounded_corner(view:UIView)  {
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
        
    }
    
   @objc func func_after_login() {
        view_Please_login.isHidden = true
    
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            view_logout.isHidden = false
            
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            Model_Splash.shared.user_id = "\(dict_LoginData["user_id"] ?? "")"
        } else {
            view_logout.isHidden = true
        }
    }
    
    @objc func func_for_login() {
        view_my_profile.isHidden = true
        
        let my_profilee = storyboard?.instantiateViewController(withIdentifier: "Please_login_first_ViewController") as! Please_login_first_ViewController
        remove(vc: my_profilee)
        
        let my_profile = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        my_profile.modalPresentationStyle = .fullScreen
        present(my_profile, animated: true, completion: nil)
    }
    
    @objc func func_for_cancel(_ sender: UIButton) {
//        remove()
//         view_my_profile.isHidden = true
//        onSlideMenuButtonPressed(sender)
    }
    
    @IBAction func btn_edit_profile(_ button:UIButton!){
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            Model_Splash.shared.user_id = "\(dict_LoginData["user_id"] ?? "")"
            
            view_Please_login.isHidden = true
            
            let my_profile = storyboard?.instantiateViewController(withIdentifier: "My_Profile_ViewController") as! My_Profile_ViewController
            my_profile.modalPresentationStyle = .fullScreen
            present(my_profile, animated: true, completion: nil)
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
        }
    }
    
    @IBAction func btn_favorites(_ button:UIButton!) {
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            view_Please_login.isHidden = true
            let terms_of_ad = storyboard?.instantiateViewController(withIdentifier: "Favorites_ViewController") as! Favorites_ViewController
            terms_of_ad.modalPresentationStyle = .fullScreen
            present(terms_of_ad, animated: true, completion: nil)
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
        }
        
    }
    
    @IBAction func btn_my_shop(_ button:UIButton!) {
        is_from_profile = false
        
        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            view_Please_login.isHidden = true
            func_get_shop_ad_list_mobile()
            
//            let terms_of_ad = storyboard?.instantiateViewController(withIdentifier: "Terms_of_Advertising_ViewController") as! Terms_of_Advertising_ViewController
//            present(terms_of_ad, animated: true, completion: nil)
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
        }
        
    }
    
    
    @IBAction func btn_my_Ads(_ button:UIButton!) {
//        if let _ = UserDefaults.standard.object(forKey: "login_Data") as? Data {
//            view_Please_login.isHidden = true
            let terms_of_ad = storyboard?.instantiateViewController(withIdentifier: "My_Ads_ViewController") as! My_Ads_ViewController
        terms_of_ad.modalPresentationStyle = .fullScreen
            present(terms_of_ad, animated: true, completion: nil)
//        } else {
//            view_Please_login.isHidden = false
//            is_myshop = true
//            is_myshop_login = true
//        }
        
    }

    
    @IBAction func btn_logout(_ button:UIButton!) {
        view_logout_pop_up.isHidden = false
    }
    
    @IBAction func btn_logout_no(_ button:UIButton!) {
        view_logout_pop_up.isHidden = true
    }
    
    @IBAction func btn_logout_yes(_ button:UIButton!) {
        view_logout_pop_up.isHidden = true
        
        UserDefaults.standard.removeObject(forKey: "login_Data")
        view_logout.isHidden = true
        NotificationCenter .default.post(name: NSNotification.Name .init(rawValue: "login_chat"), object: nil)
    }
    
    @IBAction func btn_login(_ button:UIButton!) {
        view_Please_login.isHidden = true
        let my_profile = storyboard?.instantiateViewController(withIdentifier: "Login_ViewController") as! Login_ViewController
        my_profile.modalPresentationStyle = .fullScreen
        present(my_profile, animated: true, completion: nil)
    }
    
    @IBAction func btn_cancel(_ button:UIButton!) {
        view_Please_login.isHidden = true
    }
    
    @IBAction func btn_call(_ button:UIButton!) {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let _ = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
//            print(dict_LoginData)
            view_Please_login.isHidden = true

            Model_Chat_Message_Fri.shared.fri_id = "50"
            Model_Chat_Message_Fri.shared.fri_name = "Admin"
            Model_Chat_List.shared.user_role = "2"
            
            let chat_user_list = storyboard?.instantiateViewController(withIdentifier: "Chat_Message_Fri_ViewController") as! Chat_Message_Fri_ViewController
            chat_user_list.modalPresentationStyle = .fullScreen
            present(chat_user_list, animated: true, completion: nil)
        } else {
            view_Please_login.isHidden = false
            
            is_myshop = true
            is_myshop_login = true
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        cell.selectionStyle = .none
        
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton (type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}



extension Profile_ViewController {
    func func_get_shop_ad_list_mobile() {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            
            print(dict_LoginData)
            Model_My_Profile.shared.user_mobile = "\(dict_LoginData["user_mobile"] ?? "")"
        }
        
        view_loader.isHidden = false
        
        Model_My_Profile.shared.func_get_shop_ad_list_mobile { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if Model_My_Profile.shared.arr_My_Ads.count > 0 {
                    let terms_of_ads = self.storyboard?.instantiateViewController(withIdentifier: "My_Shop_Manage_ViewController") as! My_Shop_Manage_ViewController
                    terms_of_ads.modalPresentationStyle = .fullScreen
                    self.present(terms_of_ads, animated: true, completion: nil)
                } else {
                    let terms_of_ads = self.storyboard?.instantiateViewController(withIdentifier: "Terms_of_Advertising_ViewController") as! Terms_of_Advertising_ViewController
                    terms_of_ads.modalPresentationStyle = .fullScreen
                    self.present(terms_of_ads, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!) {
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x:UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            //                self.removeFromParent()
        })
    }
    
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.append(["title":"Home", "icon":"HomeIcon"])
        arrayMenuOptions.append(["title":"Play", "icon":"PlayIcon"])
        
        //        tblMenuOptions.reloadData()
    }
}

import UIKit

extension UIViewController:Profile_SlideMenuDelegate {
//extension Profile_ViewController {

    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        //        let topViewController : UIViewController = self.navigationController!.topViewController!
        //        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            print("Home\n", terminator: "")

            //            self.openViewControllerBasedOnIdentifier("Home")

            break
        case 1:
            print("Play\n", terminator: "")

            //            self.openViewControllerBasedOnIdentifier("PlayVC")
            
            break
        default:
            print("default\n", terminator: "")
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)

        let topViewController : UIViewController = self.navigationController!.topViewController!

        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }

    func addSlideMenuButton() {
        let btnShowMenu = UIButton (type: UIButtonType.system)
        btnShowMenu.setImage(self.defaultMenuImage(), for:.normal)
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(onSlideMenuButtonPressed_1(_:)), for:.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }

    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()

        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)

        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()

        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()

        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!

        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed_1(_ sender : UIButton) {
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : Profile_ViewController = self.storyboard!.instantiateViewController(withIdentifier: "Profile_ViewController") as! Profile_ViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        //        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.15, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }

    



    
}


