
//  ViewController.swift
//  Home Mall

//  Created by iOS-Appentus on 09/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit
import GoogleMaps
import SDWebImage

class My_Loction_ViewController: UIViewController,GMSMapViewDelegate {
    @IBOutlet weak var mapview:GMSMapView!
//    @IBOutlet weak var map_duplicate:GMSMapView!
    
    @IBOutlet weak var coll_cate_1:UICollectionView!
    @IBOutlet weak var coll_cate_2:UICollectionView!
    
    @IBOutlet weak var lbl_morethan_ads:UILabel!
    
    @IBOutlet weak var view_cate:UIView!
    
    var arr_list = [CLLocationCoordinate2D]()
    
    var arr_markers = [GMSMarker]()
    
    var  arr_cate = [Model_Category_List]()
    var  arr_sub_cate = [Model_Category_List]()
    
    var arr_cate_is_selected_cell = [Bool]()
    var arr_subcate_is_selected_cell = [Bool]()
    
    var locationManager = CLLocationManager()
    
    var co_OrdinateCurrent = CLLocationCoordinate2DMake(0.0, 0.0)
    
    var is_tap_window = false
    var is_tap_window_available = false
    
    var is_padding = false
    var is_updated_location = false
    var is_loaded_first_time = false
    var view_loader = UIView()
    var is_show_marker_window = false
    
    var arr_selected_marker_gray = [CLLocationCoordinate2D]()
    
    var marker_SelectedLocation = GMSMarker()
    
//    @IBOutlet weak var view_title:UIView!
    @IBOutlet weak var navbar:UINavigationBar!
    @IBOutlet weak var hight_view_title:NSLayoutConstraint!
    
    var is_city_refresh = false
    
    var str_there_are_more_than = ""
    var str_ads = ""
    
    var selected_marker_index = -1
    var selected_lat = 0.0
    var selected_long = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        str_there_are_more_than = "There are more than".localized
        str_ads = "ads".localized
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        
        view_cate.layer.shadowOpacity = 2.5
        view_cate.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_cate.layer.shadowRadius = 1.5
        view_cate.layer.shadowColor = UIColor .gray.cgColor
        
//      func_get_category()
        
        coll_cate_1.layer.cornerRadius = coll_cate_1.frame.size.height/2
        coll_cate_2.layer.cornerRadius = coll_cate_2.frame.size.height/2
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
//        mapview.isMyLocationEnabled = true
//        mapview.settings.myLocationButton = true
        
        is_loaded_first_time = false
        let localized = "Select a city".localized
        navbar.topItem!.title = localized
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_refresh), name: NSNotification.Name.init(rawValue: "City"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_get_city), name: NSNotification.Name (rawValue:"back"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        is_enter_city = false
        
        is_From_city_myloc = "city"
        if arr_selected_marker_gray.count > 0 {
            arr_selected_marker_gray.removeAll()
        }
        
        is_loaded_first_time = false
        is_padding = false
        is_show_marker_window = false
        
        hight_view_title.constant = 2
        // view_cate.isHidden = true
        
        arr_list.removeAll()
        arr_markers.removeAll()
        
        func_get_city()
        
//        mapview.settings.setAllGesturesEnabled(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func func_refresh() {
        if is_city_refresh {
            func_get_city()
        }
    }
    
    @IBAction func btn_search(_ sender:UIButton) {
        onSlideMenuButtonPressed(sender)
    }
    
}



// MARK:- GMSMapView Delegates
extension My_Loction_ViewController {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !func_IsLocationEnable() {
            return
        }
        
        print("map visible radius:-",mapview.getRadius())
        Model_My_Location.shared.raduis_city = "\(mapview.getRadius())"
        
        cord_city_my_location = position.target
        
        if mapView.camera.zoom <= 5 {
            func_refresh()
        } else if is_show_marker_window {
            if is_tap_window_available {
                if is_tap_window == false {
                    co_OrdinateCurrent = position.target
                    cord_city_my_location = co_OrdinateCurrent
                    Model_My_Location.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
                    Model_My_Location.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
                    
                    func_call_api()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                    self.is_tap_window_available = true
                }
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        func_present_Fav_VC()
    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        is_tap_window = false
        
//        if is_show_marker_window {
//            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
//            let img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
//            
//            img_marker.frame = view_hotel.frame
//            view_hotel.addSubview(img_marker)
//            marker.iconView = view_hotel
//        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if !is_show_marker_window {
            is_enter_city = true
            
            if let index = arr_markers.index(of: marker) {
                print(index)
                let model = Model_My_Location.shared.arr_get_city_shop[index]
                Model_My_Location.shared.city_id = model.city_id
                
//                lbl_title.text = model.city_name
                navbar.topItem!.title = model.city_nicname
                str_city_my_location = model.city_nicname
                
                Model_My_Location.shared.category_code = ""
                Model_My_Location.shared.subcate_code = ""
                Model_My_Location.shared.user_mobile = ""
                
                Model_My_Location.shared.location_lat = model.city_lat
                Model_My_Location.shared.location_lang = model.city_long
                
                Model_Search_VC.shared.location_lat =  model.city_lat
                Model_Search_VC.shared.location_lang = model.city_long
                
                let double_lat = Double(model.city_lat)!
                let double_long = Double(model.city_long)!
                cord_city_my_location = CLLocationCoordinate2D (latitude: double_lat, longitude: double_long)
                
                is_tap_window_available = false
            }
            
            is_padding = false
            is_city_refresh = true
            is_show_marker_window = true
            
            mapView.clear()
            let location_lat = Double(Model_My_Location.shared.location_lat)
            let location_lang = Double(Model_My_Location.shared.location_lang)
            
            let position = CLLocationCoordinate2D(latitude:location_lat!, longitude:location_lang!)
            let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
            
            mapView.camera = camera
            mapView.animate(to: camera)
            
            self.func_get_category()
            
            return nil
        } else {
            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
            
            let img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
            
            img_marker.frame = view_hotel.frame
            view_hotel.addSubview(img_marker)
            marker.iconView = view_hotel
            
            is_tap_window = true
            
            let marker_Info = Bundle.main.loadNibNamed("Marker_Info", owner: self, options: nil)?.first as! Marker_Info
            
            var frame = marker_Info.frame
            frame.size.width = self.view.frame.width-30
            marker_Info.frame = frame
            
            if let index = arr_markers.index(of: marker) {
                print(index)
                
                selected_long = arr_list[index].longitude
                selected_lat = arr_list[index].latitude
                
                if let selected_locations = loadCoordinates() {
                    arr_selected_marker_gray = selected_locations
                    
                    for i in 0..<arr_selected_marker_gray.count {
                        let location_cord = arr_selected_marker_gray[i]
                        if location_cord.latitude == arr_list[index].latitude && location_cord.latitude == arr_list[index].latitude  {
                            arr_selected_marker_gray[i] = CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude)
                            
                            break
                        } else {
                            arr_selected_marker_gray.append(CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude))
                            
                            break
                        }
                    }
                } else {
                    arr_selected_marker_gray.append(CLLocationCoordinate2D(latitude: arr_list[index].latitude, longitude: arr_list[index].longitude))
                }
                
                storeCoordinates(arr_selected_marker_gray)
                
                Model_Details.shared.index_selected = index
                let model = Model_My_Location.shared.arr_get_shop_ad_list[index]
                
//                marker_Info.img.sd_setShowActivityIndicatorView(true)
//                marker_Info.img.sd_setIndicatorStyle(.gray)
//                marker_Info.img.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
                
                if !model.shop_image.isEmpty {
                    let url = URL(string:model.shop_image)
                    let data = try? Data(contentsOf: url!)
                    marker_Info.img.image = UIImage(data: data!)
                }
                marker_Info.lbl_name.text = model.shop_name
                marker_Info.lbl_owner_name.text = model.user_name
                marker_Info.lbl_location_address.text = model.shop_address
                marker_Info.lbl_date_of_update.text = model.shop_add_date.date_localized
                
                if model.avg_rating.isEmpty {
                    marker_Info.star.rating = 0
                } else {
                    marker_Info.star.rating = Float(model.avg_rating)!
                }
            }
            return marker_Info
        }
        
    }
    
    
    func func_Geocoder(_ coordinate: CLLocationCoordinate2D,completion:@escaping (String)->()) {
        let cllocation = CLLocation (latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        //        print("\(Locale.current.regionCode)")
        
        var languageCode = ""
        if "\(Locale.current.regionCode ?? "")".lowercased() == "in" {
            languageCode = "en"
        } else {
            languageCode = "ar"
        }
        
        if #available(iOS 11.0, *) {
            CLGeocoder().reverseGeocodeLocation(cllocation, preferredLocale:Locale.init(identifier: languageCode), completionHandler: {
                (placemarks, error) -> Void in
                
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    let dict_Address = pm.addressDictionary
                    //                    print("\(dict_Address!["Country"]!)")
                    //                    print("\(dict_Address!["State"]!)")
                    //                    print("\(dict_Address!["SubAdministrativeArea"]!)")

                    var string_final = "\(dict_Address!["SubLocality"] ?? "") \(dict_Address!["City"] ?? "")"
                    
//                    var string_final = ""
//
//                    if dict_Address!["FormattedAddressLines"] != nil{
//                        let str = dict_Address!["FormattedAddressLines"]! as! NSArray
//
//                        for i in 0..<str.count{
//                            string_final += "\(str[i]) "
//                        }
//                    }
//
//                    string_final = string_final.replacingOccurrences(of:"\(dict_Address!["Country"]!)", with: "")
                    
                    //                    let str_Street = dict_Address!["Street"] ?? ""
                    //                    let str_Name = dict_Address!["Name"] ?? ""
                    //                    let str_City = dict_Address!["City"] ?? ""
                    //
                    //                    let str_state = dict_Address!["State"] ?? ""
                    //                    let str_country = dict_Address!["Country"] ?? ""
                    
                    
                    completion(string_final)
                } else {
                    print("Problem with the data received from geocoder")
                }
            })
        } else {
            // Fallback on earlier versions
        }
        
        
        
//        let geocoder = GMSGeocoder()
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            guard let address = response?.firstResult() else {
//                let localized = "Address not found ".localized
//                completion(localized)
//                return
//            }
//
//            let str_Address = "\(address.locality ?? ""), \(address.administrativeArea ?? ""), \(address.country ?? "")"
//            print(str_Address)
//
//            completion(str_Address)
//        }
    }
    
}



//  MARK:- UICollectionView methods
extension My_Loction_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets (top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == coll_cate_1 {
            let model = arr_cate[indexPath.row]
            let width = func_size(width:100, messageText: model.category_name).width+14
            return CGSize (width: width, height: collectionView.frame.size.height)
            
            //            return CGSize (width: 60, height: collectionView.frame.size.height)
        } else {
            let model = arr_sub_cate[indexPath.row]
            let width = func_size(width:100, messageText: model.subcate_name).width+14
            return CGSize (width: width, height: collectionView.frame.size.height)
            
            //            return CGSize (width: 80, height: collectionView.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == coll_cate_1 {
            return arr_cate.count
        } else {
            return arr_sub_cate.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == coll_cate_1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell-1", for: indexPath)
            
            let lbl_cate = cell.viewWithTag(1) as? UILabel
            let model = arr_cate[indexPath.row]
            lbl_cate?.text = model.category_name
            
            if arr_cate_is_selected_cell[indexPath.row] {
                lbl_cate?.textColor = UIColor .black
            } else {
                lbl_cate?.textColor = UIColor .white
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell-2", for: indexPath)
            
            let lbl_cate = cell.viewWithTag(1) as? UILabel
            let model = arr_sub_cate[indexPath.row]
            lbl_cate?.text = model.subcate_name
            
            if arr_subcate_is_selected_cell[indexPath.row] {
                lbl_cate?.textColor = UIColor .black
            } else {
                lbl_cate?.textColor = UIColor .white
            }
            
            return cell
        }
        
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Model_My_Location.shared.raduis_city = "\(mapview.getRadius())"
        
        if collectionView == coll_cate_1 {
            is_padding = false
            is_loaded_first_time = false
            
            for i in 0..<arr_cate_is_selected_cell.count {
                if i == indexPath.row {
                    arr_cate_is_selected_cell[i] = true
                }  else {
                    arr_cate_is_selected_cell[i] = false
                }
            }
            
            Model_Search_VC.shared.subcate_code = ""
            Model_My_Location.shared.subcate_code = ""
            Model_City.shared.subcate_code = ""
            
            Model_Search_VC.shared.category_code = arr_cate[indexPath.row].category_code
            Model_My_Location.shared.category_code = arr_cate[indexPath.row].category_code
            Model_Category_List.shared.category_code = arr_cate[indexPath.row].category_code
            
            coll_cate_1 .reloadData()
            Model_Search_VC.shared.subcate_code = ""
            Model_City.shared.subcate_code = ""
            
            arr_subcate_is_selected_cell.removeAll()
            
            if Model_Category_List.shared.category_code.isEmpty {
                arr_sub_cate.removeAll()
                arr_subcate_is_selected_cell.removeAll()
                self.coll_cate_2.reloadData()
            } else {
                func_get_sub_category()
            }
        } else {
            for i in 0..<arr_subcate_is_selected_cell.count {
                if i == indexPath.row {
                    arr_subcate_is_selected_cell[i] = true
                } else {
                    arr_subcate_is_selected_cell[i] = false
                }
            }
            
            Model_Search_VC.shared.subcate_code = arr_sub_cate[indexPath.row].subcate_code
            Model_My_Location.shared.subcate_code = arr_sub_cate[indexPath.row].subcate_code
            Model_City.shared.subcate_code = arr_sub_cate[indexPath.row].subcate_code
            
            coll_cate_2.reloadData()
        }
        func_call_api()
    }
    
}



extension My_Loction_ViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if !is_updated_location {
//            is_updated_location = true
//
//            co_OrdinateCurrent = manager.location!.coordinate
//            Model_Search_VC.shared.location_lat = "\(co_OrdinateCurrent.latitude)"
//            Model_Search_VC.shared.location_lang = "\(co_OrdinateCurrent.longitude)"
//
//            get_shop_ad_list()
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}

// MARK:- Custom functions
extension My_Loction_ViewController {
    func func_present_Fav_VC() {
        
        Model_Details.shared.arr_details = Model_My_Location.shared.arr_get_shop_ad_list
        let fav_VC = storyboard?.instantiateViewController(withIdentifier: "Details_ViewController") as! Details_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        present(fav_VC, animated: true, completion: nil)
    }
    
    func func_list() {
        let localized = "Select a city".localized
        navbar.topItem!.title = localized
        
        mapview.clear()
        
        var bounds = GMSCoordinateBounds()
        var marker = GMSMarker()
        arr_markers.removeAll()
            for i in 0..<arr_list.count {
                let cord = arr_list[i]
                marker = GMSMarker(position: cord)
                
                
                if Model_My_Location.shared.arr_get_city_shop.count == 0 {
                    return
                }
                let model = Model_My_Location.shared.arr_get_city_shop[i]
                
                let custom_marker = UIView (frame: CGRect (x: 0, y: 0, width: 50, height: 50))
                custom_marker.backgroundColor = color_app
                
                custom_marker.layer.cornerRadius = custom_marker.frame.size.height/2
                custom_marker.clipsToBounds = true
                
                let lbl_name_of_location = UILabel (frame: custom_marker.frame)
                //            let city_name = model.city_name.components(separatedBy: ",")
                lbl_name_of_location.text = model.city_nicname //city_name[0]
                lbl_name_of_location.textColor = UIColor .white
                lbl_name_of_location.font = UIFont (name: "Lato-Bold", size: 10.0)
                lbl_name_of_location.textAlignment = .center
                lbl_name_of_location.numberOfLines = 2
                lbl_name_of_location.adjustsFontSizeToFitWidth = true
                custom_marker.addSubview(lbl_name_of_location)
                
                marker.iconView = custom_marker
                
                if !arr_markers.contains(marker) {
                    arr_markers.append(marker)
                }
                
                marker.map = self.mapview
                bounds = bounds .includingCoordinate(cord)
            }
        
        
        
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            mapview.animate(with: update)
    }
    
    func func_list_1() {
        mapview.clear()
        
        var marker = GMSMarker()
        var bounds = GMSCoordinateBounds()
        arr_markers.removeAll()
        
        for i in 0..<arr_list.count {
            let cord = arr_list[i]
            marker = GMSMarker(position: cord)
            
            let custom_marker = UIView (frame: CGRect (x: 0, y: 0, width: 100, height: 50))
            custom_marker.backgroundColor = color_app
            
            let view_hotel = UIView (frame:  CGRect (x: 0, y: 0, width: 40, height: 40))
            var img_marker = UIImageView()
            
            img_marker = UIImageView.init(image: UIImage (named: "home-mall-logo.png"))
            
            if let selected_locations = loadCoordinates() {
                arr_selected_marker_gray = selected_locations
                
                for ii in 0..<arr_selected_marker_gray.count {
                    if arr_selected_marker_gray[ii].latitude == cord.latitude && arr_selected_marker_gray[ii].latitude == cord.latitude {
                        img_marker = UIImageView.init(image: UIImage (named: "gray-light.png"))
                        
                        break
                    }
                }
            }
            
            img_marker.frame = view_hotel.frame
            view_hotel.addSubview(img_marker)
            marker.iconView = view_hotel
            
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 5.2)
            marker.zIndex = Int32(i)
            
            arr_markers.append(marker)
            marker.map = self.mapview
            bounds = bounds .includingCoordinate(cord)
        }
        
        if !is_padding {
            is_padding = true
            
            if arr_list.count == 1 {
//                let cord = arr_list[0]
//                marker = GMSMarker(position: cord)
//
//                let position = CLLocationCoordinate2D(latitude:cord.latitude, longitude:cord.longitude)
//                let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 13.8)
//
//                mapview.camera = camera
//                mapview.animate(to: camera)
            } else {
                let update = GMSCameraUpdate.fit(bounds, withPadding: 160)
                mapview.animate(with: update)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.is_show_marker_window = true
        }
    }

}



// MARK:- api functions
extension My_Loction_ViewController {
    @objc func func_get_city() {
        is_city_refresh = false
        is_tap_window_available = false
        Model_My_Location.shared.category_code = ""
        Model_My_Location.shared.subcate_code = ""
        Model_My_Location.shared.user_mobile = ""
        
        view_loader.isHidden = false
        hight_view_title.constant = 2
        
        mapview.setMinZoom(5, maxZoom: 9)
        
        Model_My_Location.shared.func_get_city_shop { (status) in
            DispatchQueue.main.async {
                self.is_show_marker_window = false
                self.view_loader.isHidden = true
                self.arr_list.removeAll()
                
                self.fun_get_shop_count()
                
                self.arr_list.removeAll()
                
                if status == "success" {
                    for model in Model_My_Location.shared.arr_get_city_shop {
                        var city_lat = 0.0
                        var city_long = 0.0
                        
                        if !model.city_lat.isEmpty {
                            city_lat = Double(model.city_lat)!
                        }
                        
                        if !model.city_long.isEmpty {
                            city_long = Double(model.city_long)!
                        }
                        self.arr_list.append(CLLocationCoordinate2D (latitude: city_lat, longitude: city_long))
                    }
                    print(self.arr_list.count)
                }
                
                if Model_My_Location.shared.arr_get_city_shop.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.arr_get_city.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                
                self.func_list()
            }
        }
    }
    
    @objc func fun_get_shop_count() {
        view_loader.isHidden = false
        Model_My_Location.shared.fun_get_shop_count { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.count) \(self.str_ads)"
            }
        }
    }
    
    
    
    func func_get_category() {
        mapview .setMinZoom(0, maxZoom: 33)
        
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_cate.removeAll()
                
                self.func_get_shop_ad_list_lat_long()
                
                if status == "success" {
                    self.arr_cate = Model_Category_List.shared.arr_category_list
                }
                
                for i in 0..<self.arr_cate.count {
//                    if i == 0 {
//                        self.arr_cate_is_selected_cell.append(true)
//                        Model_Category_List.shared.category_code = Model_Category_List.shared.arr_category_list[1].category_code
//                    } else {
                        self.arr_cate_is_selected_cell.append(false)
//                    }
                }
//                self.func_get_sub_category()
                self.coll_cate_1.reloadData()
            }
        }
    }
    
    func func_get_sub_category() {
        view_loader.isHidden = false
        Model_Category_List.shared.func_get_sub_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_sub_cate.removeAll()
                if status == "success" {
                    self.arr_sub_cate = Model_Category_List.shared.arr_sub_category_list
                }
                
                for _ in 0..<self.arr_sub_cate.count {
                    self.arr_subcate_is_selected_cell.append(false)
                }
                
                self.coll_cate_2.reloadData()
            }
        }
    }
    
    
    func get_shop_ad_list() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
            is_loaded_first_time = true
        }
        
        Model_My_Location.shared.get_shop_ad_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.arr_list.removeAll()
                
                if status == "success" {
                    self.is_show_marker_window = true
                    self.view_cate.isHidden = false
                    self.hight_view_title.constant = 73
//                    self.view_title.isHidden = false
                    
                    for model in Model_My_Location.shared.arr_get_shop_ad_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(self.arr_list.count) \(self.str_ads)"
                    self.func_list_1()
                } else {
//                    self.func_ShowHud_Error(with: "Ads not found on this city")
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                    })
                    self.is_show_marker_window = false
//                    self.view_cate.isHidden = true
                    self.hight_view_title.constant = 2
//                    self.view_title.isHidden = true
                    
                    self.func_get_city()
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
            }
        }
    }
    
}


// MARK:-  API METHODS
extension My_Loction_ViewController {
    func func_get_shop_ad_list_lat_long() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
//            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_My_Location.shared.func_get_shop_ad_list_lat_long { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.func_HideHud()
                self.arr_list.removeAll()
                self.view_cate.isHidden = false
                self.hight_view_title.constant = 73
//                self.view_title.isHidden = false
                
                if status == "success" {
//                    self.func_get_category()
                    
                    for model in Model_My_Location.shared.arr_get_shop_ad_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                    self.func_list_1()
                }  else {
//                    self.hight_view_title.constant = 60
//                    self.view_cate.isHidden = true
                    let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        
//                        self.mapview.settings.setAllGesturesEnabled(true)
                        self.mapview.clear()
                        
//                        let location_lat = Double(Model_My_Location.shared.location_lat)
//                        let location_lang = Double(Model_My_Location.shared.location_lang)
//
//                        let position = CLLocationCoordinate2D(latitude:location_lat!, longitude:location_lang!)
//                        let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude:position.longitude, zoom: 15)
//
//                        self.mapview.camera = camera
//                        self.mapview.animate(to: camera)
                    })
                }
                
                if Model_My_Location.shared.arr_get_shop_ad_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.arr_get_shop_ad_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
            }
        }
    }
    
    
    func func_get_shop_ad_list_category() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
            //            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_My_Location.shared.func_get_shop_ad_list_category { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.func_HideHud()
                self.view_cate.isHidden = false
                self.hight_view_title.constant = 73
//                self.view_title.isHidden = false
                
                self.arr_list.removeAll()
                
                if status == "success" {
//                    self.func_get_category()

                    for model in Model_My_Location.shared.arr_get_shop_ad_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                }  else {
//                    self.hight_view_title.constant = 60
                                        let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)

                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_My_Location.shared.arr_get_shop_ad_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.arr_get_shop_ad_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list_1()
            }
        }
    }
    
    
    func func_get_shop_ad_list_subcategory() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
            //            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_My_Location.shared.func_get_shop_ad_list_subcategory { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.func_HideHud()
                self.arr_list.removeAll()
                self.view_cate.isHidden = false
                self.hight_view_title.constant = 73
//                self.view_title.isHidden = false
                
                if status == "success" {
//                    self.func_get_category()

                    for model in Model_My_Location.shared.arr_get_shop_ad_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                }  else {
//                    self.hight_view_title.constant = 60
                    let localized = "Shops are not availbale".localized
//                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_My_Location.shared.arr_get_shop_ad_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.arr_get_shop_ad_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list_1()
            }
        }
    }
    
    func func_get_shop_ad_list_mobile() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
            //            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_My_Location.shared.func_get_shop_ad_list_mobile { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.func_HideHud()
                self.arr_list.removeAll()
                self.view_cate.isHidden = false
                self.hight_view_title.constant = 73
//                self.view_title.isHidden = false
                
                if status == "success" {
//                    self.func_get_category()

                    for model in Model_My_Location.shared.arr_get_shop_ad_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                }  else {
//                    self.hight_view_title.constant = 60
                                        let localized = "Shops are not availbale".localized
                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_My_Location.shared.arr_get_shop_ad_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.arr_get_shop_ad_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) 0 \(self.str_ads)"
                }
                self.func_list_1()
            }
        }
    }
    
    
    func func_get_shop_ad_list() {
        if !is_loaded_first_time {
            view_loader.isHidden = false
            //            is_loaded_first_time = true
        } else {
            func_ShowHud()
        }
        
        Model_My_Location.shared.get_shop_ad_list { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                self.is_loaded_first_time = true
                self.arr_list.removeAll()
                self.view_cate.isHidden = false
                self.hight_view_title.constant = 73
//                self.view_title.isHidden = false
                
                if status == "success" {
//                    self.func_get_category()

                    for model in Model_My_Location.shared.arr_get_shop_ad_list {
                        var shop_lat = 0.0
                        var shop_lang = 0.0
                        
                        if !model.shop_lat.isEmpty {
                            shop_lat = Double(model.shop_lat)!
                        }
                        
                        if !model.shop_lang.isEmpty {
                            shop_lang = Double(model.shop_lang)!
                        }
                        
                        self.arr_list.append(CLLocationCoordinate2D (latitude: shop_lat, longitude: shop_lang))
                    }
                } else {
                    self.hight_view_title.constant = 2
//                    self.view_cate.isHidden = true
                    
                    let localized = "Shops are not availbale".localized
                    self.func_ShowHud_Error(with: localized)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
                if Model_My_Location.shared.arr_get_shop_ad_list.count > 0 {
                    self.lbl_morethan_ads.text = "\(self.str_there_are_more_than) \(Model_My_Location.shared.arr_get_shop_ad_list.count) \(self.str_ads)"
                } else {
                    self.lbl_morethan_ads.text = "There are more then 0 \(self.str_ads)"
                }
                self.func_list()
            }
        }
    }
    
    
    
    func func_call_api() {
        is_tap_window_available = true
        if Model_My_Location.shared.category_code.isEmpty && Model_My_Location.shared.subcate_code.isEmpty {
            func_get_shop_ad_list_lat_long()
        } else if Model_My_Location.shared.category_code.isEmpty || Model_My_Location.shared.subcate_code.isEmpty {
            if !Model_My_Location.shared.category_code.isEmpty {
                func_get_shop_ad_list_category()
            } else {
                func_get_shop_ad_list_subcategory()
            }
        } else if !Model_My_Location.shared.category_code.isEmpty && !Model_My_Location.shared.subcate_code.isEmpty {
            func_get_shop_ad_list_subcategory()
        } else if Model_My_Location.shared.subcate_code.contains(",") || Model_My_Location.shared.category_code.contains(",") {
            func_get_shop_ad_list_subcategory()
        } else {
            func_get_shop_ad_list_subcategory()
//            func_get_shop_ad_list()
        }
        
    }
    
    func storeCoordinates(_ coordinates: [CLLocationCoordinate2D]) {
        let locations = coordinates.map { coordinate -> CLLocation in
            return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        let archived = NSKeyedArchiver.archivedData(withRootObject: locations)
        UserDefaults.standard.set(archived, forKey: "coordinates")
        UserDefaults.standard.synchronize()
    }
    
    // Return an array of CLLocationCoordinate2D
    func loadCoordinates() -> [CLLocationCoordinate2D]? {
        guard let archived = UserDefaults.standard.object(forKey: "coordinates") as? Data,
            let locations = NSKeyedUnarchiver.unarchiveObject(with: archived) as? [CLLocation] else {
                return nil
        }
        
        let coordinates = locations.map { location -> CLLocationCoordinate2D in
            return location.coordinate
        }
        return coordinates
    }

}





extension GMSMapView {
    func getCenterCoordinate() -> CLLocationCoordinate2D {
        let centerPoint = self.center
        let centerCoordinate = self.projection.coordinate(for: centerPoint)
        return centerCoordinate
    }
    
    func getTopCenterCoordinate() -> CLLocationCoordinate2D {
        // to get coordinate from CGPoint of your map
        let topCenterCoor = self.convert(CGPoint(x: self.frame.size.width, y: 0), from: self)
        let point = self.projection.coordinate(for: topCenterCoor)
        return point
    }
    
     func getRadius() -> CLLocationDistance {
        let centerCoordinate = getCenterCoordinate()
        let centerLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
        let topCenterCoordinate = self.getTopCenterCoordinate()
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        let radius = CLLocationDistance(centerLocation.distance(from: topCenterLocation))
        
        if radius.inKilometers() < 1 {
            return 1
        }
        
        return round(radius.inKilometers())
    }
    
    
    
}

extension CLLocationDistance {
    func inMiles() -> CLLocationDistance {
        return self*0.00062137
    }
    
    func inKilometers() -> CLLocationDistance {
        return self/1000
    }
}
