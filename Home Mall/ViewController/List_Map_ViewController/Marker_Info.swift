//  Marker_Info.swift
//  Home Mall

//  Created by iOS-Appentus on 10/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import ASStarRatingView

class Marker_Info: UIView {

    @IBOutlet weak var star:ASStarRatingView!
    @IBOutlet weak var view_container:UIView!
    
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var img_container:UIView!
    @IBOutlet weak var lbl_name:UILabel!
    
    @IBOutlet weak var lbl_location_address:UILabel!
    @IBOutlet weak var lbl_owner_name:UILabel!
    @IBOutlet weak var lbl_date_of_update:UILabel!
    
    override func draw(_ rect: CGRect) {
        
        view_container.layer.cornerRadius = 6
        view_container.clipsToBounds = true
        
        view_container.layer.shadowOpacity = 3.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 3.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
        
        img.layer.cornerRadius = 6
        img.clipsToBounds = true

        img_container.layer.cornerRadius = 4
        img_container.layer.shadowOpacity = 3.0
        img_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        img_container.layer.shadowRadius = 3.0
        img_container.layer.shadowColor = UIColor .lightGray.cgColor
        
        lbl_name.layer.cornerRadius = lbl_name.frame.size.height/2
        lbl_name.clipsToBounds = true
    }
    
}
