//
//  Model_Category_List.swift
//  Home Mall
//
//  Created by iOS-Appentus on 29/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation


class Model_My_Location {
    static let shared = Model_My_Location()
    
    var location_lat = ""
    var location_lang = ""
    
    var shop_id = ""
    var user_id = ""
    var shop_image = ""
    var shop_lat = ""
    var shop_lang = ""
    var shop_location = ""
    var shop_add_date = ""
    var category_code = ""
    var subcate_code = ""
    var shop_near_by = ""
    var shop_address = ""
    var user_name = ""
    var user_country_code = ""
    var user_mobile = ""
    var user_email = ""
    var user_join_date = ""
    var user_profile = ""
    var user_shop_id = ""
    var user_device_type = ""
    var user_device_token = ""
    var user_password = ""
    var user_status = ""
    var category_id = ""
    var category_name = ""
    var category_status = ""
    var subcate_id = ""
    var subcate_name = ""
    var subcate_status = ""
    var distance = ""
    var shop_name = ""
    var shop_about = ""
    var view_count = ""
    var avg_rating = ""
    
    var fav_id = ""
    var feedback_id = ""
    var feedback_comment = ""
    var sender_user_id = ""
    var receiver_shop_id = ""
    var feedback_rating = ""
    
//    var arr_shop_ad_list = [Model_Search_VC]()
    
    var city_id = ""
    var city_name = ""
    var city_lat = ""
    var city_long = ""
    
    var raduis_city = ""
    
    var arr_get_city = [Model_Search_VC]()
    var arr_get_city_shop = [Model_Search_VC]()
    var arr_get_shop_ad_list = [Model_Search_VC]()
    
    func func_get_shop_ad_list_lat_long(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_ad_list_lat_long"
        let params = "location_lat=\(location_lat)&location_lang=\(location_lang)&\(k_raduis_parm)=\(raduis_city)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            
            self.arr_get_shop_ad_list.removeAll()
            Model_Search_VC.shared.arr_list_View.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_shop_ad_list.append(self.func_set_Category(dict: dict_json))
                    Model_Search_VC.shared.arr_list_View.append(self.func_set_Category(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    func func_get_shop_ad_list_mobile(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_ad_list_mobile"
        let params = "user_mobile=\(user_mobile)&user_type=0"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            
            self.arr_get_shop_ad_list.removeAll()
            Model_Search_VC.shared.arr_list_View.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_shop_ad_list.append(self.func_set_Category(dict: dict_json))
                    Model_Search_VC.shared.arr_list_View.append(self.func_set_Category(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    
    func func_get_shop_ad_list_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_ad_list_category"
        let params = "location_lat=\(location_lat)&location_lang=\(location_lang)&category_code=\(category_code)&\(k_raduis_parm)=\(raduis_city)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            
            self.arr_get_shop_ad_list.removeAll()
            Model_Search_VC.shared.arr_list_View.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_shop_ad_list.append(self.func_set_Category(dict: dict_json))
                    Model_Search_VC.shared.arr_list_View.append(self.func_set_Category(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    func func_get_shop_ad_list_subcategory(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_ad_list_subcategory"
        let params = "location_lat=\(location_lat)&location_lang=\(location_lang)&subcate_code=\(subcate_code)&\(k_raduis_parm)=\(raduis_city)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            
            self.arr_get_shop_ad_list.removeAll()
            Model_Search_VC.shared.arr_list_View.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_shop_ad_list.append(self.func_set_Category(dict: dict_json))
                    Model_Search_VC.shared.arr_list_View.append(self.func_set_Category(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }

    
    
    func get_shop_ad_list(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_ad_list"
        let params = "category_code=\(category_code)&subcate_code=\(subcate_code)&location_lat=\(location_lat)&location_lang=\(location_lang)&user_mobile=\(user_mobile)&\(k_raduis_parm)=\(raduis_city)"
        print(params)
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_shop_ad_list.removeAll()
            Model_Search_VC.shared.arr_list_View.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_shop_ad_list.append(self.func_set_Category(dict: dict_json))
                    Model_Search_VC.shared.arr_list_View.append(self.func_set_Category(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    func func_get_city_shop(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_city"
        let param = "keyword="
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: param) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_city_shop.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    
                    let city_lat = "\(dict_json["city_lat"] ?? "")"
                    let city_long = "\(dict_json["city_long"] ?? "")"
                    
                    if !city_lat.isEmpty || !city_long.isEmpty {
                        self.arr_get_city_shop.append(self.func_set_get_city(dict: dict_json))
                    }
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    func func_get_city(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_city_shop"
        let param = "keyword="
        
        API_Home_Mall.func_API_Call_POST(str_URL: str_FullURL, param: param) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_get_city.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_get_city.append(self.func_set_get_city(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_set_get_city(dict:[String:Any]) -> Model_Search_VC {
        let model = Model_Search_VC()
        
        model.city_id = "\(dict["city_id"] ?? "")"
        model.city_name = "\(dict["city_name"] ?? "")"
        model.city_nicname = "\(dict["city_nicname"] ?? "")"
        model.city_lat = "\(dict["city_lat"] ?? "")"
        model.city_long = "\(dict["city_long"] ?? "")"
        
        return model
    }
    
    private func func_set_Category(dict:[String:Any]) -> Model_Search_VC {
        print("dict is:-",dict)
        
        let model = Model_Search_VC()
        
        model.shop_id = "\(dict["shop_id"] ?? "")"
        model.user_id = "\(dict["user_id"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        
        let str_shop_lat = "\(dict["shop_lat"] ?? "")"
        model.shop_lat = str_shop_lat.trimmingCharacters(in: .whitespaces)
        
        let str_shop_lang = "\(dict["shop_lang"] ?? "")"
        model.shop_lang = str_shop_lang
        
        model.shop_location = "\(dict["shop_location"] ?? "")"
        model.shop_add_date = "\(dict["shop_add_date"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        model.subcate_code = "\(dict["subcate_code"] ?? "")"
        
        model.shop_near_by = "\(dict["shop_near_by"] ?? "")"
        model.shop_address = "\(dict["shop_address"] ?? "")"
        model.user_name = "\(dict["user_name"] ?? "")"
        model.user_country_code = "\(dict["user_country_code"] ?? "")"
        model.user_mobile = "\(dict["user_mobile"] ?? "")"

        model.user_email = "\(dict["user_email"] ?? "")"
        model.user_join_date = "\(dict["user_join_date"] ?? "")"
        model.user_profile = "\(dict["user_profile"] ?? "")"
        model.user_shop_id = "\(dict["user_shop_id"] ?? "")"
        model.user_device_type = "\(dict["user_device_type"] ?? "")"
        model.user_device_token = "\(dict["user_device_token"] ?? "")"

        model.user_password = "\(dict["user_password"] ?? "")"
        model.user_status = "\(dict["user_status"] ?? "")"
        model.category_id = "\(dict["category_id"] ?? "")"
        model.category_name = "\(dict["category_name"] ?? "")"

        model.category_status = "\(dict["category_status"] ?? "")"
        model.subcate_id = "\(dict["subcate_id"] ?? "")"
        model.subcate_name = "\(dict["subcate_name"] ?? "")"
        model.subcate_status = "\(dict["subcate_status"] ?? "")"
        model.distance = "\(dict["distance"] ?? "")"
        model.shop_name = "\(dict["shop_name"] ?? "")"
        model.shop_about = "\(dict["shop_about"] ?? "")"
        model.view_count = "\(dict["view_count"] ?? "")"
        model.avg_rating = "\(dict["avg_rating"] ?? "")"

        model.fav_id = "\(dict["fav_id"] ?? "")"
        
        model.feedback_id = "\(dict["feedback_id"] ?? "")"
        model.feedback_comment = "\(dict["feedback_comment"] ?? "")"
        model.sender_user_id = "\(dict["sender_user_id"] ?? "")"
        model.receiver_shop_id = "\(dict["receiver_shop_id"] ?? "")"
        model.feedback_rating = "\(dict["feedback_rating"] ?? "")"
        
        return model
    }
    
    var count = ""
    
    func fun_get_shop_count(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_shop_count"
        
        API_Home_Mall.func_API_Call_GET(apiName: str_FullURL) {
            (dict_JSON) in
            
            if dict_JSON["status"] as? String == "success" {
                self.count = "\(dict_JSON["count"] ?? "0")"
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }

    
}


