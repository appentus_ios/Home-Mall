//
//  Splash_ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Splash_ViewController: UIViewController {
    @IBOutlet weak var img_logo:UIImageView!
//    @IBOutlet weak var lbl_welcome:UILabel!
    @IBOutlet weak var progress_Bar:CircularProgress!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        img_logo.layer.borderWidth = 1
//        img_logo.layer.borderColor = UIColor (red: 58.0/255.0, green: 21.0/255.0, blue: 124.0/255.0, alpha: 1.0) .cgColor
//        img_logo.layer.cornerRadius = img_logo.frame.size.height/2
//        img_logo.clipsToBounds = true
        
        progress_Bar.progressColor = color_app
        progress_Bar.trackColor = UIColor .darkGray
        progress_Bar.tag = 101
        
        self.perform(#selector(animateProgress), with: nil, afterDelay: 0.3)
        
//        lbl_welcome.layer.cornerRadius = lbl_welcome.frame.size.height/2
//        lbl_welcome.clipsToBounds = true
        
//        lbl_homemall.layer.cornerRadius = lbl_homemall.frame.size.height/2
//        lbl_homemall.clipsToBounds = true
        
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            Model_Splash.shared.user_id = "\(dict_LoginData["user_id"] ?? "")"
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            let tabbar_controller = self.storyboard?.instantiateViewController(withIdentifier: "TabBar_Controller") as! TabBar_Controller
            tabbar_controller.modalPresentationStyle = .fullScreen
            self.present(tabbar_controller, animated: true, completion: nil)
        }
        
    }
    
    @objc func animateProgress() {
        let cp = self.view.viewWithTag(101) as! CircularProgress
        cp.setProgressWithAnimation(duration: 1.5, value: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}
