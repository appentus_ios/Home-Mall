//
//  Model_Splash.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation

class Model_Splash {
    static let shared = Model_Splash()
    
    var user_country_code = ""
    var user_device_token = ""
    var user_device_type = ""
    var user_email = ""
    var user_id = ""
    var user_join_date = ""
    var user_mobile = ""
    var user_name = ""
    var user_password = ""
    var user_profile = ""
    var user_shop_id = ""
    var user_status = ""
    
    
}


