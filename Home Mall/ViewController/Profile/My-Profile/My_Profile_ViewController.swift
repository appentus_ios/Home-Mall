//
//  ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 22/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SDWebImage

var is_from_profile = false

class My_Profile_ViewController: UIViewController {

    @IBOutlet weak var view_edit:UIView!
    @IBOutlet weak var view_profile_container:UIView!
    @IBOutlet weak var view_addyourshop:UIView!
    @IBOutlet weak var view_shop_image:UIView!
    
    @IBOutlet weak var view_logout:UIView!
    @IBOutlet weak var view_logout_1:UIView!
    @IBOutlet weak var btn_no:UIButton!
    @IBOutlet weak var btn_yes:UIButton!
    
    @IBOutlet weak var img_profile:UIImageView!
    @IBOutlet weak var lbl_user_name:UILabel!
    @IBOutlet weak var lbl_date_join:UILabel!
    @IBOutlet weak var lbl_mobile_number:UILabel!
    
    @IBOutlet weak var lbl_plus:UILabel!
    @IBOutlet weak var lbl_go_to_mange:UILabel!
    
    var view_loader = UIView()
    
    var str_user_mobile = ""
    
    var is_manage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        is_from_profile = true
        func_set_shadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        func_set_data()
        func_get_shop_ad_list_mobile ()
    }
    
    func func_set_data() {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            
            print(dict_LoginData)
            
            img_profile.sd_setShowActivityIndicatorView(true)
            img_profile.sd_setIndicatorStyle(.gray)
            let str_user_profile = "\(dict_LoginData["user_profile"] ?? "")"
            img_profile.sd_setImage(with:URL (string:str_user_profile), placeholderImage:(UIImage(named:"")))
            
            lbl_user_name.text = "\(dict_LoginData["user_name"] ?? "")"
            str_user_mobile = "\(dict_LoginData["user_mobile"] ?? "")"
            let localized_mobile_number = "Mobile Number".localized
            lbl_mobile_number.text = "\(localized_mobile_number) : \(str_user_mobile)"
            
            let localized = "Date Join".localized
            let user_join_date = "\(dict_LoginData["user_join_date"] ?? "")"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            formatter.locale = Locale (identifier:date_localization)
            let date_converted = formatter.date(from:user_join_date)
            
            lbl_date_join.text = "\(localized) : \(formatter.string(from:date_converted!))"
        }
    }
    
    func func_get_shop_ad_list_mobile () {
        view_loader.isHidden = false
        Model_My_Profile.shared.user_mobile = str_user_mobile
        Model_My_Profile.shared.func_get_shop_ad_list_mobile { (status) in
            DispatchQueue.main.async {
                self.view_loader.isHidden = true
                
                if Model_My_Profile.shared.arr_My_Ads.count > 0 {
                    let localized_goto = "Go to".localized
                    let manage = "Manage your shop".localized
                    
                    self.lbl_plus.text = localized_goto.localized
                    self.lbl_plus.font = UIFont .systemFont(ofSize: 30)
                    self.lbl_go_to_mange.text = manage
                    self.is_manage = true
                } else {
                    self.lbl_plus.text = "+"
                    let addShop = "Add your shop".localized
                    self.lbl_plus.font = UIFont .systemFont(ofSize: 50)
                    self.lbl_go_to_mange.text = addShop
                    self.is_manage = false
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func func_set_shadow() {
        view_addyourshop.layer.cornerRadius = 10
        view_addyourshop.clipsToBounds = true
        
        view_edit.layer.cornerRadius = 10
        view_edit.layer.shadowOpacity = 3.0
        view_edit.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view_edit.layer.shadowRadius = 3.0
        view_edit.layer.shadowColor = color_app.cgColor
        
        view_profile_container.layer.shadowOpacity = 3.0
        view_profile_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_profile_container.layer.shadowRadius = 3.0
        view_profile_container.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_shop_image.layer.cornerRadius = 2
        view_shop_image.layer.shadowOpacity = 5.0
        view_shop_image.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view_shop_image.layer.shadowRadius = 6.0
        view_shop_image.layer.shadowColor = UIColor (red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 0.7).cgColor
        
        view_logout.isHidden = true
        
        view_logout_1.layer.cornerRadius = 6
        view_logout_1.clipsToBounds = true
        
        btn_no.layer.cornerRadius = 10
        btn_yes.layer.cornerRadius = 10
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_edit(_ sender:UIButton) {
        let edit_profile = storyboard?.instantiateViewController(withIdentifier: "Edit_Profile_ViewController") as! Edit_Profile_ViewController
        edit_profile.modalPresentationStyle = .fullScreen
        present(edit_profile, animated: true, completion: nil)
    }
    
    @IBAction func btn_add_my_shop(_ sender:UIButton) {
        if is_manage {
            let terms_of_ads = storyboard?.instantiateViewController(withIdentifier: "My_Shop_Manage_ViewController") as! My_Shop_Manage_ViewController
            terms_of_ads.modalPresentationStyle = .fullScreen
            present(terms_of_ads, animated: true, completion: nil)
        } else {
            let terms_of_ads = storyboard?.instantiateViewController(withIdentifier: "Terms_of_Advertising_ViewController") as! Terms_of_Advertising_ViewController
            terms_of_ads.modalPresentationStyle = .fullScreen
            present(terms_of_ads, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btn_logout(_ sender:UIButton) {
        view_logout.isHidden = false
    }
    
    @IBAction func btn_yes_logout(_ sender:UIButton) {
        view_logout.isHidden = true
        UserDefaults.standard.removeObject(forKey: "login_Data")

           dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "after_login"), object: nil)
        NotificationCenter .default.post(name: NSNotification.Name .init(rawValue: "login_chat"), object: nil)
        
    }
    
    @IBAction func btn_no_logout(_ sender:UIButton) {
        view_logout.isHidden = true
    }
    
}




