//
//  Logout.swift
//  Home Mall
//  Created by iOS-Appentus on 22/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit

protocol Logout_Delegate:class {
    func func_logout()
}

class Logout: UIView {
    @IBOutlet weak var view_logout:UIView!
    @IBOutlet weak var btn_no:UIButton!
    @IBOutlet weak var btn_yes:UIButton!
    
    weak var delegate_logout:Logout_Delegate?
    
    override func draw(_ rect: CGRect) {
//        view_logout.layer.cornerRadius = 6
//        view_logout.clipsToBounds = true
//
//        btn_no.layer.cornerRadius = 10
//        btn_yes.layer.cornerRadius = 10
    }
    
    @IBAction func btn_no(_ sender:UIButton) {
        delegate_logout?.func_logout()
    }

}
