//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 22/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import SDWebImage



class Edit_Profile_ViewController: UIViewController {
    @IBOutlet weak var img_profile:UIImageView!
    @IBOutlet weak var img_camera_icon:UIImageView!
    @IBOutlet weak var txt_about_you:UITextView!
    @IBOutlet weak var txt_user_name:UITextField!
    @IBOutlet weak var btn_submit:UIButton!
    
    var is_image = false
    
    var view_loader = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_about_you.text = "Ad About".localized
        
        view_loader = func_Show_loader()
        self.view.addSubview(view_loader)
        view_loader.isHidden = true
        
        txt_about_you.layer.cornerRadius = 6
        txt_about_you.layer.borderColor = color_app.cgColor
        txt_about_you.layer.borderWidth = 1
        txt_about_you.clipsToBounds = true
        
        img_profile.layer.cornerRadius = img_profile.frame.size.height/2
        img_profile.clipsToBounds = true
        
        img_camera_icon.layer.cornerRadius = img_camera_icon.frame.size.height/2
        img_camera_icon.clipsToBounds = true
        
        btn_submit.layer.cornerRadius = 10
        btn_submit.clipsToBounds = true
        
        func_set_data()
    }
    
    func func_set_data() {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            
            print(dict_LoginData)
            
            img_profile.sd_setShowActivityIndicatorView(true)
            img_profile.sd_setIndicatorStyle(.gray)
            
            let str_user_profile = "\(dict_LoginData["user_profile"] ?? "")"
            if str_user_profile.isEmpty {
                is_image = false
            } else {
                is_image = true
            }
            
            img_profile.sd_setImage(with:URL (string:str_user_profile), placeholderImage:(UIImage(named:"image-add-button.png")))
            
            txt_user_name.text = "\(dict_LoginData["user_name"] ?? "")"
            txt_about_you.text = "\(dict_LoginData["user_about"] ?? "")"
            if txt_about_you.text!.isEmpty {
                txt_about_you.text = "Ad About".localized
            }
            txt_about_you.textColor = UIColor .black
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_save(_ sender:UIButton) {
        if !func_validation() {
            return
        }
        
        view_loader.isHidden = false
        Model_Edit_Profile.shared.user_name = txt_user_name.text!
        Model_Edit_Profile.shared.user_about = txt_about_you.text!
        Model_Edit_Profile.shared.profile = img_profile.image!
        
        Model_Edit_Profile.shared.func_update_profile_customer { (status) in
            DispatchQueue.main.async {
               self.view_loader.isHidden = true
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Edit_Profile.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Edit_Profile.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                }
                
            }
        }
    }
    
    @IBAction func btn_select_image(_ sender:UIButton) {
        func_ChooseImage()
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension Edit_Profile_ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Ad About".localized {
            textView.text = ""
            textView.textColor = UIColor .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Ad About".localized
            textView.textColor = UIColor .lightGray
        }
    }
    
}


extension Edit_Profile_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func func_ChooseImage() {
        let alert = UIAlertController(title: "", message: "Please select".localized, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos".localized, style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel , handler:{ (UIAlfdateertAction)in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func func_OpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        info[UIImagePickerControllerEditedImage] as? UIImage
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            img_profile.image = pickedImage
            is_image = true
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func func_validation() -> Bool {
        if !is_image {
            self.func_ShowHud_Error(with:"Select image".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_user_name.text!.isEmpty {
            self.func_ShowHud_Error(with:"Enter name".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else {
            return true
        }
    }
    
}








