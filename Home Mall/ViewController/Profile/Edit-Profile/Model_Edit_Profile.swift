//
//  Model_SignIn.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire


class Model_Edit_Profile {
    static let shared = Model_Edit_Profile()
    
    var user_name = ""
    var user_id = ""
    var user_about = ""
    var profile = UIImage()
    
    var str_message = ""
    
    func func_update_profile_customer(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"update_user"
        
        let params = [
            "user_id":Model_Splash.shared.user_id,
            "user_name":"\(user_name)",
            "user_about":"\(user_about)"
        ]
        print(params)
        
        API_Home_Mall.func_Upadate_Profile(endUrl: str_FullURL, imageData: UIImageJPEGRepresentation(profile, 0.2), parameters: params) {
            (dict_JSON) in
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                let dict_Result = arr_Result![0]
                
//                self.str_message = dict_JSON["message"] as! String
                    self.str_message = "userupdate".localized
                
                if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                    var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                    print(dict_LoginData)
                    
                    dict_LoginData["user_name"] = dict_Result["user_name"]!
                    dict_LoginData["user_profile"] = dict_Result["user_profile"]!
                    dict_LoginData["user_join_date"] = dict_Result["user_join_date"]!
                    dict_LoginData["user_about"] = dict_Result["user_about"]!
                    
                    let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
                    UserDefaults.standard .setValue(data_dict_Result, forKey: "login_Data")
                    
                    print(dict_LoginData)
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
}



