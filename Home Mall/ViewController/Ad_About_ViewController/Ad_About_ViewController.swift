
//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit

class Ad_About_ViewController: UIViewController {
    
    @IBOutlet weak var btn_continue:UIButton!
    
    @IBOutlet weak var txt_select_city:UITextField!
    @IBOutlet weak var txt_about_ad:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_continue.layer.cornerRadius = 10
        btn_continue.layer.shadowOpacity = 3.0
        btn_continue.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        btn_continue.layer.shadowRadius = 3.0
        btn_continue.layer.shadowColor = color_app.cgColor
        
        txt_select_city.layer.cornerRadius = 6
        txt_select_city.layer.borderColor = color_app.cgColor
        txt_select_city.layer.borderWidth = 1
        txt_select_city.clipsToBounds = true
        
        txt_about_ad.layer.cornerRadius = 6
        txt_about_ad.layer.borderColor = color_app.cgColor
        txt_about_ad.layer.borderWidth = 1
        txt_about_ad.clipsToBounds = true
        
        if Model_My_Ads.shared.is_edit {
            let model = Model_Details.shared.arr_details[Model_Details.shared.index_selected]
            
            txt_select_city.text = model.shop_name
            txt_about_ad.text = model.shop_about
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_shop_Count), name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }
    
    @objc func func_shop_Count() {
        if Model_My_Ads.shared.is_edit {
            func_ShowHud_Error(with:Model_Tabbar.shared.message)
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                self.func_HideHud()
                
                let tabbar = self.storyboard?.instantiateViewController(withIdentifier:"TabBar_Controller") as! TabBar_Controller
                tabbar.modalPresentationStyle = .fullScreen
                self.present(tabbar, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "shop_count"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name (rawValue: "user_exists"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_continue(_ sender:Any) {
        if !func_validation() {
            return
        }
        
        Model_Ad_Shop.shared.shop_about = txt_about_ad.text!
        Model_Ad_Shop.shared.shop_name = txt_select_city.text!
        
        let review_Ad = storyboard?.instantiateViewController(withIdentifier: "Review_this_Ad_ViewController") as! Review_this_Ad_ViewController
        review_Ad.modalPresentationStyle = .fullScreen
        present(review_Ad, animated: true, completion: nil)
    }
    
    func func_validation() -> Bool {
        if txt_select_city.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Ad name".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else if txt_select_city.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Ad about".localized)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            
            return false
        } else {
            return true
        }
    }

}



extension Ad_About_ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = ""
            textView.textColor = UIColor .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = ""
            textView.textColor = UIColor .lightGray
        }
    }
}

