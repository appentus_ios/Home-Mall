
//  Model_Ad_Shop.swift
//  Home Mall
//  Created by iOS-Appentus on 30/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import Foundation
import UIKit
import Alamofire


class Model_Ad_Shop {
    static let shared = Model_Ad_Shop()
    
    var shop_lat =  ""
    var shop_lang =  ""
    var shop_location =  ""
    var category_code =  ""
    var subcate_code =  ""
//    var subcate_name =  ""
    var shop_near_to =  ""
    var shop_name =  ""
    var shop_address =  ""
    var shop_about =  ""
    var city_id = ""
    var image_tag = ""
    
    var arr_img_works = [UIImage]()
    
    var shopfile = UIImage (named: "image-add-button.png")
    
    var str_message = ""
    
    func func_Add_shop(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"add_shop"
        
        let parameters = [
            "user_id":Model_Splash.shared.user_id,
            "shop_lat":shop_lat,
            "shop_lang":shop_lang,
            "shop_location":shop_location,
            "category_code":Model_Category_List.shared.category_code,
            "subcate_code":subcate_code,
            "shop_name":shop_name,
            "shop_near_to":shop_near_to,
            "shop_about":shop_about,
            "shop_address":shop_address,
            "city_id":city_id,
            "image_tag":image_tag
        ]
        
        print(parameters)
        
        let url = URL (string: str_FullURL)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            var arrayImgData = [Data]()
            
            var imageData = Data()
            if self.arr_img_works.count > 0 {
                for i in 0 ..< self.arr_img_works.count {
                    imageData = UIImageJPEGRepresentation(self.arr_img_works[i], 0.2)!
                    arrayImgData.append(imageData)
                }
                
                for i in 0..<arrayImgData.count {
                    multipartFormData.append(arrayImgData[i], withName: "shopfile[\(i)]", fileName: "image\(i).jpg", mimeType: "image/jpg")
                }
            }            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        let json =  try JSONSerialization .jsonObject(with:response.data!
                            , options: .allowFragments)
                        
                        let dict_JSON =  cleanJsonToObject(data: json as AnyObject) as! [String:Any]
                        print(dict_JSON)

                        if dict_JSON["status"] as! String == "success" {
                            self.str_message =  dict_JSON["message"] as! String
                            self.arr_shop_ad_list.removeAll()
                            
                            for dict_json in dict_JSON["result"] as! [[String:Any]] {
                                self.arr_shop_ad_list.append(self.func_set_Category(dict: dict_json))
                            }
                        } else {
                            self.str_message = dict_JSON["message"] as! String
                        }
                        completionHandler(dict_JSON["status"] as! String)
                    }
                    catch let error as NSError {
                        print("error is:-",error)
                        self.str_message = "\(error)"
                        completionHandler("false")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                self.str_message = "\(error.localizedDescription)"
            }
        }
    }
    
    
    var location_lat = ""
    var location_lang = ""
    
    var shop_id = ""
    var user_id = ""
    var shop_image = ""
//    var shop_lat = ""
//    var shop_lang = ""
//    var shop_location = ""
    var shop_add_date = ""
//    var category_code = ""
//    var subcate_code = ""
    var shop_near_by = ""
//    var shop_address = ""
    var user_name = ""
    var user_country_code = ""
    var user_mobile = ""
    var user_email = ""
    var user_join_date = ""
    var user_profile = ""
    var user_shop_id = ""
    var user_device_type = ""
    var user_device_token = ""
    var user_password = ""
    var user_status = ""
    var category_id = ""
    var category_name = ""
    var category_status = ""
    var subcate_id = ""
    var subcate_name = ""
    var subcate_status = ""
    var distance = ""
    
    var arr_shop_ad_list = [Model_Ad_Shop]()

    private func func_set_Category(dict:[String:Any]) -> Model_Ad_Shop {
        let model = Model_Ad_Shop()
        
        model.shop_id = "\(dict["shop_id"] ?? "")"
        model.user_id = "\(dict["user_id"] ?? "")"
        model.shop_image = "\(dict["shop_image"] ?? "")"
        
        let str_shop_lat = "\(dict["shop_lat"] ?? "")"
        model.shop_lat = str_shop_lat.trimmingCharacters(in: .whitespaces)
        
        let str_shop_lang = "\(dict["shop_lang"] ?? "")"
        model.shop_lang = str_shop_lang
        
        model.shop_location = "\(dict["shop_location"] ?? "")"
        model.shop_add_date = "\(dict["shop_add_date"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        model.subcate_code = "\(dict["subcate_code"] ?? "")"
        
        model.shop_near_by = "\(dict["shop_near_by"] ?? "")"
        model.shop_address = "\(dict["shop_address"] ?? "")"
        model.user_name = "\(dict["user_name"] ?? "")"
        model.user_country_code = "\(dict["user_country_code"] ?? "")"
        model.user_mobile = "\(dict["user_mobile"] ?? "")"
        
        model.user_email = "\(dict["user_email"] ?? "")"
        model.user_join_date = "\(dict["user_join_date"] ?? "")"
        model.user_profile = "\(dict["user_profile"] ?? "")"
        model.user_shop_id = "\(dict["user_shop_id"] ?? "")"
        model.user_device_type = "\(dict["user_device_type"] ?? "")"
        model.user_device_token = "\(dict["user_device_token"] ?? "")"
        
        model.user_password = "\(dict["user_password"] ?? "")"
        model.user_status = "\(dict["user_status"] ?? "")"
        model.category_id = "\(dict["category_id"] ?? "")"
        model.category_name = "\(dict["category_name"] ?? "")"
        
        model.category_status = "\(dict["category_status"] ?? "")"
        model.subcate_id = "\(dict["subcate_id"] ?? "")"
        model.subcate_name = "\(dict["subcate_name"] ?? "")"
        model.subcate_status = "\(dict["subcate_status"] ?? "")"
        model.distance = "\(dict["distance"] ?? "")"
        
        model.shop_name = "\(dict["shop_name"] ?? "")"
        model.shop_about = "\(dict["shop_about"] ?? "")"
        
        return model
    }
    
    
}









