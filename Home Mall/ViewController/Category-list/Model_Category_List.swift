//
//  Model_Category_List.swift
//  Home Mall
//
//  Created by iOS-Appentus on 29/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import Foundation


class Model_Category_List {
    static let shared = Model_Category_List()
    
    var category_id = ""
    var category_name = ""
    var category_status = ""
    var category_code = ""
    
    var subcate_id = ""
    var subcate_name = ""
    var subcate_code = ""
    var subcate_status = ""
    
    var arr_category_list = [Model_Category_List]()
    var arr_sub_category_list = [Model_Category_List]()
    
    func func_get_category(completionHandler:@escaping (String)->()) {
            let str_FullURL = k_base_url+"get_category"
        
            API_Home_Mall.func_API_Call_GET(apiName: str_FullURL) {
                (dict_JSON) in
                print(dict_JSON)
                
                self.arr_category_list.removeAll()
                if dict_JSON["status"] as? String == "success" {
                    let model = Model_Category_List()
                    
                    model.category_id = ""
                    model.category_name = "الكل"
                    model.category_status = ""
                    model.category_code = ""
                    
                    self.arr_category_list.append(model)
                    for dict_json in dict_JSON["result"] as! [[String:Any]] {
                        self.arr_category_list.append(self.func_set_Category(dict: dict_json))
                    }
                    completionHandler(dict_JSON["status"] as! String)
                } else {
                    if let str_status = dict_JSON["status"] as? String {
                        if str_status == "failed" {

                            completionHandler(dict_JSON["status"] as! String)
                        } else {
                            completionHandler("false")
                        }
                    } else {
                        completionHandler("false")
                    }
                }
            }

        }
    
    private func func_set_Category(dict:[String:Any]) -> Model_Category_List {
        let model = Model_Category_List()
        
        model.category_id = "\(dict["category_id"] ?? "")"
        model.category_name = "\(dict["category_name"] ?? "")"
        model.category_status = "\(dict["category_status"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        
        return model
    }
    
    func func_get_sub_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"get_subcategory"
        
        let params = [
                        "cate_code":category_code
                    ]
        print(params)
        
        API_Home_Mall .postAPI(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.arr_sub_category_list.removeAll()
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_sub_category_list.append(self.func_set_sub_Category(dict: dict_json))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    private func func_set_sub_Category(dict:[String:Any]) -> Model_Category_List {
        let model = Model_Category_List()
        
        model.subcate_id = "\(dict["subcate_id"] ?? "")"
        model.subcate_name = "\(dict["subcate_name"] ?? "")"
        model.subcate_code = "\(dict["subcate_code"] ?? "")"
        model.category_code = "\(dict["category_code"] ?? "")"
        model.subcate_status = "\(dict["subcate_status"] ?? "")"
        
        return model
    }
    
}

