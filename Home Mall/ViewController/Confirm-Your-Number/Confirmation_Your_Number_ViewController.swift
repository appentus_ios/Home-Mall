//  ConfViewController.swift
//  Home Mall

//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import SVProgressHUD

class Confirmation_Your_Number_ViewController: UIViewController {
    @IBOutlet weak var btn_country_code:UIButton!
    @IBOutlet weak var btn_edit:UIButton!
    @IBOutlet weak var btn_ok:UIButton!
    
    @IBOutlet weak var txt_phone_number:UITextField!
    
    @IBOutlet weak var view_fone_number:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_fone_number.layer.borderWidth = 1
        view_fone_number.layer.borderColor = color_app .cgColor
        view_fone_number.layer.cornerRadius = 16
        view_fone_number.clipsToBounds = true
        
        btn_edit.layer.cornerRadius = 10
        btn_edit.layer.shadowOpacity = 1.0
        btn_edit.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        btn_edit.layer.shadowRadius = 3.0
        btn_edit.layer.shadowColor = color_app.cgColor
        
        btn_ok.layer.cornerRadius = 10
        btn_ok.layer.shadowOpacity = 1.0
        btn_ok.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        btn_ok.layer.shadowRadius = 3.0
        btn_ok.layer.shadowColor = color_app.cgColor
        
        btn_country_code.setTitle(Model_Confirm_Number.shared.str_country_code, for: .normal)
        txt_phone_number.text = Model_Confirm_Number.shared.str_mobile_numbe
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_edit(_ sender:UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_ok(_ sender:UIButton) {
        self.view.endEditing(true)
        func_send_otp()
    }
    
    func func_send_otp() {
        let view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        
        Model_Verify_Code.shared.func_send_otp { (status) in
            DispatchQueue.main.async {
                view_loading.isHidden = true
                if status == "success" {
                    let verify_code = self.storyboard?.instantiateViewController(withIdentifier: "Verify_Code_ViewController") as! Verify_Code_ViewController
                    verify_code.modalPresentationStyle = .fullScreen
                    self.present(verify_code, animated: true, completion: nil)
                } else {
                    self.func_ShowHud_Error(with: Model_Verify_Code.shared.str_msg)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        self.func_HideHud()
                    }
                }
            }
        }
    }

    
}


