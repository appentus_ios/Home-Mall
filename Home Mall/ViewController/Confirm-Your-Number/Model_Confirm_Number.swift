//  ViewController.swift
//  Home Mall
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit


class Model_Confirm_Number {
    static let shared = Model_Confirm_Number()
    
    var str_country_code = ""
    var str_mobile_numbe = ""
    var dict_login_data = [String:Any]()
    
    var str_msg = ""
    
    func func_user_register(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_base_url+"user_register"        
        
//        if str_selected_lang == "ar" {
//            str_country_code = "+966"
//        } else {
//            str_country_code = "+91"
//        }
        
        let params = [
            "user_country_code":str_country_code,
            "user_mobile":str_mobile_numbe,
            "device_token":k_FireBaseFCMToken,
            "device_type":"2"
        ]
        
        print(params)
        
        API_Home_Mall .postAPI(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                let arr_result = dict_JSON["result"] as! [[String:Any]]
                self.dict_login_data = arr_result[0]
                Model_Splash.shared.user_id = "\(self.dict_login_data["user_id"] ?? "")"
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_msg = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
}





