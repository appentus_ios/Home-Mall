//
//  ViewController.swift
//  Home Mall
//
//  Created by iOS-Appentus on 23/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import ASStarRatingView


class Ads_Today_ViewController: UIViewController {
    
    @IBOutlet weak var tbl_VC:UITableView!
    
    var arr_is_selected_cell = [Bool]()
    
    var view_loading = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        view_loading = func_Show_loader()
        self.view.addSubview(view_loading)
        view_loading.isHidden = true
        
        arr_is_selected_cell.removeAll()
        Model_Ads_Today.shared.arr_ads_today.removeAll()
        
        func_todays_ad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func func_todays_ad() {
        view_loading.isHidden = false
        
        Model_Ads_Today.shared.func_todays_ad { (status) in
            DispatchQueue.main.async {
                self.view_loading.isHidden = true
                if status == "success" {
                    if Model_Ads_Today.shared.arr_ads_today.count > 0 {
                        self.tbl_VC.isHidden = false
                        for _ in 0..<Model_Ads_Today.shared.arr_ads_today.count {
                            self.arr_is_selected_cell.append(false)
                        }
                    } else {
                        self.tbl_VC.isHidden = true
                    }
                } else {
                    self.tbl_VC.isHidden = true
                }
                self.tbl_VC.reloadData()
            }
        }
    }

    
}




extension Ads_Today_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Ads_Today.shared.arr_ads_today.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! List_TableViewCell
        
        let model = Model_Ads_Today.shared.arr_ads_today[indexPath.row]
        
        cell.image_shadow.sd_setShowActivityIndicatorView(true)
        cell.image_shadow.sd_setIndicatorStyle(.gray)
        cell.image_shadow.sd_setImage(with:URL (string: model.shop_image), placeholderImage:(UIImage(named:"image-add-button.png")))
        cell.lbl_name.text = model.shop_name
        cell.lbl_owner_name.text = model.user_name
        cell.lbl_location_address.text = model.shop_location
        cell.lbl_join_date.text = model.shop_add_date.date_localized
        
        if model .avg_rating.isEmpty {
            cell.star_view.rating = 0
        } else {
            cell.star_view.rating = Float(model.avg_rating)!
        }
        
        if arr_is_selected_cell[indexPath.row] {
            cell.lbl_name.textColor = UIColor .white
            cell.lbl_location_address.textColor = color_light_gray
            
            cell.img_next_arrow.image = cell.img_next_arrow.image?.imageWithColor(color1: UIColor .white)
            
            cell.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            cell.view_container.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            cell.view_container_1.backgroundColor = UIColor (red: 97.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            cell.view_name.layer.cornerRadius = cell.view_name.frame.size.height/2
            cell.view_name.clipsToBounds = true
            
            cell.view_next_arrow.backgroundColor = UIColor (red: 89.0/255.0, green: 227.0/255.0, blue: 186.0/255.0, alpha: 1.0)
            
            cell.view_container.layer.shadowOpacity = 0.0
            cell.view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            cell.view_container.layer.shadowRadius = 0.0
            cell.view_container.layer.shadowColor = UIColor .clear.cgColor
            
            cell.view_name.backgroundColor = UIColor .darkGray
            func_set(view_container: cell.view_container,color: UIColor .gray)
        } else {
            cell.lbl_name.textColor = color_light_gray
            cell.lbl_location_address.textColor = UIColor (red: 255.0/255.0, green: 197.0/255.0, blue: 58.0/255.0, alpha: 1.0)
            
            cell.img_next_arrow.image = cell.img_next_arrow.image?.imageWithColor(color1: color_light_gray)
            
            cell.backgroundColor = UIColor .clear
            cell.view_container_1.backgroundColor = UIColor .white
            cell.view_name.layer.cornerRadius = cell.view_name.frame.size.height/2
            cell.view_name.clipsToBounds = true
            
            cell.view_container.backgroundColor = UIColor .white
            cell.view_next_arrow.backgroundColor = UIColor (red: 241.0/255.0, green: 242.0/255.0, blue: 243.0/255.0, alpha: 1.0)
            
            func_set(view_container: cell.view_container,color: UIColor.gray)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: false)
        
        for i in 0..<Model_Ads_Today.shared.arr_ads_today.count {
            if i == indexPath.row {
                arr_is_selected_cell[i] = true
            } else {
                arr_is_selected_cell[i] = false
            }
        }
        
        Model_Details.shared.index_selected = indexPath.row
        Model_Details.shared.arr_details = Model_Ads_Today.shared.arr_ads_today

        func_present_Fav_VC()
        tbl_VC.reloadData()
    }
    
    func func_present_Fav_VC() {
        let fav_VC = storyboard?.instantiateViewController(withIdentifier: "Details_ViewController") as! Details_ViewController
        fav_VC.modalPresentationStyle = .fullScreen
        present(fav_VC, animated: true, completion: nil)
    }
    
    func func_set_view_name(view_container:AnyObject ,color:UIColor,cornerRadius:CGFloat) {
        if #available(iOS 13.0, *) {
            view_container.layer.cornerRadius = cornerRadius
            view_container.layer.shadowOpacity = 3.0
            view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            view_container.layer.shadowRadius = 3.0
            view_container.layer.shadowColor = color.cgColor
        } else {
            
        }
    }

    func func_set(view_container:AnyObject ,color:UIColor) {
        if #available(iOS 13.0, *) {
        view_container.layer .cornerRadius = 6
        view_container.layer.shadowOpacity = 5.0
        view_container.layer.shadowOffset = CGSize(width: 8.0, height: 8.0)
        view_container.layer.shadowRadius = 5.0
        view_container.layer.shadowColor = color.cgColor
        }
    }
    
}


