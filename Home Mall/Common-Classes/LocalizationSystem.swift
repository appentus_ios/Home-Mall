//
//  LocalizationSystem.swift
//  Home Mall
//
//  Created by iOS-Appentus on 06/04/19.
//  Copyright © 2019 appentus. All rights reserved.



import Foundation
import UIKit

//class LocalizationSystem:NSObject {
//
//    var bundle: Bundle!
//
//    class var sharedInstance: LocalizationSystem {
//        struct Singleton {
//            static let instance: LocalizationSystem = LocalizationSystem()
//        }
//        return Singleton.instance
//    }
//
//    override init() {
//        super.init()
//        bundle = Bundle.main
//    }
//
//    func localizedStringForKey(key:String, comment:String) -> String {
//        return bundle.localizedString(forKey: key, value: comment, table: nil)
//    }
//
//    func localizedImagePathForImg(imagename:String, type:String) -> String {
//        guard let imagePath =  bundle.path(forResource: imagename, ofType: type) else {
//            return ""
//        }
//        return imagePath
//    }
//
//
//
//    //MARK:- setLanguage
//    // Sets the desired language of the ones you have.
//    // If this function is not called it will use the default OS language.
//    // If the language does not exists y returns the default OS language.
//
//
//
//    func setLanguage(languageCode:String) {
//        var appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
//        appleLanguages.remove(at: 0)
//        appleLanguages.insert(languageCode, at: 0)
//        UserDefaults.standard.set(appleLanguages, forKey: "AppleLanguages")
//        UserDefaults.standard.synchronize() //needs restrat
//
//        if let languageDirectoryPath = Bundle.main.path(forResource: languageCode, ofType: "lproj")  {
//            bundle = Bundle.init(path: languageDirectoryPath)
//        } else {
//            resetLocalization()
//        }
//    }
//
//    //MARK:- resetLocalization
//    //Resets the localization system, so it uses the OS default language.
//    func resetLocalization() {
//        bundle = Bundle.main
//    }
//
//    //MARK:- getLanguage
//    // Just gets the current setted up language.
//    func getLanguage() -> String {
//        let appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
//        let prefferedLanguage = appleLanguages[0]
//        if prefferedLanguage.contains("-") {
//            let array = prefferedLanguage.components(separatedBy: "-")
//            return array[0]
//        }
//        return prefferedLanguage
//    }
//
//}





private let appleLanguagesKey = "AppleLanguages"


enum Language: String {

    case english = "en"
    case arabic = "ar"
    case ukrainian = "uk"

    var semantic: UISemanticContentAttribute {
        switch self {
        case .english, .ukrainian:
            return .forceLeftToRight
        case .arabic:
            return .forceRightToLeft
        }
    }


    static var language: Language {
        get {
            if let languageCode = UserDefaults.standard.string(forKey: appleLanguagesKey),
                let language = Language(rawValue: languageCode) {
                return language
            } else {
                let preferredLanguage = NSLocale.preferredLanguages[0] as String
                let index = preferredLanguage.index(
                    preferredLanguage.startIndex,
                    offsetBy: 2
                )
                guard let localization = Language(
                    rawValue: preferredLanguage.substring(to: index)
                    ) else {
                        return Language.english
                }

                return localization
            }
        }
        set {
            guard language != newValue else {
                return
            }

            //change language in the app
            //the language will be changed after restart
            UserDefaults.standard.set([newValue.rawValue], forKey: appleLanguagesKey)
            UserDefaults.standard.synchronize()

            //Changes semantic to all views
            //this hack needs in case of languages with different semantics: leftToRight(en/uk) & rightToLeft(ar)
            UIView.appearance().semanticContentAttribute = newValue.semantic

            //initialize the app from scratch
            //show initial view controller
            //so it seems like the is restarted
            //NOTE: do not localize storboards
            //After the app restart all labels/images will be set
            //see extension String below
            //            UIApplication.shared.windows[0].rootViewController = UIStoryboard(
            //                name: "Main",
            //                bundle: nil
            //                ).instantiateInitialViewController()
        }
    }
}



extension String {
    var localized: String {
        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
    }
    
    var localizedImage: UIImage? {
        return localizedImage()
            ?? localizedImage(type: ".png")
            ?? localizedImage(type: ".jpg")
            ?? localizedImage(type: ".jpeg")
            ?? UIImage(named: self)
    }
    
    private func localizedImage(type: String = "") -> UIImage? {
        guard let imagePath = Bundle.localizedBundle.path(forResource: self, ofType: type) else {
            return nil
        }
        return UIImage(contentsOfFile: imagePath)
    }
    
}

extension Bundle {
    //Here magic happens
    //when you localize resources: for instance Localizable.strings, images
    //it creates different bundles
    //we take appropriate bundle according to language
    static var localizedBundle: Bundle {
        let languageCode = Language.language.rawValue
        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
            return Bundle.main
        }
        return Bundle(path: path)!
    }
}



