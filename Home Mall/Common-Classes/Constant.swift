//  Constant.swift
//  Home Mall
//  Created by iOS-Appentus on 10/01/19.
//  Copyright © 2019 appentus. All rights reserved.

import Foundation
import UIKit
import SVProgressHUD



let color_app = UIColor (red: 255.0/255.0, green: 86.0/255.0, blue: 202.0/255.0, alpha: 1.0)
//let k_base_url = "http://homemall.xyz/homemall/api/api/"
//let k_base_url = "http://appentus.me/homemall/api/api/"
//let k_base_url = "http://ondemandcab.com/homemall/api/Api/"


let default_selected_location = "default_selected_location"

let url_common = "http://46.105.96.151/homemall/api/"
let k_base_url = "\(url_common)Api/"
let k_BaseURL_chat = "\(url_common)chat/"

let k_raduis_parm = "radius"

let date_localization = "ar"

extension UIViewController {
    func func_Show_loader() -> UIView {
        let view_loading = Bundle.main.loadNibNamed("Loading", owner: self, options: nil)?.first as! Loading
        view_loading.frame = self.view.frame
        
        return view_loading
    }
    
    func func_Show_loader(message message:String, title title:String) -> UIView {
        let view_loading = Bundle.main.loadNibNamed("Loading_With_Message", owner: self, options: nil)?.first as! Loading_With_Message
        view_loading.frame = self.view.frame
        
        view_loading.btn_message.setTitle(title, for: .normal)
        view_loading.lbl_title.text = message
        
        return view_loading
    }
    
    func func_ShowHud() {
        SVProgressHUD.show()
    }
    
    func func_ShowHud_Success(with str_Error:String) {
        DispatchQueue.main.async {
            SVProgressHUD.setBackgroundColor(UIColor (red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
            SVProgressHUD.showSuccess(withStatus: str_Error)
            self.view .isUserInteractionEnabled = false
        }
    }

    func func_ShowHud_Error(with str_Error:String) {
        DispatchQueue.main.async {
            SVProgressHUD.showError(withStatus: str_Error)
            self.view .isUserInteractionEnabled = false
        }
    }
    
    func func_HideHud() {
        DispatchQueue.main.async {
            SVProgressHUD .dismiss()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func func_IsValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}



public extension UIViewController {

    /// Adds child view controller to the parent.
    ///
//    / - Parameter child: Child view controller.
    func add(_ child: UIViewController) {
        addChildViewController(child)
        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }

    func remove() {
        guard parent != nil else {
            return
        }
        
        willMove(toParentViewController: nil)
        removeFromParentViewController()
        view.removeFromSuperview()
    }
    
    /// It removes the child view controller from the parent.
    func remove(vc : UIViewController) {
//        guard parent != nil else {
//            return
//        }

        vc.willMove(toParentViewController: nil)
        vc.removeFromParentViewController()
        vc.view.removeFromSuperview()
    }
}



extension String {
//    var localized: String {
//        return NSLocalizedString(self, comment: "")
//    }
}



extension String {
    var date_localized : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale (identifier:date_localization)
        let date_converted = formatter.date(from:self)
        
        return formatter.string(from:date_converted ?? Date())
    }
    
    var date_time_localized : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:mm a"
        formatter.locale = Locale (identifier:date_localization)
        let date_converted = formatter.date(from:self)
        
        return formatter.string(from:date_converted!)
    }
    
}
