//
//  let infoWindow = Bundle.main.loadNibNamed("Marker_Info", owner: self, options: nil)?.swift
//  Home Mall
//
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Loading: UIView {
    @IBOutlet weak var view_loading:UIView!
    
    override func draw(_ rect: CGRect) {
        
        view_loading.layer.cornerRadius = 4
        view_loading.clipsToBounds = true
    }
    
}
