//
//  Loading_With_Message.swift
//  Home Mall
//
//  Created by iOS-Appentus on 11/01/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Loading_With_Message: UIView {
    @IBOutlet weak var view_loading:UIView!

    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var btn_message:UIButton!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        view_loading.layer.cornerRadius = 4
        view_loading.clipsToBounds = true
    }
    
    
}
